package uk.ac.aber.major.mik46.go.repositories

import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.repositories.ProductsRepo


class FakeProductsRepository : ProductsRepo {

    val productsList = MutableList(25){ index ->  Product(productID = "product$index") }
    private var productsLoadedCount = 0
    private var dateQueryLoadedCount = 0



    override suspend fun getProductsByQuery(queryCondition: ProductQueryType) : MutableList<Product>{
        var tempList =  productsList
        when(queryCondition) {
            ProductQueryType.DATE -> {
                tempList.sortBy{ it.uploadTime }
                tempList = getNextTen(dateQueryLoadedCount, tempList)
                dateQueryLoadedCount += tempList.size
            }
            else -> {
                tempList = getNextTen(productsLoadedCount, tempList)
                productsLoadedCount += tempList.size
            }
        }
        return tempList
    }

    override suspend fun getUserFromFireStoreByUID(UID: String): User {
        TODO("Not yet implemented")
    }
}

private fun getNextTen(currentCount: Int, queriedList: MutableList<Product>): MutableList<Product>{
    val remainingItems = queriedList.size - currentCount
    val sublistSize = if (remainingItems > 10) 10 else remainingItems
    val mySubList =
        queriedList.subList(currentCount, currentCount + sublistSize)
    return mySubList.toMutableList()
}