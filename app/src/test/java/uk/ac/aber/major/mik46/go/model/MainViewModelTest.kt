package uk.ac.aber.major.mik46.go.model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.google.firebase.auth.FirebaseAuth
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.repositories.FakeMainRepository
import uk.ac.aber.major.mik46.go.repositories.FakeMessagingRepository
import java.util.*

@OptIn(ExperimentalCoroutinesApi::class)
class MainViewModelTest{

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var mainViewModel: MainViewModel
    private lateinit var fakeFireStoreRepository: FakeMainRepository
    private lateinit var fakeMessagingRepository: FakeMessagingRepository
    private lateinit var testUser: User
    private lateinit var testUser2: User

    private lateinit var authentication :FirebaseAuth


    @Before
    fun setup() {

        authentication = mockk<FirebaseAuth>()
        testUser = User(authentication = authentication, userName = "user1")
        testUser2 = User(authentication = authentication, userName = "user2")
        fakeFireStoreRepository = FakeMainRepository()
        fakeMessagingRepository = FakeMessagingRepository()
    }

    @After
    fun testEnd(){
        clearAllMocks()
        unmockkAll()
    }


    @Test
    fun `updateUsersChats should update usersChats LiveData with correct value`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)
        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try{
            val expectedList = listOf(UserChat(receiver = testUser))
            fakeFireStoreRepository.chatsList.add(UserChat(receiver = testUser))
            mainViewModel.updateUsersChats(testUser)
            advanceUntilIdle()

            val result = mainViewModel.usersChats.value


            assertThat(result).isEqualTo(expectedList)
        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `updateUsersChats should return empty when no values added`() = runTest() {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try{
            mainViewModel.updateUsersChats(testUser)
            advanceUntilIdle()

            val result = mainViewModel.usersChats.value


            assertThat(result).isEmpty()
        }finally {
            Dispatchers.resetMain()
        }

    }



    @Test
    fun `initializeListener is adding to the usersMessagesListener list`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()

            assertThat(mainViewModel.usersMessagesListener.value).isNotEmpty()
        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `initializeListener is activating listener`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()

            assertThat(fakeMessagingRepository.listenerIsActive).isTrue()
        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `removeCurrentListener should set listenerIsActive to false in the messaging repository`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test1"))
            advanceUntilIdle()
            mainViewModel.removeCurrentListener()
            advanceUntilIdle()

            assertThat(mainViewModel.usersMessagesListener.value).isEmpty()
            assertThat(fakeMessagingRepository.listenerIsActive).isFalse()

        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `removeCurrentListener multiple empties of the listener list`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            launch{mainViewModel.removeCurrentListener()}
            advanceUntilIdle()
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            launch{mainViewModel.removeCurrentListener()}
            advanceUntilIdle()
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            launch{mainViewModel.removeCurrentListener()}
            advanceUntilIdle()
            assertThat(mainViewModel.usersMessagesListener.value).isEmpty()
            assertThat(fakeMessagingRepository.listenerIsActive).isFalse()

        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `updateMessages adds to the Old messages list`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.updateUsersMessages(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            assertThat(mainViewModel.usersOldMessages.value).isNotEmpty()

        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `updateMessages first update is size 10`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.updateUsersMessages(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            assertThat(mainViewModel.usersOldMessages.value).hasSize(10)

        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `updateMessages second update is size 20`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.updateUsersMessages(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.updateUsersMessages(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            assertThat(mainViewModel.usersOldMessages.value).hasSize(20)

        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `updateMessages repeat update caps at the list size`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.updateUsersMessages(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.updateUsersMessages(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.updateUsersMessages(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.updateUsersMessages(testUser, "testEmail@ddd.com")
            advanceUntilIdle()


            assertThat(mainViewModel.usersOldMessages.value).hasSize(25)

        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `emptyOldMessages empties the Old messages list`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.updateUsersMessages(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.emptyOldMessageList()
            advanceUntilIdle()
            assertThat(mainViewModel.usersOldMessages.value).isEmpty()

        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `sendMessage adds to listener list`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test1"))
            advanceUntilIdle()

            assertThat(mainViewModel.usersMessagesListener.value).hasSize(2)
        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `sendMessage adding multiple to listener list`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test1"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test2"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test3"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test4"))
            advanceUntilIdle()

            assertThat(mainViewModel.usersMessagesListener.value).hasSize(5)
        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `sendMessage adding multiple to listener list and then removing`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test1"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test2"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test3"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test4"))
            advanceUntilIdle()

            mainViewModel.removeCurrentListener()
            advanceUntilIdle()

            assertThat(mainViewModel.usersMessagesListener.value).isEmpty()
            assertThat(fakeMessagingRepository.listenerIsActive).isFalse()
        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `test addImageToFirebaseStorage Uri is mocked and returns empty string`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            val imageByteArray = "Test Image Data".toByteArray()
            val userEmail = "test@example.com"
            val imageTime = Date()

            val result = mainViewModel.addImageToFirebaseStorage(imageByteArray, userEmail, imageTime)

            assertEquals("", result.toString())
        }finally {
            Dispatchers.resetMain()
        }
    }


    @Test
    fun `send 4 Messages and reinitialize should keep messages`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test1"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test2"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test3"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test4"))
            advanceUntilIdle()

            mainViewModel.removeCurrentListener()
            advanceUntilIdle()
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()

            assertThat(mainViewModel.usersMessagesListener.value).hasSize(5)
        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `send 4 Messages and reinitialize twice should keep messages`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            for (i in 0..1){
            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test1"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test2"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test3"))
            advanceUntilIdle()
            mainViewModel.sendUserMessage(testUser, "testEmail@ddd.com", UserChatMessage(content = "sent message test4"))
            advanceUntilIdle()

            mainViewModel.removeCurrentListener()
            advanceUntilIdle()
                }

            mainViewModel.initializeUsersMessagesListener(testUser, "testEmail@ddd.com")
            advanceUntilIdle()

            assertThat(mainViewModel.usersMessagesListener.value).hasSize(9)
        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `userSetup should add a regToken to list`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.userSetup(testUser)
            advanceUntilIdle()

            assertThat(fakeFireStoreRepository.userRegTokenList).hasSize(1)


        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `userSetup should only add 1 regToken per user`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.userSetup(testUser)
            advanceUntilIdle()
            mainViewModel.userSetup(testUser)
            advanceUntilIdle()

            assertThat(fakeFireStoreRepository.userRegTokenList).hasSize(1)


        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `userSetup adding 2 users add 2 regTokens`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.userSetup(testUser)
            advanceUntilIdle()
            mainViewModel.userSetup(testUser2)
            advanceUntilIdle()

            assertThat(fakeFireStoreRepository.userRegTokenList).hasSize(2)


        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `userSetup adding 2 users twice adds only 2 regTokens`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.userSetup(testUser)
            advanceUntilIdle()
            mainViewModel.userSetup(testUser2)
            advanceUntilIdle()
            mainViewModel.userSetup(testUser)
            advanceUntilIdle()
            mainViewModel.userSetup(testUser2)
            advanceUntilIdle()

            assertThat(fakeFireStoreRepository.userRegTokenList).hasSize(2)


        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `userSetup then removing token`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.userSetup(testUser)
            advanceUntilIdle()

            mainViewModel.removeRegistrationToken(testUser)
            advanceUntilIdle()


            assertThat(fakeFireStoreRepository.userRegTokenList).hasSize(0)


        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `removing token without adding user does nothing`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.removeRegistrationToken(testUser)
            advanceUntilIdle()


            assertThat(fakeFireStoreRepository.userRegTokenList).hasSize(0)


        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `userSetup adding 2 users then removing 1 token`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.userSetup(testUser)
            advanceUntilIdle()
            mainViewModel.userSetup(testUser2)
            advanceUntilIdle()

            mainViewModel.removeRegistrationToken(testUser)
            advanceUntilIdle()


            assertThat(fakeFireStoreRepository.userRegTokenList).hasSize(1)


        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `userSetup adding 2 users then removing token from the same only removes that user`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            mainViewModel.userSetup(testUser)
            advanceUntilIdle()
            mainViewModel.userSetup(testUser2)
            advanceUntilIdle()

            mainViewModel.removeRegistrationToken(testUser)
            advanceUntilIdle()
            mainViewModel.removeRegistrationToken(testUser)
            advanceUntilIdle()
            mainViewModel.removeRegistrationToken(testUser)
            advanceUntilIdle()


            assertThat(fakeFireStoreRepository.userRegTokenList).hasSize(1)


        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `getCurrentRegistrationToken should return the testRegToken`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = testDispatcher
        )

        Dispatchers.setMain(testDispatcher)

        try {
            val regToken = fakeFireStoreRepository.getCurrentRegistrationToken()
            advanceUntilIdle()


            assertThat(regToken).isEqualTo("testToken")


        }finally {
            Dispatchers.resetMain()
        }
    }



}