package uk.ac.aber.major.mik46.go

import org.junit.Assert.assertEquals
import org.junit.Test
import uk.ac.aber.major.mik46.go.model.UserChatMessage
import java.util.*


class UserChatMessageTest {

    @Test
    fun `getTimeFormat returns 'just now' when time difference is less than 1 minute`() {
        val currentTime = Date()
        val messageTime = Date(currentTime.time - 5000) // message sent 5 seconds ago
        val message = UserChatMessage(time = messageTime)
        val result = message.getTimeFormat()
        assertEquals("just now", result)
    }

    @Test
    fun `getTimeFormat returns 'x seconds ago' when time difference is less than 1 hour`() {
        val currentTime = Date()
        val messageTime = Date(currentTime.time - 30000) // message sent 30 seconds ago
        val message = UserChatMessage(time = messageTime)
        val result = message.getTimeFormat()
        assertEquals("30s ago", result)
    }

    @Test
    fun `getTimeFormat returns 'x minutes ago' when time difference is less than 1 day`() {
        val currentTime = Date()
        val messageTime = Date(currentTime.time - 3600000) // message sent 1 hour ago
        val message = UserChatMessage(time = messageTime)
        val result = message.getTimeFormat()
        assertEquals("60m ago", result)
    }

    @Test
    fun `getTimeFormat returns 'x hours ago' when time difference is less than 1 week`() {
        val currentTime = Date()
        val messageTime = Date(currentTime.time - 86400000) // message sent 1 day ago
        val message = UserChatMessage(time = messageTime)
        val result = message.getTimeFormat()
        assertEquals("24h ago", result)
    }

    @Test
    fun `getTimeFormat returns 'x days ago' when time difference is less than 7 days`() {
        val currentTime = Date()
        val messageTime = Date(currentTime.time - 604800000) // message sent 1 week ago
        val message = UserChatMessage(time = messageTime)
        val result = message.getTimeFormat()
        assertEquals("7d ago", result)
    }

}