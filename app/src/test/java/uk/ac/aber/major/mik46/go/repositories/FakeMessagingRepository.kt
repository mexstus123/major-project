package uk.ac.aber.major.mik46.go.repositories

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.*
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.UserChatMessage
import uk.ac.aber.major.mik46.go.model.repositories.MessagingRepo
import java.util.*


class FakeMessagingRepository : MessagingRepo{


    private val oldMessageList = MutableList(25){UserChatMessage(content = "test1")}
    private var oldMessageCallCount = 0

    private val listenerMessageList = mutableListOf(UserChatMessage(content = "1st message on listener"))
    var listenerIsActive = false

    override fun removeCurrentListener() {
        listenerIsActive = false
    }

    override suspend fun sendMessage(
        user: User,
        receiverEmail: String,
        message: UserChatMessage
    ) {
        // Add the message to the messageList
        listenerMessageList.add(message)
    }

    override suspend fun addImageToFirebaseStorage(
        imageByteArray: ByteArray,
        userEmail: String,
        imageTime: Date
    ): Uri {
        // Do nothing as there is no Firebase storage in this implementation
        val mockedUri = mockk<Uri>()
        every { mockedUri.toString() } returns ""
        return mockedUri
    }

    override suspend fun updateUsersMessages(
        user: User,
        receiverEmail: String
    ): MutableList<UserChatMessage> {
        val remainingItems = oldMessageList.size - oldMessageCallCount
        val sublistSize = if (remainingItems > 10) 10 else remainingItems
        val mySubList = oldMessageList.subList(oldMessageCallCount, oldMessageCallCount + sublistSize)
        oldMessageCallCount += sublistSize
        return mySubList.toMutableList()
    }

    @OptIn(DelicateCoroutinesApi::class)
    override suspend fun initializeUsersMessagesListener(
        user: User,
        receiverEmail: String,
        messageList: MutableLiveData<List<UserChatMessage>>
    ) = withContext(Dispatchers.Main){
        // Save the listenerRegistration so that it can be removed later
        this.coroutineContext.run { messageList.value = listenerMessageList }

        listenerIsActive = true
    }
}