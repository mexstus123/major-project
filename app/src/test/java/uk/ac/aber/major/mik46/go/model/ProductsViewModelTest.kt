package uk.ac.aber.major.mik46.go.model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.google.firebase.auth.FirebaseAuth
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.major.mik46.go.model.viewmodels.ProductsViewModel
import uk.ac.aber.major.mik46.go.repositories.FakeProductsRepository
import java.util.*

@OptIn(ExperimentalCoroutinesApi::class)
class ProductsViewModelTest{

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var productsViewModel: ProductsViewModel
    private lateinit var fakeProductsRepository: FakeProductsRepository

    private lateinit var testUser: User
    private lateinit var testUser2: User

    private lateinit var authentication :FirebaseAuth


    @Before
    fun setup() {

        authentication = mockk<FirebaseAuth>()
        testUser = User(authentication = authentication, userName = "user1")
        testUser2 = User(authentication = authentication, userName = "user2")
        fakeProductsRepository = FakeProductsRepository()
    }

    @After
    fun testEnd(){
        clearAllMocks()
        unmockkAll()
    }


    @Test
    fun `get products only gives 10 at a time`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)
        productsViewModel = ProductsViewModel(
            ioDispatcher = testDispatcher,
            productsRepository = fakeProductsRepository
        )

        Dispatchers.setMain(testDispatcher)

        try{

            productsViewModel.updateProductsScreen(ProductQueryType.NOQUERY)
            advanceUntilIdle()

            val result = productsViewModel.products.value


            assertThat(result).hasSize(10)
        }finally {
            Dispatchers.resetMain()
        }
    }
    @Test
    fun `get products stops at list size`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)
        productsViewModel = ProductsViewModel(
            ioDispatcher = testDispatcher,
            productsRepository = fakeProductsRepository
        )

        Dispatchers.setMain(testDispatcher)

        try{

            productsViewModel.updateProductsScreen(ProductQueryType.NOQUERY)
            advanceUntilIdle()
            productsViewModel.updateProductsScreen(ProductQueryType.NOQUERY)
            advanceUntilIdle()
            productsViewModel.updateProductsScreen(ProductQueryType.NOQUERY)
            advanceUntilIdle()
            productsViewModel.updateProductsScreen(ProductQueryType.NOQUERY)
            advanceUntilIdle()

            val result = productsViewModel.products.value


            assertThat(result).hasSize(25)
        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `get products by query adds to separate lists`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)
        productsViewModel = ProductsViewModel(
            ioDispatcher = testDispatcher,
            productsRepository = fakeProductsRepository
        )

        Dispatchers.setMain(testDispatcher)

        try{

            productsViewModel.updateProductsScreen(ProductQueryType.NOQUERY)
            advanceUntilIdle()
            productsViewModel.updateProductsScreen(ProductQueryType.DATE)
            advanceUntilIdle()


            val result = productsViewModel.products.value
            val resultByDate = productsViewModel.productsByDate.value


            assertThat(result).hasSize(10)
            assertThat(resultByDate).hasSize(10)
        }finally {
            Dispatchers.resetMain()
        }
    }


    @Test
    fun `get products queried by date gives 10 at a time`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)
        productsViewModel = ProductsViewModel(
            ioDispatcher = testDispatcher,
            productsRepository = fakeProductsRepository
        )

        Dispatchers.setMain(testDispatcher)

        try{

            productsViewModel.updateProductsScreen(ProductQueryType.DATE)
            advanceUntilIdle()

            val result = productsViewModel.productsByDate.value


            assertThat(result).hasSize(10)
        }finally {
            Dispatchers.resetMain()
        }
    }

    @Test
    fun `get products queried by date sorts by date`() = runTest {
        val testDispatcher = StandardTestDispatcher(testScheduler)
        productsViewModel = ProductsViewModel(
            ioDispatcher = testDispatcher,
            productsRepository = fakeProductsRepository
        )

        Dispatchers.setMain(testDispatcher)

        try{
            val currentTime = Date()
            fakeProductsRepository.productsList.add(Product(uploadTime = Date(currentTime.time - 86400000) ))

            productsViewModel.updateProductsScreen(ProductQueryType.DATE)
            advanceUntilIdle()
            productsViewModel.updateProductsScreen(ProductQueryType.DATE)
            advanceUntilIdle()
            productsViewModel.updateProductsScreen(ProductQueryType.DATE)
            advanceUntilIdle()

            val result = productsViewModel.productsByDate.value

            // Create a custom comparator to compare the Date objects of each product
            val dateComparator = Comparator<Product> { p1, p2 -> p1.uploadTime.compareTo(p2.uploadTime) }

            // Check if the list of products is sorted by date
            assertThat(result).isInOrder(dateComparator)
            assertThat(result).hasSize(26)

        }finally {
            Dispatchers.resetMain()
        }
    }

    }