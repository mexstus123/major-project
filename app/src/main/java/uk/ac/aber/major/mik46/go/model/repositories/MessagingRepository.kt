package uk.ac.aber.major.mik46.go.model.repositories

import android.content.ContentValues
import android.net.Uri

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.*
import com.google.firebase.messaging.Constants
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import uk.ac.aber.major.mik46.go.model.Product
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.UserChatMessage
import uk.ac.aber.major.mik46.go.model.notifications.NotificationData
import uk.ac.aber.major.mik46.go.model.notifications.PushNotification
import uk.ac.aber.major.mik46.go.model.notifications.RetrofitInstance
import java.util.*


/**
 * Repository class responsible for handling data operations.
 * This is the mediator between the [UserWordDao] and the [UserWordsViewModel].
 *
 * @param application The application context
 */
class MessagingRepository(): MessagingRepo {

    private val storage = FirebaseStorage.getInstance()


    private val db = FirebaseFirestore.getInstance()

    private lateinit var currentLastVisibleDocument: DocumentSnapshot
    lateinit var listenerRegistration: ListenerRegistration

    private suspend fun getUserDocumentID(UID: String): String{
        var userDocumentID = ""

        try {
            //getting the user id by email
            db.collection("new").whereEqualTo("UID", UID).get().await().forEach {
                userDocumentID = it.id
            }
        }catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
        return userDocumentID
    }

    override fun removeCurrentListener(){
        listenerRegistration.remove()
    }

    override suspend fun addImageToFirebaseStorage(imageByteArray: ByteArray,UID: String, imageTime: Date): Uri{
        var downloadUrl: Uri = Uri.EMPTY
       try {
            downloadUrl = storage.reference.child("IMAGES").child(getUserDocumentID(UID)).child(imageTime.toString())
                .putBytes(imageByteArray).await()
                .storage.downloadUrl.await()

        } catch (e: FirebaseFirestoreException) {
           Log.d("error", "Failed to add image to storage: $e")
       }
        return downloadUrl
    }


    override suspend fun sendMessage(user: User, receiverUID: String, message: UserChatMessage) { //TODO optimize so that its not saving messages into 2 places <- HARD
        var userDocumentID = getUserDocumentID(user.UID)
        var receiverRegTokens = emptyMap<String,String>()

        try {
            val messageToSenderDB = hashMapOf(
                "content" to message.content,
                "sender" to message.sender,
                "time" to message.time,
                "image" to message.image,
                "wasRead" to true
            )
            val messageToReceiverDB = hashMapOf(
                "content" to message.content,
                "sender" to message.sender,
                "time" to message.time,
                "image" to message.image,
                "wasRead" to false
            )
            //if the path is empty make a new one
            val senderDbRoute = db.collection("new/${userDocumentID}/Chats").whereEqualTo("receiverUID", receiverUID).get().await()
            if (senderDbRoute.isEmpty) {
                //add new path
                db.collection("new/${userDocumentID}/Chats")
                    .add(
                        hashMapOf(
                            "receiverUID" to user.UID
                    ))
            }

            //saving it to current users messages collection
            db.collection("new/${userDocumentID}/Chats").whereEqualTo("receiverUID", receiverUID).get().await()
                .map { documentSnapshot ->
                    //getting to the users messages and adding the message
                    db.collection("new/${userDocumentID}/Chats/${documentSnapshot.id}/Messages")
                        .add(messageToSenderDB)
                        .addOnSuccessListener { documentReference ->
                            Log.d(
                                ContentValues.TAG,
                                "DocumentSnapshot added with ID: ${documentReference.id}"
                            )
                        }
                        .addOnFailureListener { e ->
                            Log.w(ContentValues.TAG, "Error adding document", e)
                        }

                }

            //saving it to message receivers messages collection
            db.collection("new").whereEqualTo("UID", receiverUID).get().await().forEach {
                userDocumentID = it.id
                receiverRegTokens = it.get("regTokens")!! as Map<String, String>
            }

            //if the path is empty make a new one
            val receiversDbRoute = db.collection("new/${userDocumentID}/Chats").whereEqualTo("receiverUID", user.UID).get().await()
            if (receiversDbRoute.isEmpty) {
                //add new path
                db.collection("new/${userDocumentID}/Chats")
                    .add(hashMapOf("receiverUID" to user.UID))
            }

            receiversDbRoute
                .map { documentSnapshot ->

                    //getting to the messages and looping through all the messages
                    db.collection("new/${userDocumentID}/Chats/${documentSnapshot.id}/Messages")
                        .add(messageToReceiverDB)
                        .addOnSuccessListener { documentReference ->
                            Log.d(
                                ContentValues.TAG,
                                "DocumentSnapshot added with ID: ${documentReference.id}"
                            )
                        }
                        .addOnFailureListener { e ->
                            Log.w(ContentValues.TAG, "Error adding document", e)
                        }

                }
            val notificationContent = when(message.image){
                true -> "Image"
                false -> message.content
            }

            sendMessagingNotifications(receiverRegTokens, user.userName, notificationContent)


        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
    }

    private suspend fun sendMessagingNotifications(regTokens:  Map<String, String>, userName: String, notificationContent: String) {

        //for each of the users reg tokens, so each device the user is logged into
        regTokens.forEach { (key, value) ->
            //make the notification
            val notification = PushNotification(
                NotificationData(userName, notificationContent),
                value
            )
            //try to send it
            try {
                val response = RetrofitInstance.api.postNotification(notification)
                if (response.isSuccessful) {
                    Log.d(
                        Constants.TAG,
                        "notification sent: ${notification.data.message}, ${notification.data.title}"
                    )
                } else {
                    Log.e(Constants.TAG, response.errorBody().toString())
                }
            } catch (e: Exception) {
                Log.e(Constants.TAG, e.toString())
            }
        }
    }



    override suspend fun updateUsersMessages(user: User, receiverUID: String): MutableList<UserChatMessage>{
        val userDocumentID = getUserDocumentID(user.UID)
        val messageList = mutableListOf<UserChatMessage>()

        try {

            //nested loop to get all the messages within the chats
            db.collection("new/${userDocumentID}/Chats").whereEqualTo("receiverUID", receiverUID).get()
                .await()
                .map { documentSnapshot ->

                    when(::currentLastVisibleDocument.isInitialized){
                        true ->{db.collection("new/${userDocumentID}/Chats/${documentSnapshot.id}/Messages")
                            .orderBy("time", Query.Direction.DESCENDING)
                            .startAfter(currentLastVisibleDocument)
                            .limit(10).get().await()
                            .forEachIndexed{
                                    index, it ->
                                messageList.add(it.toObject(UserChatMessage::class.java))
                                if (index == messageList.lastIndex){
                                    currentLastVisibleDocument = it
                                }

                            }}
                        false -> {db.collection("new/${userDocumentID}/Chats/${documentSnapshot.id}/Messages")
                            .orderBy("time", Query.Direction.DESCENDING)
                            .startAt(0)
                            .limit(10).get().await()
                            .forEachIndexed{
                                    index, it ->
                                messageList.add(it.toObject(UserChatMessage::class.java))
                                if (index == messageList.lastIndex){
                                    currentLastVisibleDocument = it
                                }

                            }}
                    }
                }
        }catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
        return  messageList.toMutableList()
    }

    override suspend fun initializeUsersMessagesListener(user: User, receiverUID: String, messageList: MutableLiveData<List<UserChatMessage>>){

        val userDocumentID = getUserDocumentID(user.UID)

        try {

            val senderDbRoute = db.collection("new/${userDocumentID}/Chats").whereEqualTo("receiverUID", receiverUID).get().await()
            if (senderDbRoute.isEmpty) {
                //add new path
                db.collection("new/${userDocumentID}/Chats")
                    .add(
                        hashMapOf(
                            "receiverUID" to receiverUID,
                        ))
            }
                //nested loop to get all the messages within the chats
                db.collection("new/${userDocumentID}/Chats").whereEqualTo("receiverUID", receiverUID).get().await()
                    .map { documentSnapshot ->
                        //updating the wasRead field of the most recent message
                        db.collection("new/${userDocumentID}/Chats/${documentSnapshot.id}/Messages")
                            .orderBy("time", Query.Direction.DESCENDING).limit(1).get().addOnSuccessListener { documents ->
                                for (document in documents) {

                                    val messageRef = db.collection("new/${userDocumentID}/Chats/${documentSnapshot.id}/Messages").document(document.id)
                                    messageRef.update("wasRead", true)
                                }
                            }
                        //getting to the messages and looping through all the messages
                        listenerRegistration = db.collection("new/${userDocumentID}/Chats/${documentSnapshot.id}/Messages")
                            .orderBy("time", Query.Direction.DESCENDING).limit(10)
                            .addSnapshotListener { snapshot, e ->
                                if (e != null) {
                                    Log.w(ContentValues.TAG, "Initial Listen failed.", e)
                                    return@addSnapshotListener
                                }

                                if (snapshot != null && !snapshot.isEmpty) {
                                    // Get the last visible item
                                    currentLastVisibleDocument = snapshot.documents.last()
                                    val lastVisibleForListener = snapshot.documents.last()

                                    // Update the query with the new end position
                                    val newQuery = db.collection("new/${userDocumentID}/Chats/${documentSnapshot.id}/Messages")
                                        .orderBy("time", Query.Direction.DESCENDING)
                                        .endAt(lastVisibleForListener)

                                    listenerRegistration.remove()
                                    listenerRegistration = newQuery.addSnapshotListener { newSnapshot, newE ->
                                        // Handle the new snapshot
                                        newSnapshot?.let {
                                            if (newE != null) {
                                                Log.w(ContentValues.TAG, "Listen failed.", newE)
                                                return@addSnapshotListener
                                            }

                                                val messageRef = db.collection("new/${userDocumentID}/Chats/${documentSnapshot.id}/Messages").document(
                                                    it.documents[0].id)
                                                messageRef.update("wasRead", true)


                                            CoroutineScope(Dispatchers.Main).launch {
                                                messageList.value =
                                                    it.toObjects(UserChatMessage::class.java)
                                            }

                                        }
                                        if (newSnapshot != null) {

                                            Log.d(
                                                ContentValues.TAG,
                                                "Current data: ${newSnapshot.documents}"
                                            )
                                        } else {
                                            Log.d(ContentValues.TAG, "Current data: null")
                                        }
                                    }
                                }


                            }
                    }
            } catch (e: FirebaseFirestoreException) {
                Log.d("error", "getDataFromFireStore: $e")
            }


    }
}