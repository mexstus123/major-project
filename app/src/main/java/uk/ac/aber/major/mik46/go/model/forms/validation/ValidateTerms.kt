package uk.ac.aber.major.mik46.go.model.forms.validation

class ValidateTerms {

    fun execute(acceptedTerms: Boolean): ValidationResult {
        if(!acceptedTerms) {
            return ValidationResult(
                successful = false,
                errorMessage = "Please accept the terms and conditions"
            )
        }
        return ValidationResult(
            successful = true
        )
    }
}