package uk.ac.aber.major.mik46.go.model

import android.icu.text.SimpleDateFormat
import com.google.firebase.firestore.GeoPoint
import java.util.*
import java.util.concurrent.TimeUnit


data class Product(
    var uploadTime: Date = Date(),
    var productID: String = "",
    var manufacturerID: String = "",
    var name: String = "",
    var description: String = "",
    var price: String = "",
    var _imageURI: String = "",
    var favouriteCount: Int = 0,
    var location: GeoPoint = GeoPoint(0.0,0.0),
    var rating: Double = 0.0,
    var minOrder: String = "0",
    var maxOrder: String = "1",
){

    var imageURI: String
        get() {
            return _imageURI.ifEmpty { "https://firebasestorage.googleapis.com/v0/b/golocaluk-29e43.appspot.com/o/Default%2FNoImageFound.png?alt=media&token=10ce5c24-52ec-46a1-b438-2d29ee5b5c91" }
        }
        set(value) {
            _imageURI = value
        }
}



