package uk.ac.aber.major.mik46.go.ui.screens

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.components.IconButtonGroup
import uk.ac.aber.major.mik46.go.ui.components.InnerNavigationScaffold
import uk.ac.aber.major.mik46.go.ui.navigation.Screen
import uk.ac.aber.major.mik46.go.ui.screens.signIn.SignInScreen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AccountSettingsScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()
    val isSignedIn by mainViewModel.isSignedIn.collectAsState()








    InnerNavigationScaffold(
        onClickBack = {
            navController.popBackStack()
        },
        title = stringResource(R.string.settings)
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            if (!isSignedIn) {
                SignInScreen(coroutineScope = coroutineScope, user = user, status = status , navController = navController, mainViewModel = mainViewModel)
            }else{
                AccountSettingsScreenContents(coroutineScope, user, mainViewModel, status,navController)
            }


        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AccountSettingsScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController) {

    val ctx = LocalContext.current

    val firstTextValue = rememberSaveable { mutableStateOf("") }

    val buttonListing = remember { createButtonsList(ctx,mainViewModel,navController) }
    val scrollState = rememberScrollState()


    val currentUser by mainViewModel.currentUser.observeAsState(User())


    Column(
            verticalArrangement = Arrangement.Top,
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(scrollState),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Spacer(modifier = Modifier.height(10.dp))

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 0.dp, 5.dp),
            text = stringResource(R.string.app_settings),
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )

            buttonListing .forEach {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(60.dp)
                        .clickable(onClick = it.onClick),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        modifier = Modifier
                            .weight(0.15F)
                            .fillMaxSize(0.5F)
                            .padding(horizontal = 10.dp),
                        imageVector = it.icon,
                        contentDescription = it.buttonText
                    )

                    if(it.additionalText.isEmpty()) {
                        Text(
                            modifier = Modifier
                                .weight(0.7F)
                                .fillMaxWidth(),
                            text = it.buttonText,
                            textAlign = TextAlign.Start,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 16.sp
                        )
                    }else{
                        Text(
                            modifier = Modifier
                                .weight(0.35F)
                                .fillMaxWidth(),
                            text = it.buttonText,
                            textAlign = TextAlign.Start,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 16.sp
                        )
                        Text(
                        modifier = Modifier
                            .weight(0.35F)
                            .fillMaxWidth(),
                        text = it.additionalText,
                        textAlign = TextAlign.End,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 14.sp,
                        color = MaterialTheme.colorScheme.outline
                        )
                    }
                    Icon(
                        modifier = Modifier
                            .weight(0.15F)
                            .fillMaxSize(0.5F)
                            .padding(horizontal = 10.dp),
                        imageVector = Icons.Default.KeyboardArrowRight,
                        contentDescription = "arrow ${it.buttonText}"
                    )
                }
            }

        }
}

private fun createButtonsList(context: Context, mainViewModel: MainViewModel, navController: NavHostController): List<IconButtonGroup> {

    return listOf(
        IconButtonGroup(
            Icons.Default.FormatPaint,
            "Appearance",
            additionalText = when(mainViewModel.isDarkTheme.value){
                true -> "Dark"
                false ->  "Light"
                else -> "Dynamic"
            },
            onClick = {
                navController.navigate(route = Screen.AppearanceSettings.route)
            }
        ),
        IconButtonGroup(
            Icons.Default.Accessibility,
            "Accessibility",
            onClick = {
                navController.navigate(route = Screen.AccessibilitySettings.route)
            }
        ), IconButtonGroup(
            Icons.Default.Language,
            "Language",
            additionalText = Locale.current.language.uppercase(),
            onClick = {
                navController.navigate(route = Screen.LanguageSettings.route)
            }
        ), IconButtonGroup(
            Icons.Default.NotificationImportant,
            "Notifications",
            additionalText = when(mainViewModel.userNotificationSetting.value){
                true -> "Enabled"
                false ->  "Disabled"

            },
            onClick = {
                navController.navigate(route = Screen.NotificationsSettings.route)
            }
        ),
    )
}









