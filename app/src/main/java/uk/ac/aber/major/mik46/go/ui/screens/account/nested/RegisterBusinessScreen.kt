package uk.ac.aber.major.mik46.go.ui.screens.account.nested

import android.widget.Toast
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.google.firebase.firestore.GeoPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.forms.editProfile.EditProfileEvent
import uk.ac.aber.major.mik46.go.model.forms.editProfile.EditProfileViewModel
import uk.ac.aber.major.mik46.go.ui.components.InnerNavigationScaffold
import uk.ac.aber.major.mik46.go.ui.navigation.Screen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun RegisterBusinessScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()
    val isSignedIn by mainViewModel.isSignedIn.collectAsState()
    val focusManager = LocalFocusManager.current








    InnerNavigationScaffold(
        onClickBack = {
            navController.popBackStack()
        },
        title = stringResource(R.string.upgrade)
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
                .pointerInput(Unit) { detectTapGestures(onTap = { focusManager.clearFocus() }) },
            color = MaterialTheme.colorScheme.background
        ) {


                RegisterBusinessScreenContents(coroutineScope, user, mainViewModel, status,navController)



        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RegisterBusinessScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController) {


    val editProfileViewModel: EditProfileViewModel = viewModel()


    val context = LocalContext.current

    val state = editProfileViewModel.state

    val isValidState = editProfileViewModel.isDescriptionValid

    LaunchedEffect(isValidState.value) {
        if (isValidState.value) {
            isValidState.value = false
            mainViewModel.currentUser.value!!.manufacturerAccount = true
            mainViewModel.currentUser.value!!.businessDescription = state.description

            mainViewModel.putCurrentUserData()
            navController.popBackStack(Screen.Account.route, false)

            Toast.makeText(
                context,
                "Register success",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    LaunchedEffect(key1 = Unit) {
        editProfileViewModel.setCurrentUser(mainViewModel.currentUser.value!!)
    }



    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 25.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(0.dp, 20.dp, 0.dp, 0.dp),
            text = "Business Information",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp,
            color = MaterialTheme.colorScheme.onBackground
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 10.dp, 0.dp),
            text = "Fill out the form below with the details of your business. To change more about your account go to the profile settings",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Normal,
            fontSize = 14.sp,
            color = MaterialTheme.colorScheme.outline
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 5.dp, 0.dp, 5.dp),
            text = stringResource(R.string.business_description),
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )

        TextField(
            value = state.description,
            onValueChange = {
                editProfileViewModel.onEvent(EditProfileEvent.DescriptionChanged(it))
            },
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            placeholder = {
                Text(text = "Enter Description Here...")
            },
            keyboardOptions = KeyboardOptions(
                capitalization = KeyboardCapitalization.Sentences,
            ),
            trailingIcon = {
                Text(
                    text = "${state.description.length}/2000",
                    color = MaterialTheme.colorScheme.outline.copy(alpha = 0.4F)
                    )
            },
            maxLines = 15
        )
        if (state.descriptionError != null) {
            Text(
                text = state.descriptionError,
                color = MaterialTheme.colorScheme.error,
                textAlign = TextAlign.End
            )
        }

        Spacer(modifier = Modifier.height(32.dp))

        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Checkbox(
                checked = state.acceptedTerms,
                onCheckedChange = {
                    editProfileViewModel.onEvent(EditProfileEvent.AcceptTerms(it))
                }
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Accept Business Terms Of Service")
        }
        if (state.termsError != null) {
            Text(
                text = state.termsError,
                color = MaterialTheme.colorScheme.error,
            )
        }

        Spacer(modifier = Modifier.height(32.dp))

        Button(
            onClick = {
                editProfileViewModel.onEvent(EditProfileEvent.Submit)

            },
            modifier = Modifier
                .width(250.dp),
        ) {
            Text(
                modifier = Modifier,
                text = "Register Business",
                fontWeight = FontWeight.SemiBold,
                textAlign = TextAlign.Center,
                fontSize = 18.sp,
            )
        }

    }
}










