package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InnerNavigationScaffold(
    onClickBack: () -> Unit = {},
    title: String = "",
    pageContent: @Composable (innerPadding: PaddingValues) -> Unit = {},




) {


    Scaffold(
        topBar = {
            InnerNavigationCenterAlignedTopBar(
                onClickBack = onClickBack
                ,
                title = title
            )

        },

        content = { innerPadding -> pageContent(innerPadding) },
        modifier = Modifier.wrapContentHeight()
    )
}
