package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SignInScaffold(
    title: String = "Sign In",
    pageContent: @Composable (innerPadding: PaddingValues) -> Unit = {},
) {


    Scaffold(
        topBar = {
            // the CenterAlignedTopAppBar composable displays the top app bar
            CenterAlignedTopAppBar(
                // specify the colors for the top app bar
                colors = TopAppBarDefaults.mediumTopAppBarColors(containerColor = MaterialTheme.colorScheme.surfaceVariant),
                // the title of the top app bar
                title = {
                    // display the app title text
                    Text(
                        text = title,
                        fontWeight = FontWeight.Bold,
                    )
                },
            )

        },

        content = { innerPadding -> pageContent(innerPadding) },
        modifier = Modifier.wrapContentHeight()
    )
}
