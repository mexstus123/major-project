package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage

@Composable
fun AccountProductRow(
    productName: String,
    pictureURI: String,
    onClick: () -> Unit = {},
    buttonText: String,
    isFavouritesList: Boolean = false
){
    var isFavourite by remember { mutableStateOf(true) }

    Row(
        modifier = if(isFavouritesList){
            Modifier
                .fillMaxWidth()
                .clickable(onClick = onClick)
        }else{
            Modifier
                .fillMaxWidth()
             },
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Column(Modifier.weight(0.15F, false)) {
            Spacer(modifier = Modifier.padding(vertical = 4.dp))
            Card(
                modifier = Modifier
                    .size(55.dp),
                shape = RoundedCornerShape(10.dp),
            ) {
                AsyncImage(
                    modifier = Modifier.fillMaxHeight(),
                    model = pictureURI,
                    contentDescription = "product_photo",
                    contentScale = ContentScale.FillBounds
                )
            }
            Spacer(modifier = Modifier.padding(vertical = 4.dp))
        }




        Text(
            modifier = Modifier
                .weight(0.55F)
                .padding(start = 15.dp, end = 15.dp),
            text = productName,
            fontWeight = FontWeight.Bold,
            fontSize = 16.sp,
            textAlign = TextAlign.Start
        )

        if(isFavouritesList){
            IconButton(
                onClick = {
                    //on click favourite or un favourite the item
                    isFavourite = !isFavourite

                    //TODO remove product from list & sync with db
                },
            ) {
                Icon(
                    //update the icon based on isfav var
                    imageVector = when (isFavourite) {
                        true -> {
                            Icons.Filled.Favorite
                        }
                        false -> {
                            Icons.Outlined.FavoriteBorder
                        }
                    },
                    //update the icon colour on isfav var
                    tint = when (isFavourite) {
                        true -> {
                            MaterialTheme.colorScheme.primary
                        }
                        false -> {
                            MaterialTheme.colorScheme.onBackground
                        }
                    },
                    contentDescription =  "$productName heart icon",
                    modifier = Modifier
                        .size(30.dp)
                        .weight(0.3F, false)
                )
            }

        }else {
            Button(
                onClick = onClick,
                modifier = Modifier
                    .wrapContentSize(),
            ) {
                Text(
                    modifier = Modifier
                        .weight(0.3F, false),
                    text = buttonText,
                    fontWeight = FontWeight.SemiBold,
                    textAlign = TextAlign.Center,
                    fontSize = 15.sp,
                )
            }
        }
    }
}