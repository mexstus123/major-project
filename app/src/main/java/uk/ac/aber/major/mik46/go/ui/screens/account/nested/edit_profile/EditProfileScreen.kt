package uk.ac.aber.major.mik46.go.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.components.IconButtonGroup
import uk.ac.aber.major.mik46.go.ui.components.InnerNavigationScaffold
import uk.ac.aber.major.mik46.go.ui.navigation.Screen
import uk.ac.aber.major.mik46.go.ui.screens.signIn.SignInScreen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun EditProfileScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()
    val isSignedIn by mainViewModel.isSignedIn.collectAsState()








    InnerNavigationScaffold(
        onClickBack = {
            navController.popBackStack()
        },
        title = stringResource(R.string.profile)
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
            color = MaterialTheme.colorScheme.background
        ) {
            if (!isSignedIn) {
                SignInScreen(coroutineScope = coroutineScope, user = user, status = status , navController = navController, mainViewModel = mainViewModel)
            }else{
                EditProfileScreenContents(coroutineScope, user, mainViewModel, status,navController)
            }


        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditProfileScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController) {


    val firstTextValue = rememberSaveable { mutableStateOf("") }

    val infoButtonList = remember { accountInfoButtonsList(mainViewModel,navController) }
    val managementButtonList = remember { accountManagementButtonsList(mainViewModel,navController) }
    val scrollState = rememberScrollState()


    val currentUser by mainViewModel.currentUser.observeAsState(User())


    Column(
            verticalArrangement = Arrangement.Top,
            modifier = Modifier
                .fillMaxSize()
                ,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Spacer(modifier = Modifier.height(10.dp))

        Box(modifier = Modifier
            .size(100.dp)
            .clickable {
                navController.navigate(Screen.EditProfilePicture.route)
            } ,
        contentAlignment = Alignment.BottomCenter){
            Card(
                modifier = Modifier
                    .size(100.dp),
                shape = RoundedCornerShape(125.dp),
            ) {
                AsyncImage(
                    modifier = Modifier.fillMaxSize(),
                    model = currentUser.profilePictureURI,
                    contentDescription = "profile_photo",
                    contentScale = ContentScale.FillBounds
                )
            }
            Row(modifier = Modifier
                .height(28.dp)
                .width(98.dp),
                horizontalArrangement = Arrangement.End
            ) {
                Card(
                    modifier = Modifier
                        .size(25.dp)
                    ,
                    shape = RoundedCornerShape(125.dp),
                    colors = CardDefaults.cardColors(
                        containerColor = MaterialTheme.colorScheme.outline,
                        contentColor = MaterialTheme.colorScheme.background
                    )
                ) {
                    Icon(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(4.dp),
                        imageVector = Icons.Default.Edit,
                        contentDescription = "edit"
                    )
                }
            }

        }


        infoButtonList.forEach {
                val hasExtraText = it.additionalText.isNotEmpty()
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(60.dp)
                        .clickable(onClick = it.onClick),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Spacer(modifier = Modifier.width(10.dp))
                    if (hasExtraText) {
                        Text(
                            modifier = Modifier
                                .weight(0.25F)
                                .fillMaxWidth(),
                            text = it.buttonText,
                            textAlign = TextAlign.Start,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 16.sp
                        )
                        Text(
                            modifier = Modifier
                                .weight(0.60F)
                                .fillMaxWidth(),
                            text = it.additionalText,
                            textAlign = TextAlign.End,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 14.sp,
                            color = MaterialTheme.colorScheme.outline
                        )
                    } else {
                        Text(
                            modifier = Modifier
                                .weight(0.85F)
                                .fillMaxWidth(),
                            text = it.buttonText,
                            textAlign = TextAlign.Start,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 16.sp
                        )
                    }

                    Icon(
                        modifier = Modifier
                            .weight(0.15F)
                            .fillMaxSize(0.5F),
                        imageVector = Icons.Default.KeyboardArrowRight,
                        contentDescription = "arrow ${it.buttonText}"
                    )
                }
            }

        Divider(modifier = Modifier.padding(top = 15.dp, bottom = 5.dp))

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 0.dp, 5.dp),
            text = "Account Management",
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )

        managementButtonList.forEach {
            val hasExtraText = it.additionalText.isNotEmpty()
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(60.dp)
                    .clickable(onClick = it.onClick),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Spacer(modifier = Modifier.width(10.dp))
                if (hasExtraText) {
                    Text(
                        modifier = Modifier
                            .weight(0.5F)
                            .fillMaxWidth(),
                        text = it.buttonText,
                        textAlign = TextAlign.Start,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 16.sp
                    )
                    Text(
                        modifier = Modifier
                            .weight(0.35F)
                            .fillMaxWidth(),
                        text = it.additionalText,
                        textAlign = TextAlign.End,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 14.sp,
                        color = MaterialTheme.colorScheme.outline
                    )
                } else {
                    Text(
                        modifier = Modifier
                            .weight(0.85F)
                            .fillMaxWidth(),
                        text = it.buttonText,
                        textAlign = TextAlign.Start,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 16.sp,
                        color = if(it.hasNavigation){
                            MaterialTheme.colorScheme.onBackground
                        }else{
                            MaterialTheme.colorScheme.error.copy(blue = 0.3F, green = 0.25F)
                        }
                    )
                }

                if(it.hasNavigation){
                    Icon(
                        modifier = Modifier
                            .weight(0.15F)
                            .fillMaxSize(0.5F),
                        imageVector = Icons.Default.KeyboardArrowRight,
                        contentDescription = "arrow ${it.buttonText}"
                    )
                }

            }
        }


        }
    }


private fun accountInfoButtonsList(mainViewModel: MainViewModel, navController: NavHostController): List<IconButtonGroup> {

    return listOf(
        IconButtonGroup(
            buttonText = "Username",
            additionalText = mainViewModel.currentUser.value!!.userName,
            onClick = {
                navController.navigate(Screen.EditUsername.route)
            }
        ),
        IconButtonGroup(
            buttonText = "Email",
            additionalText = mainViewModel.currentUser.value!!.emailAddress,
            onClick = {
                navController.navigate(Screen.EditEmail.route)
            }
        ), IconButtonGroup(
            buttonText = "Phone",
            onClick = {
                navController.navigate(Screen.EditPhone.route)
            }
        ), IconButtonGroup(
            buttonText = "Password",
            onClick = {
                navController.navigate(Screen.EditPassword.route)
            }
        ),
    )
}

private fun accountManagementButtonsList(mainViewModel: MainViewModel, navController: NavHostController): List<IconButtonGroup> {

    return listOf(

        IconButtonGroup(
            buttonText = "Blocked Users",
            additionalText = mainViewModel.currentUser.value!!.blockedUsers.size.toString(),
            onClick = {
                navController.navigate(Screen.BlockedUsers.route)
            }
        ),
        IconButtonGroup(
            buttonText = "Hidden Products",
            additionalText = mainViewModel.currentUser.value!!.hiddenProducts.size.toString(),
            onClick = {
                navController.navigate(Screen.HiddenProducts.route)
            }
        ),
        IconButtonGroup(
            buttonText = "Delete Account",
            onClick = {
            },
            hasNavigation = false
        ),
    )
}









