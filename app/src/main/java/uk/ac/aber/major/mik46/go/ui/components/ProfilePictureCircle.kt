package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import uk.ac.aber.major.mik46.go.model.User


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProfilePictureCircle(
    onClick: () -> Unit = {},
    profilePic: String = "", //TODO for now
    size: Int,
    modifier: Modifier = Modifier
) {
    Card(
        modifier = modifier
            .size(size.dp)
            .fillMaxHeight(0.4f),
        shape = RoundedCornerShape(125.dp),

        onClick = onClick,
    ) {
        AsyncImage(
            modifier = Modifier.fillMaxSize(),
            model = profilePic, //TODO fix to user rather than load from db
            contentDescription = "profile_photo",
            contentScale = ContentScale.FillBounds
        )


    }
}
