package uk.ac.aber.major.mik46.go.model.repositories

import android.content.ContentValues.TAG

import android.util.Log
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import com.google.firebase.messaging.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.tasks.await
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.UserChat
import uk.ac.aber.major.mik46.go.model.notifications.FirebaseNotificationService.Companion.oldToken
import uk.ac.aber.major.mik46.go.model.notifications.FirebaseNotificationService.Companion.token
import java.util.*


/**
 * Repository class responsible for handling data operations.
 * This is the mediator between the [UserWordDao] and the [UserWordsViewModel].
 *
 * @param application The application context
 */
class FirebaseAuthRepository(
    private val auth: FirebaseAuth = FirebaseAuth.getInstance(),
    private val mainRepository: MainRepository = MainRepository()
) : FirebaseAuthRepo {

    override val currentUser get() = auth.currentUser

    override suspend fun firebaseSignUpWithEmailAndPassword(email: String, password: String, username: String) {
        try {
            auth.createUserWithEmailAndPassword(email, password).await()

            currentUser?.let {
                it.updateProfile(UserProfileChangeRequest.Builder().setDisplayName(username).build()).await()
                mainRepository.userSetup(
                    User(
                        UID = it.uid,
                        userName = username,
                        emailAddress = email
                    )

                )
            }

        } catch (e: FirebaseAuthException) {
            Log.d("error", "firebaseSignUpWithEmailAndPassword: $e")
        }
    }

    override suspend fun sendEmailVerification() {
        try {
            auth.currentUser?.sendEmailVerification()?.await()
        } catch (e: FirebaseException) {
            Log.d("error", "sendEmailVerification: $e")
        }
    }

    override suspend fun firebaseSignInWithEmailAndPassword(email: String, password: String): Boolean {
        try {
            auth.signInWithEmailAndPassword(email, password).await()
            if(auth.currentUser?.email == email){
                return true
            }
            return false
        } catch (e: FirebaseAuthException) {
            Log.d("error", "firebaseSignInWithEmailAndPassword: $e")
            return false
        }
    }

    override suspend fun reloadFirebaseUser() {
        try {
            auth.currentUser?.reload()?.await()
        } catch (e: FirebaseAuthException) {
            Log.d("error", "reloadFirebaseUser: $e")
        }
    }

    override suspend fun sendPasswordResetEmail(email: String) {
        try {
            auth.sendPasswordResetEmail(email).await()
        } catch (e: FirebaseAuthException) {
            Log.d("error", "sendPasswordResetEmail: $e")
        }
    }

    override fun signOut() {
        try {
            auth.signOut()
        } catch (e: FirebaseAuthException) {
            Log.d("error", "signOut: $e")
        }
    }

    override suspend fun deleteCurrentUser() {
        try {
            auth.currentUser?.delete()?.await()
        } catch (e: FirebaseAuthException) {
            Log.d("error", "deleteCurrentUser: $e")
        }
    }

    override suspend fun isEmailVerified(): Boolean {
        return try {
            auth.currentUser?.isEmailVerified!!
        } catch (e: FirebaseException) {
            Log.d("error", "isEmailVerified: $e")
            false
        }
    }

    override suspend fun hasPhoneNumber(): Boolean {
        return try {
            if (auth.currentUser?.phoneNumber.isNullOrEmpty()){
                return false
            }
            true
        } catch (e: FirebaseAuthException) {
            Log.d("error", "isEmailVerified: $e")
            false
        }
    }

    override suspend fun usernameAlreadyExists(username: String): Boolean {
        return try {
            mainRepository.checkForUserByUserName(username)
        } catch (e: FirebaseAuthException) {
            Log.d("error", "usernameAlreadyExists: $e")
            false
        }
    }

    override suspend fun emailAlreadyExists(email: String): Boolean {
        return try {
            val signInMethods = auth.fetchSignInMethodsForEmail(email).await()
            if (signInMethods.signInMethods!!.isNotEmpty()){
                return true
            }
            false
        } catch (e: FirebaseAuthException) {
            Log.d("error", "emailAlreadyExists: $e")
            true
        }
    }

    override suspend fun phoneNumberAlreadyExists(phoneNumber: String): Boolean {
        return try {
            mainRepository.checkForUserByPhoneNumber(phoneNumber)
        } catch (e: FirebaseAuthException) {
            Log.d("error", "phoneNumberAlreadyExists: $e")
            false
        }

    }

}