package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.ui.theme.GoLocalUKTheme


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsButton(
    onClick: () -> Unit = {},
    size: Int = 60,
    modifier: Modifier = Modifier
){

    Surface(
        modifier = modifier
            .size(size.dp)
            .fillMaxHeight(0.4f)
            .padding(10.dp),
        shape = RoundedCornerShape(15.dp),
        onClick = onClick,
        color = MaterialTheme.colorScheme.outline
    ) {
        Icon(
            imageVector = Icons.Filled.Settings,
            contentDescription = stringResource(R.string.settings_button),
            // specify the tint color for the icon
            tint = Color.White,
            modifier = Modifier.fillMaxSize().padding(2.dp)
        )
    }
}

@Composable
@Preview
private fun SettingsButtonPreview() {
    GoLocalUKTheme(dynamicColor = false) {
        SettingsButton(onClick = {})
    }
}