package uk.ac.aber.major.mik46.go.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.components.InnerNavigationScaffold
import uk.ac.aber.major.mik46.go.ui.screens.signIn.SignInScreen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AppearanceSettingsScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()
    val isSignedIn by mainViewModel.isSignedIn.collectAsState()








    InnerNavigationScaffold(
        onClickBack = {
            navController.popBackStack()
        },
        title = stringResource(R.string.appearance)
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            if (!isSignedIn) {
                SignInScreen(coroutineScope = coroutineScope, user = user, status = status , navController = navController, mainViewModel = mainViewModel)
            }else{
                AppearanceSettingsScreenContents(coroutineScope, user, mainViewModel, status,navController)
            }


        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppearanceSettingsScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController) {



    val scrollState = rememberScrollState()


    val themeState by mainViewModel.isDarkTheme.observeAsState()


    Column(
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Spacer(modifier = Modifier.height(10.dp))

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 0.dp, 10.dp),
            text = stringResource(R.string.colour_scheme),
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 16.sp,
            color = MaterialTheme.colorScheme.outline
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(bottom = 5.dp),

            verticalAlignment = Alignment.CenterVertically,
        ) {

            Text(
                modifier = Modifier
                    .weight(0.8F)
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp),
                text = "Use System Preferences",
                textAlign = TextAlign.Start,
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp,
            )


            //dynamic
            RadioButton(
                selected = when (themeState) {
                    true -> false
                    false -> false
                    else -> true
                },
                onClick = { mainViewModel.isDarkTheme.value = null },
                modifier = Modifier
                    .weight(0.20F)
                    .fillMaxSize()
                    .padding(horizontal = 10.dp),
            )
        }

        Spacer(modifier = Modifier.height(10.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(bottom = 5.dp),

            verticalAlignment = Alignment.CenterVertically,
        ) {

            Text(
                modifier = Modifier
                    .weight(0.8F)
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp),
                text = "Always Light Themed",
                textAlign = TextAlign.Start,
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp,
            )


            //light
            RadioButton(
                selected = when (themeState) {
                    true -> false
                    false -> true
                    else -> false
                },
                onClick = { mainViewModel.isDarkTheme.value = false },
                modifier = Modifier
                    .weight(0.20F)
                    .fillMaxSize()
                    .padding(horizontal = 10.dp),
            )
        }

        Spacer(modifier = Modifier.height(10.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(bottom = 5.dp),

            verticalAlignment = Alignment.CenterVertically,
        ) {

            Text(
                modifier = Modifier
                    .weight(0.8F)
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp),
                text = "Always Dark Themed",
                textAlign = TextAlign.Start,
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp,
            )


            //dark
            RadioButton(
                selected = when (themeState) {
                    true -> true
                    false -> false
                    else -> false
                },
                onClick = { mainViewModel.isDarkTheme.value = true },
                modifier = Modifier
                    .weight(0.20F)
                    .fillMaxSize()
                    .padding(horizontal = 10.dp),
            )
        }


//        Row(
//            modifier = Modifier
//                .fillMaxWidth()
//                .wrapContentSize(),
//            verticalAlignment = Alignment.CenterVertically,
//        ) {
//
//            Text(
//                modifier = Modifier
//                    .weight(0.8F)
//                    .fillMaxWidth()
//                    .padding(horizontal = 10.dp),
//                text = description,
//                textAlign = TextAlign.Start,
//                fontWeight = FontWeight.SemiBold,
//                fontSize = 14.sp,
//                color = MaterialTheme.colorScheme.outline
//            )
//
//            Spacer(modifier = Modifier.weight(0.20F))
//        }
//    }


        Spacer(modifier = Modifier.height(10.dp))

    }
}