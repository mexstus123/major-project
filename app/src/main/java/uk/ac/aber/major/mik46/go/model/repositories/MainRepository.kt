package uk.ac.aber.major.mik46.go.model.repositories

import android.content.ContentValues.TAG

import android.util.Log
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.Query
import com.google.firebase.messaging.Constants
import kotlinx.coroutines.tasks.await
import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.notifications.FirebaseNotificationService.Companion.oldToken
import uk.ac.aber.major.mik46.go.model.notifications.FirebaseNotificationService.Companion.token
import java.util.*


/**
 * Repository class responsible for handling data operations.
 * This is the mediator between the [UserWordDao] and the [UserWordsViewModel].
 *
 * @param application The application context
 */
class MainRepository() : MainRepo{




    var db = FirebaseFirestore.getInstance()


    override suspend fun getUserFromFireStoreByUID(UID: String): User {
        var user = User()
        try {
            db.collection("new").whereEqualTo("UID", UID).get().await().map {
                val result = it.toObject(User::class.java)
                user = result
            }
        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "getting UID: $e")
        }
        return user
    }

    override suspend fun checkForUserByUID(UID: String): Boolean {

            return try {
            !db.collection("new").whereEqualTo("UID", UID).get().await().isEmpty
        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "checking UID: $e")
            false
        }
    }

    override suspend fun checkForUserByUserName(userName: String): Boolean {

        return try {
            !db.collection("new").whereEqualTo("userNameToCheck", userName.lowercase(Locale.ROOT)).get().await().isEmpty
        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "checking userName: $e")
            false
        }
    }

    override suspend fun checkForUserByPhoneNumber(phoneNumber: String): Boolean {

        return try {
            !db.collection("new").whereEqualTo("phoneNumber", phoneNumber).get().await().isEmpty
        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "checking phoneNumber: $e")
            false
        }
    }

    private suspend fun addNewUser(user: User, currentToken: String) {
        try {
            val userToDB = hashMapOf(
                "UID" to user.UID,
                "profilePictureURI" to user.profilePictureURI,
                "userName" to user.userName,
                "manufacturerAccount" to user.manufacturerAccount,
                "regTokens" to mapOf("token0" to currentToken),
                "blockedUsers" to emptyList<Map<String, String>>(),
                "hiddenProducts" to emptyList<Map<String, String>>(),
                "followedUsers" to emptyList<Map<String, String>>(),
                "favouritedProducts" to emptyList<Map<String, String>>(),
                "phoneNumber" to user.phoneNumber
            )
            db.collection("new")
                .add(userToDB)
                .addOnSuccessListener { documentReference ->
                    Log.d(
                        TAG,
                        "Current user login added with ID: ${documentReference.id}"
                    )
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error adding user document", e)
                }

        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "failed to add new user: $e")
        }
    }

    override suspend fun userSetup(user: User) {

        val currentToken = token!!

        //check if the users reg token has been updated
        if (!oldToken.isNullOrBlank()){
            removeRegistrationToken(user, oldToken!!)
        }

        //checking if the user is in the database already by UID
        if (!checkForUserByUID(user.UID)) {
            addNewUser(user, currentToken)
        }
        else{
            // when the user is in the database we need to check his db registration tokens against his current reg token
            addTokenToDB(user, currentToken)
        }
    }

    private suspend fun addTokenToDB(user: User, currentToken: String){
        val userDocumentID = getUserDocumentID(user.UID)
        val documentRef = db.collection("new").document(userDocumentID)

        val userRegTokens: Map<*, *>?  = documentRef.get().await()["regTokens"] as Map<*, *>?

        if (userRegTokens == null){
            try {
                val newMap = mapOf("token0" to currentToken)
                documentRef.update("regTokens", newMap)
                    .addOnSuccessListener {
                        Log.d(
                            TAG,
                            "DocumentSnapshot successfully updated!"
                        )
                    }
                    .addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }

            }catch (e: FirebaseFirestoreException) {
                Log.d("error", "Failed adding user first ever token: $e")
            }
        }else
            if (!userRegTokens!!.values.contains(currentToken)){
                val newMap = userRegTokens!!.toMutableMap()
                newMap["token${userRegTokens!!.size}"] = currentToken
                try {
                    documentRef.update("regTokens", newMap.toMap())
                        .addOnSuccessListener {
                            Log.d(
                                TAG,
                                "DocumentSnapshot successfully updated!"
                            )
                        }
                        .addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }

                }catch (e: FirebaseFirestoreException) {
                    Log.d("error", "Failed adding a new reg token to database: $e")
                }
            }
    }

    private suspend fun getUserDocumentID(UID: String): String{
        var userDocumentID = ""

        try {
            //getting the user id by email
            db.collection("new").whereEqualTo("UID", UID).get().await().forEach {
                userDocumentID = it.id
            }
        }catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
        return userDocumentID
    }



    override suspend fun updateUsersChats(user: User): List<UserChat> {
        val usersChats = mutableListOf<UserChat>()

        val userDocumentID = getUserDocumentID(user.UID)

        try {
            if (userDocumentID.isNotEmpty()) {

                //nested loop to get all the messages within the chats
                db.collection("new/${userDocumentID}/Chats").get().await()
                    .forEach { documentSnapshot ->
                        //emptying variables on each loop
                        var lastMessage = ""
                        var time = Date()
                        var lastMessageWasRead = true

                        //getting to the most recent message from the chat
                        db.collection("new/${userDocumentID}/Chats/${documentSnapshot.id}/Messages")
                            .orderBy("time", Query.Direction.DESCENDING).limit(1).get().await()
                            .forEachIndexed { index, message ->
                                Log.d(Constants.TAG, "chats loaded: $index")
                                //checking for null
                                if (index == 0 && message != null) {
                                    if (message.get("image") as Boolean){ //this check stops the uri of an image being displayed
                                        lastMessage = "Image"
                                    }else{
                                        lastMessage = message.get("content")!! as String
                                    }
                                    time = message.getTimestamp("time")!!.toDate()
                                    lastMessageWasRead = message.get("wasRead")!! as Boolean
                                }
                            }

                        val receiver = documentSnapshot.get("receiverUID")!! as String

                        usersChats.add(
                            UserChat(
                                receiver = getUserFromFireStoreByUID(receiver),
                                lastMessage = lastMessage,
                                LastMessageTime = time,
                                wasLastMessageRead = lastMessageWasRead
                            )
                        )
                    }
                usersChats.sortBy { it.LastMessageTime }
                usersChats.reverse()

            }
        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
        return usersChats.toList()
    }

    override suspend fun getProductsByUID(UID: String) : List<Product>{

        val userDocID = getUserDocumentID(UID)

        var productsList = listOf<Product>()
        try {
            productsList = db.collection("new/$userDocID/Products").orderBy("uploadTime", Query.Direction.DESCENDING).get().await()
                .toObjects(Product::class.java)

        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
        return productsList.toList()

    }

    override suspend fun getAllManufacturers(): List<User> {
        var manufacturers = mutableListOf<User>()



        try {
            manufacturers = db.collection("new").whereEqualTo("manufacturerAccount", true).get().await()
                .toObjects(User::class.java)

        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
        return manufacturers.toList()
    }

    override suspend fun putCurrentUserData(user: User) {

        val userToDB = hashMapOf(
            "UID" to user.UID,
            "profilePictureURI" to user.profilePictureURI,
            "userName" to user.userName,
            "manufacturerAccount" to user.manufacturerAccount,
            "blockedUsers" to user.blockedUsers,
            "hiddenProducts" to user.hiddenProducts,
            "followedUsers" to user.followedUsers,
            "favouritedProducts" to user.favouritedProducts,
            "businessLocation" to user.businessLocation,
            "businessDescription" to user.businessDescription,
            "phoneNumber" to user.phoneNumber
        )

        val userDocumentID = getUserDocumentID(user.UID)

        try {
            if (userDocumentID.isNotEmpty()) {
                db.document("new/${userDocumentID}").update(
                    userToDB
                )
            }
        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
    }


    override suspend fun removeRegistrationToken(user: User, oldToken: String){
        if (user.UID.isEmpty()){
            return
        }

        val currentToken = when(oldToken.isBlank()){
            true -> token!!
            false -> oldToken
        }
        lateinit var documentRef: DocumentReference

        val databaseUser = db.collection("new").whereEqualTo("UID", user.UID).get().await()

        if (databaseUser.isEmpty){
            return
        }

        databaseUser.forEach{
            documentRef = db.document("new/${it.id}")
        }
        var keyForToken = ""

        try {
            // Remove a key-value pair from the map
            val updatedMap = mutableMapOf<String, String>()
            updatedMap.putAll(documentRef.get().await()["regTokens"] as Map<String, String>)

            updatedMap.forEach { (key, value) ->
                if (value == currentToken) {
                    keyForToken = key
                }
            }

            updatedMap.remove(keyForToken)

// Update the document with the updated map value
            documentRef.update("regTokens", updatedMap)
                .addOnSuccessListener {
                    Log.d(
                        TAG,
                        "DocumentSnapshot successfully updated!"
                    )
                }
                .addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }

        }catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
    }

}