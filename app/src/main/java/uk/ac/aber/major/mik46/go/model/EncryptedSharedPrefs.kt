package uk.ac.aber.major.mik46.go.model

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel

//Function to get the keys
fun getEncryptedSharedPreferences(context: Context): SharedPreferences {

    val masterKey = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)

    return EncryptedSharedPreferences.create(
        "User3",
        masterKey,
        context,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )
}

fun getAllUsersSettings(ctx: Context, mainViewModel: MainViewModel){

    mainViewModel.isDarkTheme.value = when(getEncryptedSharedPreferences(ctx).getString("theme", "system")){
        "dark" -> true
        "light" -> false
        else -> null
    }
    mainViewModel.userShowLocationSetting.value = getEncryptedSharedPreferences(ctx).getBoolean("showLocation", true)
    mainViewModel.userLanguageSetting.value = getEncryptedSharedPreferences(ctx).getString("language", "system")!!
    mainViewModel.userTextSizeSetting.value = getEncryptedSharedPreferences(ctx).getString("textSize", "system")!!
    mainViewModel.userNotificationSetting.value = getEncryptedSharedPreferences(ctx).getBoolean("notification", true)
    mainViewModel.userMessageCensorshipSetting.value = getEncryptedSharedPreferences(ctx).getBoolean("messageCensorship", false)
    mainViewModel.userUnknownUserMessagesSetting.value = getEncryptedSharedPreferences(ctx).getBoolean("unknownUserMessages", false)

    Log.d("getting shared prefs", "\nuserUnknownUserMessagesSetting: ${mainViewModel.userUnknownUserMessagesSetting.value}\nuserMessageCensorshipSetting: ${mainViewModel.userMessageCensorshipSetting.value}\nuserShowLocationSetting: ${mainViewModel.userShowLocationSetting.value}")

}

fun setAllUsersSettings(ctx: Context, mainViewModel: MainViewModel){
    getEncryptedSharedPreferences(ctx).edit()
        .putString("theme",
            when(mainViewModel.isDarkTheme.value){
                 true -> "dark"
                false ->  "light"
                else -> "system"
            }
        )
        .putBoolean("showLocation", mainViewModel.userShowLocationSetting.value?: true)
        .putString("language", mainViewModel.userLanguageSetting.value?: "system")
        .putString("textSize", mainViewModel.userTextSizeSetting.value?: "system")
        .putBoolean("notification", mainViewModel.userNotificationSetting.value?: true)
        .putBoolean("messageCensorship", mainViewModel.userMessageCensorshipSetting.value?: false)
        .putBoolean("unknownUserMessages", mainViewModel.userUnknownUserMessagesSetting.value?: false)
        .apply()

    Log.d("getting shared prefs", "\nuserUnknownUserMessagesSetting: ${mainViewModel.userUnknownUserMessagesSetting.value}\nuserMessageCensorshipSetting: ${mainViewModel.userMessageCensorshipSetting.value}\nuserShowLocationSetting: ${mainViewModel.userShowLocationSetting.value}")
}

fun setUserRegistrationInfo(ctx: Context, username: String, password: String, email: String){
    getEncryptedSharedPreferences(ctx).edit()
        .putString("password",password)
        .putString("username",username)
        .putString("email",email)

        .apply()
}

