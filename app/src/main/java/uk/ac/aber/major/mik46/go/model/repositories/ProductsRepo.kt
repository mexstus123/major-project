package uk.ac.aber.major.mik46.go.model.repositories

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.GeoPoint
import uk.ac.aber.major.mik46.go.model.*
import java.util.*

interface ProductsRepo {

    suspend fun getProductsByQuery(queryCondition: ProductQueryType, limit: Long, usersLocation: GeoPoint = GeoPoint(0.0,0.0)): List<Product>

    suspend fun getUserFromFireStoreByUID(UID: String): User

}