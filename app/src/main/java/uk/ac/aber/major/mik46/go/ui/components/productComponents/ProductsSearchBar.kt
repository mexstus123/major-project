package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductsSearchBar(
    searchWord: MutableState<String>,
    onClick: () -> Unit,
    status: ConnectivityObserver.Status,
    modifier: Modifier
){
    TextField(
        textStyle = LocalTextStyle.current.copy(fontSize = 15.sp, color = MaterialTheme.colorScheme.onBackground),
        enabled = status == ConnectivityObserver.Status.Available,
        value = when(status != ConnectivityObserver.Status.Available) {
            true -> "Network ${status.name}"
            false -> searchWord.value
        },
        onValueChange = {
//            if (searchWord.value.length <= 30){
                searchWord.value = it
            //}
                        },
        modifier = modifier
            .padding(0.dp).clickable(onClick = onClick),
        shape = RoundedCornerShape(20.dp),
        placeholder = { Text(text = stringResource(id =R.string.search ), fontSize = 15.sp, color = Color(0xFFC1C9BF)) },
        colors = TextFieldDefaults.textFieldColors(
            containerColor = when(status != ConnectivityObserver.Status.Available) {
                true -> MaterialTheme.colorScheme.outline
                false -> MaterialTheme.colorScheme.outline.copy(alpha = 0.10F)
            }, // onSurface colour- to not change on theme change
            focusedIndicatorColor =  Color.Transparent, //hide the indicator
            unfocusedIndicatorColor = Color.Transparent,
            textColor = Color(0xFF191C19),
            disabledTextColor = MaterialTheme.colorScheme.error,
            disabledIndicatorColor = Color.Transparent,
            disabledPlaceholderColor = Color.Transparent
        ),
        keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Sentences),
        singleLine = true,
        trailingIcon = { //emoji button

            IconButton(onClick = {}) {
                Icon(
                    imageVector = Icons.Filled.Search,
                    //tint = MaterialTheme.colorScheme.onBackground,
                    contentDescription = stringResource(R.string.send_icon),
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(2.dp,0.dp,8.dp,0.dp)

                    ,
                    tint = MaterialTheme.colorScheme.inversePrimary
                )
            }
        }
    )
}