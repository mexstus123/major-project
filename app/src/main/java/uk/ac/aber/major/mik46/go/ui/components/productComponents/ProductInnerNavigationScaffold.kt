package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel

import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.navigation.Screen

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductInnerNavigationScaffold(
    status: ConnectivityObserver.Status,
    mainViewModel: MainViewModel,
    navController: NavHostController,
    user: User,
    pageContent: @Composable (innerPadding: PaddingValues) -> Unit = {},


    ) {


    Scaffold(
        topBar = {
            ProductPageTopBar(
                onClickBack = {
                    navController.popBackStack()
                },
                onClickChat = {
                    navController.navigate(Screen.Messaging.route) {
                        if (navController.backQueue.size >= 4) {
                            popUpTo(Screen.Messaging.route) {
                                saveState = true
                            }
                        }
                        launchSingleTop = true
                    }
                },
                onClickSettings = {}
            )//TODO sort out on clicks

        },

        content = { innerPadding -> pageContent(innerPadding) },
        modifier = Modifier.wrapContentHeight()
    )
}
