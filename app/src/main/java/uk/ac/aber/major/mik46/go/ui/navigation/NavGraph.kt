package uk.ac.aber.major.mik46.go.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.AuthenticationViewModel
import uk.ac.aber.major.mik46.go.ui.screens.*
import uk.ac.aber.major.mik46.go.ui.screens.account.nested.RegisterBusinessScreen
import uk.ac.aber.major.mik46.go.ui.screens.account.nested.UpgradeToBusinessScreen
import uk.ac.aber.major.mik46.go.ui.screens.signIn.RegistrationScreen
import uk.ac.aber.major.mik46.go.ui.screens.signIn.ValidateEmailScreen
import uk.ac.aber.major.mik46.go.ui.screens.signIn.ValidatePhoneScreen

@Composable
fun BuildNavigationGraph(user: User, mainViewModel: MainViewModel, status: ConnectivityObserver.Status,authenticationViewModel: AuthenticationViewModel){

    val navController = rememberNavController()


    NavHost(
        navController = navController,
        startDestination = Screen.Products.route
    ){
        composable(Screen.Products.route){ ProductsScreen(navController,user,mainViewModel,status) }
        composable(Screen.Chats.route){ ChatsScreen(navController,user,mainViewModel,status) }
        composable(Screen.ProductPage.route){ ProductPage(navController,user,mainViewModel,status) }
        composable(Screen.ManufacturerPage.route){ ManufacturerPage(navController,user,mainViewModel,status) }
        composable(Screen.QueryList.route){ QueryListScreen(navController,user,mainViewModel,status) }
        composable(Screen.Map.route){ MapScreen(navController,user,mainViewModel,status) }
        composable(Screen.Messaging.route){ MessagingScreen(navController,user,mainViewModel,status) }
        composable(Screen.Registration.route){ RegistrationScreen(navController,user,mainViewModel,status,authenticationViewModel) }
            composable(Screen.ValidateEmail.route){ ValidateEmailScreen(navController,user,mainViewModel,status,authenticationViewModel) }
            composable(Screen.ValidatePhone.route){ ValidatePhoneScreen(navController,user,mainViewModel,status,authenticationViewModel) }


        composable(Screen.Account.route){ AccountScreen(navController,user,mainViewModel,status) }
            composable(Screen.EditProfile.route){ EditProfileScreen(navController,user,mainViewModel,status) }
                composable(Screen.EditProfilePicture.route){ EditProfilePictureScreen(navController,user,mainViewModel,status)}
                composable(Screen.EditUsername.route){ EditUsernameScreen(navController,user,mainViewModel,status)}
                composable(Screen.EditEmail.route){ EditEmailScreen(navController,user,mainViewModel,status)}
                composable(Screen.EditPhone.route){ EditPhoneNumberScreen(navController,user,mainViewModel,status)}
                composable(Screen.EditPassword.route){ EditPasswordScreen(navController,user,mainViewModel,status)}
                composable(Screen.BlockedUsers.route){ BlockedUsersScreen(navController,user,mainViewModel,status)}
                composable(Screen.HiddenProducts.route){ HiddenProductsScreen(navController,user,mainViewModel,status)}

            composable(Screen.UpgradeToBusiness.route){ UpgradeToBusinessScreen(navController,user,mainViewModel,status) }
                composable(Screen.RegisterBusiness.route){ RegisterBusinessScreen(navController,user,mainViewModel,status) }
            composable(Screen.MyBusiness.route){ MyBusinessScreen(navController,user,mainViewModel,status) }
            composable(Screen.Favourites.route){ FavouritesScreen(navController,user,mainViewModel,status) }
            composable(Screen.Following.route){ FollowingScreen(navController,user,mainViewModel,status) }
            composable(Screen.PrivacyAndSafety.route){ PrivacyAndSafetyScreen(navController,user,mainViewModel,status) }
            composable(Screen.AccountSettings.route){ AccountSettingsScreen(navController,user,mainViewModel,status) }
                composable(Screen.AppearanceSettings.route){ AppearanceSettingsScreen(navController,user,mainViewModel,status) }
                composable(Screen.AccessibilitySettings.route){ AccessibilitySettingsScreen(navController,user,mainViewModel,status) }
                composable(Screen.LanguageSettings.route){ LanguageSettingsScreen(navController,user,mainViewModel,status) }
                composable(Screen.NotificationsSettings.route){ NotificationsSettingsScreen(navController,user,mainViewModel,status) }

            composable(Screen.Support.route){ SupportScreen(navController,user,mainViewModel,status) }
            composable(Screen.WhatsNew.route){ WhatsNewScreen(navController,user,mainViewModel,status) }
            composable(Screen.About.route){ AboutScreen(navController,user,mainViewModel,status) }
                composable(Screen.SupportTheDev.route){ SupportTheDevScreen(navController,user,mainViewModel,status) }
    }
}