package uk.ac.aber.major.mik46.go.model.forms.editProfile

sealed class EditProfileEvent {
    data class UserNameChanged(val userName: String) : EditProfileEvent()
    data class EmailChanged(val email: String) : EditProfileEvent()
    data class PhoneNumberChanged(val phoneNumber: String) : EditProfileEvent()
    data class PasswordChanged(val password: String) : EditProfileEvent()
    data class RepeatedPasswordChanged(val repeatedPassword: String) : EditProfileEvent()
    data class DescriptionChanged(val description: String) : EditProfileEvent()


    data class AcceptTerms(val isAccepted: Boolean) : EditProfileEvent()

    object Submit: EditProfileEvent()
}
