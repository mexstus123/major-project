package uk.ac.aber.major.mik46.go.ui.components

import android.content.ContentValues
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.MediaStore
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.material3.CardDefaults.cardColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.FileProvider
import coil.compose.AsyncImage
import uk.ac.aber.major.mik46.go.model.User
import java.io.File
import java.io.FileOutputStream


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MessageBubble(
    onClick: () -> Unit = {},
    messageContent: String = "",
    messageTime: String = "",
    messageSender: String = "",
    user: User,
    isMessageImage: Boolean = false,
    showTimeState: MutableState<Boolean> = remember{
        mutableStateOf(false)
    }

) {
    val isUser = user.emailAddress != messageSender

    val ctx = LocalContext.current

    Column(Modifier.wrapContentSize()) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = when (isUser) {
                true -> {
                    Arrangement.Start
                }
                false -> {
                    Arrangement.End
                }
            }
        ) {
            if (!isUser) {
                Spacer(modifier = Modifier.weight(0.4F))
            }

            Card(
                modifier = Modifier.weight(0.6F, false),
                shape = RoundedCornerShape(20.dp),
                onClick = {
                    showTimeState.value = !showTimeState.value
                          },
                colors = when (isUser) {
                    true -> {
                        cardColors(
                            MaterialTheme.colorScheme.primaryContainer,
                            MaterialTheme.colorScheme.onPrimaryContainer
                        )
                    }
                    false -> {
                        cardColors(
                            MaterialTheme.colorScheme.secondaryContainer,
                            MaterialTheme.colorScheme.onSecondaryContainer
                        )
                    }
                }

            ) {
                if (isMessageImage) {

                    Card(
                        modifier = Modifier
                            .wrapContentSize()
                            .padding(7.dp),
                        shape = RoundedCornerShape(16.dp),
                    ) {
                        AsyncImage(
                            modifier = Modifier.fillMaxWidth(),
                            model = messageContent,
                            contentDescription = "message image $messageContent",
                            contentScale = ContentScale.FillWidth)

                        }
                }else {
                    Text(
                        modifier = Modifier.padding(10.dp),
                        text = messageContent
                    )
                }
            }
            if (isUser) {
                Spacer(modifier = Modifier.weight(0.4F))
            }

        }
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = when (isUser) {
                true -> {
                    Arrangement.Start
                }
                false -> {
                    Arrangement.End
                }
            }
        ) {
            if (!isUser) {
                Spacer(modifier = Modifier.weight(0.4F))
            }

            Card(
                modifier = Modifier.weight(0.6F, false),
                colors = when (showTimeState.value) {
                    true -> {
                        cardColors(
                            MaterialTheme.colorScheme.background,
                            MaterialTheme.colorScheme.onBackground
                        )
                    }
                    false -> {
                        cardColors(
                            MaterialTheme.colorScheme.background,
                            MaterialTheme.colorScheme.background
                        )
                    }
                }

            ) {
                Text(
                    modifier = Modifier.padding(horizontal = 10.dp),
                    text = messageTime,
                    fontSize = 11.sp
                )

            }
        }

    }
}

