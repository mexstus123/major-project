package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.setAllUsersSettings

@Composable
 fun SettingsSwitchRow(
    title: String,
    description: String,
    switchState: MutableState<Boolean>,
    mainViewModel: MainViewModel,
    type: String = ""
){
    val ctx = LocalContext.current
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(bottom = 5.dp),

        verticalAlignment = Alignment.CenterVertically,
    ) {

        Text(
            modifier = Modifier
                .weight(0.8F)
                .fillMaxWidth()
                .padding(horizontal = 10.dp),
            text = title,
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 19.sp,
        )



        Switch(
            checked = switchState.value,
            onCheckedChange = {
                switchState.value = it
//                when(type){
//                    "notifications" -> mainViewModel.userNotificationSetting.value = it
//                    //"accessibility" ->
//                }


                CoroutineScope(Dispatchers.IO).launch {
                    setAllUsersSettings(ctx,mainViewModel)
                }
            },
            modifier = Modifier
                .weight(0.20F)
                .fillMaxSize()
                .padding(horizontal = 10.dp),
            colors = if(isSystemInDarkTheme()){
                SwitchDefaults.colors(
                    checkedThumbColor = MaterialTheme.colorScheme.onBackground,
                    checkedBorderColor = MaterialTheme.colorScheme.primary,
                    checkedTrackColor = MaterialTheme.colorScheme.surfaceVariant,
                )
            }else{
                SwitchDefaults.colors(

                )
            }
        )
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentSize(),
        verticalAlignment = Alignment.CenterVertically,
    ) {

        Text(
            modifier = Modifier
                .weight(0.8F)
                .fillMaxWidth()
                .padding(horizontal = 10.dp),
            text = description,
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 14.sp,
            color = MaterialTheme.colorScheme.outline
        )

        Spacer(modifier = Modifier.weight(0.20F))
    }
}