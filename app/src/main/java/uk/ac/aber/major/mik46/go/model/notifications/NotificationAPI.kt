package uk.ac.aber.major.mik46.go.model.notifications


import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import uk.ac.aber.major.mik46.go.model.notifications.NotificationConstants.Companion.CONTENT_TYPE
import uk.ac.aber.major.mik46.go.model.notifications.NotificationConstants.Companion.SERVER_KEY

interface NotificationAPI {

    @Headers("Authorization: key=$SERVER_KEY", "Content-Type:$CONTENT_TYPE")
    @POST("fcm/send")
    suspend fun postNotification(
        @Body notification: PushNotification
    ): Response<ResponseBody>
}