package uk.ac.aber.major.mik46.go.ui.components

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import com.google.firebase.firestore.GeoPoint
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel

fun checkUsersLocationPermission(ctx: Context, mainViewModel: MainViewModel){

    if (ContextCompat.checkSelfPermission(
            ctx,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
        ContextCompat.checkSelfPermission(
            ctx,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    ) {
        mainViewModel.setLocationPermission(true)
    }
}

fun getUsersLastKnownLocation(ctx: Context, mainViewModel: MainViewModel) {

    // initialize FusedLocationProviderClient
    val fusedLocationClient = LocationServices.getFusedLocationProviderClient(ctx)

    if (ActivityCompat.checkSelfPermission(
                ctx,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                ctx,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            mainViewModel.setLocationPermission(false)
            return
        }

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location ->
                if (location != null) {
                    mainViewModel.setLocationPermission(true)
//                    val geoPointTemp = GeoPoint(location.latitude,location.longitude)
                    mainViewModel.setCurrentUserLocation(GeoPoint(location.latitude,location.longitude))
                }
            }

}