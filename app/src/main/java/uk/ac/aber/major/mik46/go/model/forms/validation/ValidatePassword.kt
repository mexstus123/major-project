package uk.ac.aber.major.mik46.go.model.forms.validation

class ValidatePassword{


    fun executeRegistration(password: String): ValidationResult {
        if(password.length < 8) {
            return ValidationResult(
                successful = false,
                errorMessage = "Password needs to consist of at least 8 characters"
            )
        }
        val containsLettersAndDigits = password.any { it.isDigit() } &&
                password.any { it.isLetter() }
        if(!containsLettersAndDigits) {
            return ValidationResult(
                successful = false,
                errorMessage = "Password needs to contain at least one letter and digit"
            )
        }

        return ValidationResult(
            successful = true
        )
    }

    fun executeSignIn(password: String): ValidationResult {
        if(password.length < 8) {
            return ValidationResult(
                successful = false,
                errorMessage = "Incorrect Password"
            )
        }
        val containsLettersAndDigits = password.any { it.isDigit() } &&
                password.any { it.isLetter() }
        if(!containsLettersAndDigits) {
            return ValidationResult(
                successful = false,
                errorMessage = "Incorrect Password"
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}