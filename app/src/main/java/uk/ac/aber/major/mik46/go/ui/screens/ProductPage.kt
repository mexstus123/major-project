package uk.ac.aber.major.mik46.go.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.viewmodels.ProductsViewModel
import uk.ac.aber.major.mik46.go.ui.components.*
import uk.ac.aber.major.mik46.go.ui.navigation.Screen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ProductPage(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, productsViewModel: ProductsViewModel = viewModel()){


    LaunchedEffect(key1 = Unit){
        productsViewModel.getManufacturer(mainViewModel.currentViewedProduct.value!!.manufacturerID)
    }

    val coroutineScope = rememberCoroutineScope()
    val manufacturer by productsViewModel.manufacturer.observeAsState(User())
    mainViewModel.manufacturer.value = manufacturer

    ProductInnerNavigationScaffold(
        navController = navController,
        status = status,
        mainViewModel = mainViewModel,
        user = user,
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            ProductPageContents(coroutineScope, user, mainViewModel, status, navController = navController)
        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductPageContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, productsViewModel: ProductsViewModel = viewModel(), navController: NavHostController) {

    val ctx = LocalContext.current

    val firstTextValue = rememberSaveable { mutableStateOf("") }


    val manufacturer by productsViewModel.manufacturer.observeAsState(User())
    val currentProduct by mainViewModel.currentViewedProduct.observeAsState(Product())


    LazyColumn(
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxSize()
            .padding(vertical = 10.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        item {
            Spacer(modifier = Modifier.height(20.dp))
        }

        item {
            Card(
                modifier = Modifier
                    .size(330.dp),
                shape = RoundedCornerShape(20.dp)
            ) {
                AsyncImage(
                    modifier = Modifier.fillMaxSize(),
                    model = currentProduct.imageURI,
                    contentDescription = "Product Image",
                    contentScale = ContentScale.FillBounds
                )
            }
        }
        item {
            Spacer(modifier = Modifier.height(10.dp))
        }

        item {
            Column(
                modifier = Modifier
                    .width(370.dp)
                    .padding(horizontal = 10.dp),
                horizontalAlignment = Alignment.Start
            ) {
                Text(
                    text = currentProduct.name,
                    fontSize = 26.sp,
                    textAlign = TextAlign.Start,
                    fontWeight = FontWeight.SemiBold
                )

                Text(
                    text = currentProduct.price,
                    fontSize = 22.sp,
                    textAlign = TextAlign.Start,
                    fontWeight = FontWeight.SemiBold
                )
                Text(
                    text = "Min Order: ${currentProduct.minOrder}",
                    fontSize = 18.sp,
                    textAlign = TextAlign.Start,
                )
            }
        }

        item {
            Divider(
                modifier = Modifier
                    .width(350.dp)
                    .padding(top = 10.dp),
                color = MaterialTheme.colorScheme.outline.copy(alpha = 0.8F)
            )
        }

        item {
            Row(
                modifier = Modifier
                    .width(380.dp)
                    .padding(horizontal = 10.dp)
                    .clickable {
                        navController.navigate(Screen.ManufacturerPage.route) {
                            if (navController.backQueue.size >= 4) {
                                popUpTo(Screen.Messaging.route) {
                                    saveState = true
                                }
                            }
                            launchSingleTop = true
                        }
                    },
                verticalAlignment = Alignment.CenterVertically
            ) {

                ProfilePictureCircle(
                    size = 80,
                    modifier = Modifier
                        .weight(0.23F)
                        .padding(vertical = 10.dp),
                    profilePic = manufacturer.profilePictureURI,
                )

                Spacer(modifier = Modifier.padding(horizontal = 5.dp))

                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(0.77F),
                    horizontalAlignment = Alignment.Start
                ) {
                    Text(
                        text = manufacturer.userName,
                        fontSize = 22.sp,
                        textAlign = TextAlign.Start,
                        fontWeight = FontWeight.SemiBold
                    )

                    Text(
                        text = manufacturer.emailAddress,
                        fontSize = 16.sp,
                        textAlign = TextAlign.Start,
                        fontWeight = FontWeight.Normal
                    )
                }
            }
        }

        item {
            Divider(
                modifier = Modifier
                    .width(350.dp)
                    .padding(bottom = 10.dp),
                color = MaterialTheme.colorScheme.outline.copy(alpha = 0.8F)
            )
        }

        item {
            Row(
                modifier = Modifier
                    .wrapContentHeight()
                    .width(380.dp)
                    .padding(start = 10.dp, top = 5.dp,end = 10.dp)
                    .background(MaterialTheme.colorScheme.background),
                horizontalArrangement = Arrangement.Start
            ) {
                Text(
                    text = stringResource(id = R.string.description),
                    fontSize = 24.sp,
                    textAlign = TextAlign.Start,
                    fontWeight = FontWeight.SemiBold
                )

            }
        }

        item {
            Row(
                modifier = Modifier
                    .wrapContentHeight()
                    .width(380.dp)
                    .padding(start = 10.dp, top = 10.dp)
                    .background(MaterialTheme.colorScheme.background)
            ) {
                Text(
                    text = currentProduct.description,
                    fontSize = 16.sp,
                    textAlign = TextAlign.Start,
                    fontWeight = FontWeight.Normal
                )

            }
        }

    }
}









