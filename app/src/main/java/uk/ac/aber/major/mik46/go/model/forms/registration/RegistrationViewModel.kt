package uk.ac.aber.major.mik46.go.model.forms.registration

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.model.forms.validation.*

class RegistrationViewModel(
    private val validateUserName: ValidateUserName = ValidateUserName(),
    private val validateEmail: ValidateEmail = ValidateEmail(),
    private val validatePassword: ValidatePassword = ValidatePassword(),
    private val validateRepeatedPassword: ValidateRepeatedPassword = ValidateRepeatedPassword(),
    private val validateTerms: ValidateTerms = ValidateTerms()
): ViewModel() {

    val isCleared = MutableStateFlow(false)

    val isValid = mutableStateOf(false)

    var state by mutableStateOf(RegistrationFormState())


    fun onEvent(event: RegistrationFormEvent) {
        when(event) {
            is RegistrationFormEvent.UserNameChanged -> {
                if (isCleared.value){
                    state = state.copy(userName = "")
                }else{
                    state = state.copy(userName = event.userName)
                }

            }
            is RegistrationFormEvent.EmailChanged -> {
                if (isCleared.value){
                    state = state.copy(email = "")
                    isCleared.value = false
                }else{
                    state = state.copy(email = event.email)
                }

            }
            is RegistrationFormEvent.PasswordChanged -> {
                if (isCleared.value){
                    state = state.copy(password = "")
                    isCleared.value = false
                }else{
                    state = state.copy(password = event.password)
                }
            }
            is RegistrationFormEvent.RepeatedPasswordChanged -> {
                if (isCleared.value){
                    state = state.copy(repeatedPassword = "")
                    isCleared.value = false
                }else{
                    state = state.copy(repeatedPassword = event.repeatedPassword)
                }

            }
            is RegistrationFormEvent.AcceptTerms -> {
                state = state.copy(acceptedTerms = event.isAccepted)
            }
            is RegistrationFormEvent.Submit -> {
                submitData()
            }
        }
    }

    private fun submitData() {
        viewModelScope.launch {
            val userNameResult = validateUserName.executeRegistration(state.userName)
            val emailResult = validateEmail.executeRegistration(state.email)
            val passwordResult = validatePassword.executeRegistration(state.password)
            val repeatedPasswordResult = validateRepeatedPassword.execute(
                state.password, state.repeatedPassword
            )
            val termsResult = validateTerms.execute(state.acceptedTerms)

            val hasError = listOf(
                userNameResult,
                emailResult,
                passwordResult,
                repeatedPasswordResult,
                termsResult
            ).any { !it.successful }

            if (hasError) {
                state = state.copy(
                    userNameError = userNameResult.errorMessage,
                    emailError = emailResult.errorMessage,
                    passwordError = passwordResult.errorMessage,
                    repeatedPasswordError = repeatedPasswordResult.errorMessage,
                    termsError = termsResult.errorMessage
                )
            } else {
                isValid.value = true
            }
        }
    }

}