package uk.ac.aber.major.mik46.go.model.notifications

import android.Manifest
import android.app.ActivityManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_HIGH
import android.app.PendingIntent
import android.app.PendingIntent.*
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import uk.ac.aber.major.mik46.go.MainActivity
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.getEncryptedSharedPreferences
import kotlin.random.Random

private const val CHANNEL_ID = "NotificationChannel"

class FirebaseNotificationService() : FirebaseMessagingService() {




    companion object {
        var sharedPref: SharedPreferences? = null

        var token: String?
        get() {
            return sharedPref?.getString("token", "")
        }
        set(value) {
            sharedPref?.edit()?.putString("token", value)?.apply()
        }
        var oldToken: String?
            get() {
                return sharedPref?.getString("oldToken", "")
            }
            set(value) {
                sharedPref?.edit()?.putString("oldToken", value)?.apply()
            }
    }

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        oldToken = token
        token = newToken
    }


    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        //checking if the user has the application running
        if (isApplicationRunning()) {
            // The app is currently in the foreground, do not show the notification
            return
        }

        //checking user notifications settings
        if(!getEncryptedSharedPreferences(this).getBoolean("notification", true)){
            return
        }

        // Checking if the app has the necessary permissions
        if (tiramisuPermissionsCheck()) {

            // Create an intent to launch the app's main activity
            val intent = Intent(this, MainActivity::class.java)

            // Get an instance of the notification manager
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            // Generate a random ID for the notification so the notifications do not replace each other
            val notificationID = Random.nextInt()

            // If the Android version is 8.0 or above, create a notification channel
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel(notificationManager)
            }
            // Add the flag to clear the activity from the stack when launching the intent
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

            // Create a pending intent with the intent created above
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, FLAG_IMMUTABLE)

            // Build the notification using the notification builder
            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(message.data["title"])
                .setContentText(message.data["message"])
                .setSmallIcon(R.drawable.message_notification_icon)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build()

            // Show the notification using the notification manager
            notificationManager.notify(notificationID, notification)
        }
    }


    private fun createNotificationChannel(notificationManager: NotificationManager) {
        val channelName = "channelName"
        val channel = NotificationChannel(CHANNEL_ID, channelName, IMPORTANCE_HIGH).apply {
            description = "notification for messages"
            enableLights(true)
            lightColor = Color.GREEN
        }
        notificationManager.createNotificationChannel(channel)
    }

    private fun tiramisuPermissionsCheck(): Boolean {
    // If we are above level 33, check permissions
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.POST_NOTIFICATIONS
        ) == PackageManager.PERMISSION_GRANTED
    } else {
        return true
    }
    }

    private fun isApplicationRunning(): Boolean {

        val activityManager = this.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        //checking if the user has the application running
        val appProcesses = activityManager.runningAppProcesses
        for (appProcess in appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                // The app is currently in the foreground, do not show the notification
                return true
            }
        }
        return false
    }
}