package uk.ac.aber.major.mik46.go.model.forms.validation

data class ValidationResult(
    val successful: Boolean,
    val errorMessage: String? = null
)
