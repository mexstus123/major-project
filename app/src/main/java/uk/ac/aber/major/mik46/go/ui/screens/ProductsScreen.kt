package uk.ac.aber.major.mik46.go.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.viewmodels.ProductsViewModel
import uk.ac.aber.major.mik46.go.ui.components.MainScaffold
import uk.ac.aber.major.mik46.go.ui.components.ProductRowItem
import uk.ac.aber.major.mik46.go.ui.components.ProductsTopBar
import uk.ac.aber.major.mik46.go.ui.components.getUsersLastKnownLocation
import uk.ac.aber.major.mik46.go.ui.navigation.Screen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ProductsScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()

    MainScaffold(
        navController = navController,
        coroutineScope = coroutineScope,
        user = user,
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            ProductsScreenContents(coroutineScope,mainViewModel,status = status, navController = navController, user = user)
        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */

@Composable
fun ProductsScreenContents(coroutineScope: CoroutineScope, mainViewModel: MainViewModel = viewModel(), productsViewModel: ProductsViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController, user: User) {

    val ctx = LocalContext.current

    val searchWord = remember {
        mutableStateOf("")
    }

    LaunchedEffect(key1 = Unit) {
        getUsersLastKnownLocation(ctx,mainViewModel)
        productsViewModel.updateProductsScreen()
    }

    val productsLists by productsViewModel.productsPageLists.observeAsState(mapOf())

    Column(
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        //search bar
        ProductsTopBar(searchWord = searchWord, status = status)

        val isLoading by productsViewModel.isLoading.collectAsState()
        val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = isLoading)

        SwipeRefresh(
            swipeEnabled = status == ConnectivityObserver.Status.Available,

            state = swipeRefreshState,
            onRefresh = {
                coroutineScope.launch {
                    productsViewModel.updateProductsScreen()
                }
            }
        ) {

            LazyColumn(
                userScrollEnabled = true,
                verticalArrangement = Arrangement.Top,
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                ProductQueryType.values().forEach { queryType ->
                item {
                    Spacer(modifier = Modifier.height(30.dp))
                    //scrollable rows for each list type
                        val scrollState = rememberScrollState()
                        Surface(modifier = Modifier
                            .wrapContentHeight()
                            .clickable {
                                mainViewModel.queryType.value = queryType
                                navController.navigate(Screen.QueryList.route)
                            }
                            .fillMaxWidth(),
                            color = MaterialTheme.colorScheme.background
                        ) {
                            Row(
                                modifier = Modifier
                                    .wrapContentHeight()
                                    .fillMaxWidth()
                                    .padding(start = 10.dp, top = 5.dp)
                                    .background(MaterialTheme.colorScheme.background)
                            ) {
                                Text(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .height(40.dp)
                                        .weight(0.9F),
                                    text = queryType.categoryTitle,
                                    textAlign = TextAlign.Start,
                                    fontWeight = FontWeight.SemiBold,
                                    fontSize = 23.sp
                                )

                                Icon(
                                    modifier = Modifier.weight(0.1F).size(35.dp),
                                    imageVector = Icons.Filled.ArrowForward,
                                    contentDescription = stringResource(R.string.forward_arrow),
                                    // specify the tint color for the icon
                                    tint = MaterialTheme.colorScheme.onBackground,
                                )
                            }

                        }

                        Spacer(modifier = Modifier.height(3.dp))
                        LazyRow(
                            userScrollEnabled = true,
                            modifier = Modifier
                                .height(250.dp)
                        ) {

                            if (productsLists[queryType] != null) {
                                items(productsLists[queryType]!!) {
                                    ProductRowItem(
                                        onClick = {
                                            mainViewModel.currentViewedProduct.value = it
                                            navController.navigate(Screen.ProductPage.route)
                                        },
                                        price = it.price,
                                        title = it.name,
                                        image = it.imageURI,
                                        modifier = Modifier
                                            .fillMaxHeight()
                                            .width(200.dp)
                                            .padding(10.dp, 0.dp, 0.dp, 0.dp)

                                    )
                                }
                            }
                        }
                        Spacer(modifier = Modifier.height(30.dp))
                    }

                }
            }

        }
    }
}










