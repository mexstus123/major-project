package uk.ac.aber.major.mik46.go.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.components.ProductInnerNavigationScaffold
import uk.ac.aber.major.mik46.go.ui.components.ProductRowItem
import uk.ac.aber.major.mik46.go.ui.navigation.Screen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ManufacturerPage(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()

    ProductInnerNavigationScaffold(
        navController = navController,
        status = status,
        mainViewModel = mainViewModel,
        user = user,
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            ManufacturerPageContents(coroutineScope, user, mainViewModel, status,navController)
        }
    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ManufacturerPageContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status,navController: NavHostController) {


    val manufacturer = mainViewModel.manufacturer.observeAsState(User())

    LaunchedEffect(key1 = Unit) {
        mainViewModel.getProductsByUID(manufacturer.value.UID)
    }

    val productsLists by mainViewModel.manufacturersProducts.observeAsState(emptyList())


    LazyColumn(
        verticalArrangement = Arrangement.Top,
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
//        item {
//            Spacer(modifier = Modifier.padding(20.dp))
//        }
        item {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(150.dp)
                    .padding(20.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {

                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(0.73F),
                    verticalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = manufacturer.value.userName,
                        fontSize = 24.sp,
                        textAlign = TextAlign.Start,
                        fontWeight = FontWeight.SemiBold
                    )
                    Text(
                        text = manufacturer.value.favouriteCount.toString(),
                        fontSize = 18.sp,
                        textAlign = TextAlign.Start,
                        fontWeight = FontWeight.SemiBold
                    )

                }
                Card(
                    modifier = Modifier
                        .size(100.dp)
                        .weight(0.27F),
                    shape = RoundedCornerShape(125.dp)
                ) {
                    AsyncImage(
                        modifier = Modifier.fillMaxSize(),
                        model = manufacturer.value.profilePictureURI,
                        contentDescription = "profile_photo",
                        contentScale = ContentScale.FillBounds
                    )
                }

            }
        }
        item{
            Row(
                modifier = Modifier
                    .wrapContentHeight()
                    .fillMaxWidth()
                    .padding(start = 10.dp, top = 5.dp)
                    .background(MaterialTheme.colorScheme.background)
            ) {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(40.dp),
                    text ="Users Products",
                    textAlign = TextAlign.Start,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 23.sp
                )

            }
        }

        item {
            Spacer(modifier = Modifier.height(3.dp))
            LazyRow(
                userScrollEnabled = true,
                modifier = Modifier
                    .height(250.dp)
            ) {


                items(productsLists) {
                    ProductRowItem(
                        onClick = {
                            mainViewModel.currentViewedProduct.value = it
                            navController.navigate(Screen.ProductPage.route)
                        },
                        price = it.price,
                        title = it.name,
                        image = it.imageURI,
                        modifier = Modifier
                            .fillMaxHeight()
                            .width(200.dp)
                            .padding(10.dp, 0.dp, 0.dp, 0.dp)

                    )
                }
            }
        }

        item {
            Row(
                modifier = Modifier
                    .wrapContentHeight()
                    .fillMaxWidth()
                    .padding(start = 10.dp, top = 5.dp)
                    .background(MaterialTheme.colorScheme.background)
            ) {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(40.dp),
                    text ="Description",
                    textAlign = TextAlign.Start,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 23.sp
                )

            }
        }

        item {
            Row(
                modifier = Modifier
                    .wrapContentHeight()
                    .fillMaxWidth()
                    .padding(start = 10.dp, top = 5.dp)
                    .background(MaterialTheme.colorScheme.background)
            ) {
                Text(
                    text = manufacturer.value.businessDescription,
                    fontSize = 16.sp,
                    textAlign = TextAlign.Start,
                    fontWeight = FontWeight.Normal
                )

            }
        }


    }


}









