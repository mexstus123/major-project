package uk.ac.aber.major.mik46.go.model.forms.validation

import uk.ac.aber.major.mik46.go.ui.components.BadWordsList

class ValidateDescription(
) {

    suspend fun execute(userName: String): ValidationResult {
        if(userName.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Description cannot be blank"
            )
        }
        if(userName.length > 2000) {
            return ValidationResult(
                successful = false,
                errorMessage = "Description character limit is 2000"
            )
        }
        val regex = Regex("^[a-zA-Z0-9]+$")
        if(!regex.matches(userName)) {
            return ValidationResult(
                successful = false,
                errorMessage = "Descriptions cannot contain special characters"
            )
        }
        BadWordsList.BAD_WORDS.forEach {
            if(userName.contains(it, true)) {
            return ValidationResult(
                successful = false,
                errorMessage = "Remove Explicit word: $it"
            )
        }
    }
        return ValidationResult(
            successful = true
        )
    }
}