package uk.ac.aber.major.mik46.go.model

enum class ProductQueryType(val categoryTitle: String) {
    POPULARITY("Most Popular"),
    LOCATION("Closest To You"),
    RATING("Highly Rated"),
    FAVOURITES("User Favourites"),
    RELEVANT("Relevant To You"),
    NOQUERY("Find Something New"),
    DATE("Recently Added")
}