package uk.ac.aber.major.mik46.go.model

import android.icu.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


data class UserChat(
    var LastMessageTime: Date = Date(),
    var lastMessage: String = "",
    var wasLastMessageRead: Boolean = true,
    var receiver: User = User(),
    var messages: MutableList<UserChatMessage> = mutableListOf()
){


    fun getTimeFormat(): String{
        val currentDate = Date()

        val currentTime = Date().time
        val messageTime = LastMessageTime.time

        val diffInMs = currentTime - messageTime
        val diffInHours = TimeUnit.MILLISECONDS.toHours(diffInMs)
        val diffInDays = TimeUnit.MILLISECONDS.toDays(diffInMs)


        val timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault()).format(messageTime)

        return when {

            diffInHours < 24 -> timeFormat
            diffInDays < 7 -> "${diffInDays}d ago"
            else -> SimpleDateFormat("dd MMM", Locale.getDefault()).format(messageTime)
        }
    }
}



