package uk.ac.aber.major.mik46.go.ui.screens

import android.Manifest
import android.content.pm.PackageManager
import android.util.Log
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.ActivityCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.messaging.Constants.TAG
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.compose.*

import uk.ac.aber.major.mik46.go.ui.components.CustomClusterItem
import uk.ac.aber.major.mik46.go.ui.components.MainScaffold
import com.google.maps.android.compose.clustering.Clustering
import kotlinx.coroutines.*
import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.viewmodels.MapsViewModel
import uk.ac.aber.major.mik46.go.ui.navigation.Screen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun MapScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, mapsViewModel: MapsViewModel = viewModel()){

    val coroutineScope = rememberCoroutineScope()
    val ctx = LocalContext.current

    MainScaffold(
        navController = navController,
        coroutineScope = coroutineScope,
        user = user,
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {


                MapScreenContents(coroutineScope,mainViewModel,navController)


        }
    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(MapsComposeExperimentalApi::class, DelicateCoroutinesApi::class)
@Composable
fun MapScreenContents(coroutineScope: CoroutineScope, mainViewModel: MainViewModel, navController: NavHostController) {

    var clusterManager by remember { mutableStateOf<ClusterManager<CustomClusterItem>?>(null) }
    val ctx = LocalContext.current

    LaunchedEffect(key1 = Unit){
        mainViewModel.getAllManufacturers()
    }

    val markersList = mainViewModel.manufacturerMarkers.observeAsState(emptyList<CustomClusterItem>()).value




    Column(
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val mapProperties = MapProperties(
            // Only enable if user has accepted location permissions.
            isMyLocationEnabled = mainViewModel.locationIsGranted.collectAsState().value,
        )
        //val googleMapOptions = GoogleMapOptions().camera(CameraPosition(LatLng(55.3781,3.4360),30F,0.0F,0.0F))
        val cameraPositionState = rememberCameraPositionState()

        LaunchedEffect(key1 = Unit){
            if (ActivityCompat.checkSelfPermission(
                    ctx,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    ctx,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                LocationServices.getFusedLocationProviderClient(ctx).lastLocation.addOnSuccessListener(){
                    cameraPositionState.position = CameraPosition.fromLatLngZoom(
                        LatLng(it.latitude, it.longitude),
                        9.5F
                    )
                }
            }
        }






        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            GoogleMap(
                cameraPositionState = cameraPositionState,
                //googleMapOptionsFactory = {googleMapOptions},
                modifier = Modifier.fillMaxSize(),
                properties = mapProperties,
                //cameraPositionState = cameraPositionState
            ) {





                MapEffect(markersList) { map ->
                    if (clusterManager == null) {
                clusterManager = ClusterManager<CustomClusterItem>(ctx, map).apply {
                            renderer = DefaultClusterRenderer(ctx, map, this)
                        }
                    }
                }
                Clustering(
                    items = markersList!!,
                    onClusterClick = {
                        Log.d(TAG, "Cluster clicked! $it")
                        false
                    },
                    onClusterItemClick = {
                        Log.d(TAG, "Cluster item clicked! $it")
                        false
                    },
                    onClusterItemInfoWindowClick = {
                        Log.d(TAG, "Cluster item info window clicked! $it")
                        CoroutineScope(Dispatchers.Main).launch {
                            mainViewModel.mapSetManufacturerByID(it.manufacturerUID)
                            navController.navigate(Screen.ManufacturerPage.route)
                        }

                    },
                    // Optional: Custom rendering for clusters
                    clusterContent = { cluster ->
                        Surface(
                            Modifier.size(40.dp),
                            shape = CircleShape,
                            color = Color.Blue,
                            contentColor = Color.White,
                            border = BorderStroke(1.dp, Color.White)
                        ) {
                            Box(contentAlignment = Alignment.Center) {
                                Text(
                                    "%,d".format(cluster.size),
                                    fontSize = 16.sp,
                                    fontWeight = FontWeight.Black,
                                    textAlign = TextAlign.Center
                                )
                            }
                        }
                    },
                    // Optional: Custom rendering for non-clustered items
                    clusterItemContent = null
                )

            }
        }
    }
}









