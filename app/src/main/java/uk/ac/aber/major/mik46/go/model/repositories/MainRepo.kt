package uk.ac.aber.major.mik46.go.model.repositories

import uk.ac.aber.major.mik46.go.model.Product
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.UserChat

interface MainRepo {

    suspend fun updateUsersChats(user: User): List<UserChat>

    suspend fun removeRegistrationToken(user: User, oldToken: String = "")

    suspend fun putCurrentUserData(user: User)

    suspend fun userSetup(user: User)

    suspend fun checkForUserByPhoneNumber(phoneNumber: String): Boolean

    suspend fun checkForUserByUID(UID: String): Boolean

    suspend fun checkForUserByUserName(userName: String): Boolean

    suspend fun getUserFromFireStoreByUID(UID: String): User

    suspend fun getAllManufacturers(): List<User>

    suspend fun getProductsByUID(UID: String) : List<Product>
}