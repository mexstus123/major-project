package uk.ac.aber.major.mik46.go.ui.navigation

/**
 * A sealed class representing the screens in the app.
 *
 * @property route a string representing the route for the screen
 */
sealed class Screen(
    val route: String
){
    /**
     * Each one of these objects represents a screen in the application.
     */
    object Products : Screen("Products")
    object Chats : Screen("Chats")
    object Search : Screen("Search")
    object ProductPage : Screen("ProductPage")
    object ManufacturerPage : Screen("ManufacturerPage")
    object Map : Screen("Map")
    object Messaging : Screen("Messaging")
    object QueryList : Screen("QueryList")
    object Registration : Screen("Registration")
        object ValidateEmail : Screen("ValidateEmail")
        object ValidatePhone : Screen("ValidatePhone")

    object Account : Screen("Account")
        object EditProfile : Screen("EditProfile")
            object EditProfilePicture : Screen("EditProfilePicture ")
            object EditUsername : Screen("EditUsername")
            object EditEmail : Screen("EditEmail")
            object EditPhone : Screen("EditPhone")
            object EditPassword : Screen("EditPassword")
            object BlockedUsers : Screen("BlockedUsers")
            object HiddenProducts : Screen("HiddenProducts")


        object UpgradeToBusiness : Screen("UpgradeToBusiness")
            object RegisterBusiness : Screen("RegisterBusiness")
        object MyBusiness : Screen("MyBusiness")

        object Favourites : Screen("Favourites")
        object Following : Screen("Following")
        object PrivacyAndSafety : Screen("PrivacyAndSafety")

        object AccountSettings : Screen("AccountSettings")
            object AppearanceSettings : Screen("AppearanceSettings")
            object AccessibilitySettings : Screen("AccessibilitySettings")
            object LanguageSettings : Screen("LanguageSettings")
            object NotificationsSettings : Screen("NotificationsSettings")

        object Support : Screen("Support")
        object WhatsNew : Screen("WhatsNew")
        object About : Screen("About")
            object SupportTheDev : Screen("SupportTheDev")

}
/**
 * A list of main screens in the app for the navigation bar.
 */
val mainScreens = listOf(
    Screen.Map,
    Screen.Products,
    Screen.Chats,
    Screen.Account
)