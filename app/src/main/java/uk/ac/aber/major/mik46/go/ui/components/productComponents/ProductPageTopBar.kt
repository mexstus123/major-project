package uk.ac.aber.major.mik46.go.ui.components


import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.R
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import uk.ac.aber.major.mik46.go.ui.theme.GoLocalUKTheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductPageTopBar(
    onClickBack: () -> Unit = {},
    onClickChat: () -> Unit = {},
    onClickSettings: () -> Unit = {},

){
    // the CenterAlignedTopAppBar composable displays the top app bar
    CenterAlignedTopAppBar(
        // specify the colors for the top app bar
        colors = TopAppBarDefaults.mediumTopAppBarColors(containerColor = MaterialTheme.colorScheme.surfaceVariant),
        // the title of the top app bar
        title = {
            // display the app title text
            OpenChatButton(onClick = onClickChat)
        },
        // the navigation icon for the top app bar
        navigationIcon = {
            // the IconButton displays the navigation icon and calls the onClick lambda when clicked

            BackNavigationButton(onClickBack, modifier = Modifier)
        },
        actions = {
            // the IconButton displays the navigation icon and calls the onClick lambda when clicked

            SettingsButton(onClickSettings, modifier = Modifier)
        }
    )
}

@Composable
@Preview
private fun ProductPageTopBarPreview() {
    GoLocalUKTheme(dynamicColor = false) {
        ProductPageTopBar()
    }
}
