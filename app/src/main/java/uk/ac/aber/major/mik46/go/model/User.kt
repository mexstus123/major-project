package uk.ac.aber.major.mik46.go.model

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint


data class User(
    var userName: String = "",
    var emailAddress: String = "",
    var _profilePictureURI: String = "",
    var phoneNumber: String = "",
    var manufacturerAccount: Boolean = false,
    var authentication: FirebaseAuth = FirebaseAuth.getInstance(),
    var UID: String = "",
    var loadingState: Boolean = false,
    var location: GeoPoint = GeoPoint(0.0,0.0),
    var blockedUsers: List<Map<String, String>> = emptyList(),
    var hiddenProducts: List<Map<String, String>> = emptyList(),
    var followedUsers: List<Map<String, String>> = emptyList(),
    var favouritedProducts: List<Map<String, String>> = emptyList(),
    var businessLocation: GeoPoint = GeoPoint(0.0,0.0),
    var businessDescription: String = "",
    var favouriteCount: Int = 222

){
    fun setup(){
        if (authentication.currentUser != null){
            userName = authentication.currentUser!!.displayName.toString()
            emailAddress = authentication.currentUser!!.email.toString()
            profilePictureURI = authentication.currentUser!!.photoUrl.toString()
        }
    }


    var profilePictureURI: String
        get() {
            return _profilePictureURI.ifEmpty { "https://firebasestorage.googleapis.com/v0/b/golocaluk-29e43.appspot.com/o/Default%2FProfilePicture.jpeg?alt=media&token=69ce85e2-33a7-4c88-866e-ac8384715e6b" }
        }
        set(value) {
            _profilePictureURI = value
        }

}



