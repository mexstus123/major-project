package uk.ac.aber.major.mik46.go.ui.components.phoneAreaCode

import android.content.Context
import uk.ac.aber.major.mik46.go.ui.components.phoneAreaCode.utils.getCountryName


fun List<CountryData>.searchCountry(key: String,context: Context): MutableList<CountryData> {
    val tempList = mutableListOf<CountryData>()
    this.forEach {
        if (context.resources.getString(getCountryName(it.countryCode)).lowercase().contains(key.lowercase())) {
            tempList.add(it)
        }
    }
    return tempList
}