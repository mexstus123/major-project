package uk.ac.aber.major.mik46.go.ui.components


import androidx.compose.foundation.layout.*

import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel


/**
 * MainPageTopAppBar is a composable that displays a top app bar.
 * @param onClick: a lambda representing the action that should be taken when the navigation icon is clicked, default value is an empty lambda
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChatsPageTopAppBar(
    onClick: () -> Unit = {},
    profilePic: String,
    mainViewModel: MainViewModel = viewModel(),
    status: ConnectivityObserver.Status
){
//    val status by fireStoreViewModel.connectivityObserver.collectAsState(initial = ConnectivityObserver.Status.Unavailable)
    // the CenterAlignedTopAppBar composable displays the top app bar
    SmallTopAppBar(
        // specify the colors for the top app bar
        colors = TopAppBarDefaults.mediumTopAppBarColors(containerColor = MaterialTheme.colorScheme.surfaceVariant),
        // the title of the top app bar
        title = {
            // display the app title text
            Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.Bottom) {

                    Text(
                        text = "Chats",
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier.weight(0.3F)
                    )
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .weight(0.5F)
                    ) {
                    Text(
                        textAlign = TextAlign.Center,
                        text = when(status != ConnectivityObserver.Status.Available) {
                            true -> "Network $status"
                            false -> ""
                        },
                            fontWeight = FontWeight.Normal,
                            modifier = Modifier.align(Alignment.CenterHorizontally),
                            fontSize = 13.sp,
                            color = MaterialTheme.colorScheme.error
                                )
                }
                Spacer(modifier = Modifier.weight(0.2F))
                }


        },
        // the navigation icon for the top app bar
        actions = {
            // the IconButton displays the navigation icon and calls the onClick lambda when clicked

            ProfilePictureCircle(
                size = 45,
                onClick = onClick,
                profilePic = profilePic
            )
            Spacer(modifier = Modifier.width(10.dp))
        }
    )
}



