package uk.ac.aber.major.mik46.go.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.components.*
import uk.ac.aber.major.mik46.go.ui.screens.signIn.SignInScreen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun FollowingScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()
    val isSignedIn by mainViewModel.isSignedIn.collectAsState()








    InnerNavigationScaffold(
        onClickBack = {
            navController.popBackStack()
        },
        title = stringResource(R.string.following)
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            if (!isSignedIn) {
                SignInScreen(coroutineScope = coroutineScope, user = user, status = status , navController = navController, mainViewModel = mainViewModel)
            }else{
                FollowingScreenContents(coroutineScope, user, mainViewModel, status,navController)
            }


        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FollowingScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController) {


    var blockedUsersList = mainViewModel.currentUser.observeAsState(User()).value.followedUsers
    val context = LocalContext.current



    LaunchedEffect(key1 = context) {

    }

    LazyColumn(
        modifier = Modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        item{

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp, 20.dp, 0.dp, 5.dp),
                text = "${blockedUsersList.size} FOLLOWED USER",
                textAlign = TextAlign.Start,
                fontWeight = FontWeight.Bold,
                fontSize = 14.sp,
                color = MaterialTheme.colorScheme.outline
            )
        }

        items(blockedUsersList){
            AccountUserRow(
                username = it["followedName"]!!,
                pictureURI = it["followedPicture"]!!,
                onClick = {
                    //TODO unblock user
                    it["followedUID"]!!
                },
                buttonText = "Unblock",
                isFollowingList = true
            )
        }
    }
}









