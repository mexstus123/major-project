package uk.ac.aber.major.mik46.go.model.forms.signIn

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.model.forms.registration.RegistrationFormState
import uk.ac.aber.major.mik46.go.model.forms.validation.*
import uk.ac.aber.major.mik46.go.model.repositories.FirebaseAuthRepository

class SignInViewModel(
    private val validateEmail: ValidateEmail = ValidateEmail(),
    private val validatePassword: ValidatePassword = ValidatePassword(),
    private val authRepository: FirebaseAuthRepository = FirebaseAuthRepository()
): ViewModel() {

    val isCleared = MutableStateFlow(false)

    var state by mutableStateOf(RegistrationFormState())

    private val validationEventChannel = Channel<ValidationEvent>()
    val validationEvents = validationEventChannel.receiveAsFlow()

    fun onEvent(event: EmailSignInEvent) {
        when(event) {

            is EmailSignInEvent.EmailChanged -> {
                if (isCleared.value){
                    state = state.copy(email = "")
                    isCleared.value = false
                }else{
                    state = state.copy(email = event.email)
                }
            }
            is EmailSignInEvent.PasswordChanged -> {
                if (isCleared.value){
                    state = state.copy(password = "")
                    isCleared.value = false
                }else{
                    state = state.copy(password = event.password)
                }
            }
            is EmailSignInEvent.Submit -> {
                submitData()
            }
        }
    }

    private fun submitData() {
        viewModelScope.launch {
        val emailResult = validateEmail.executeSignIn(state.email)
        val passwordResult = validatePassword.executeSignIn(state.password)

        val hasError = listOf(
            emailResult,
            passwordResult,

        ).any { !it.successful }

        if(hasError) {
            state = state.copy(
                emailError = emailResult.errorMessage,
                passwordError = passwordResult.errorMessage,
            )
        }else{
            if (authRepository.firebaseSignInWithEmailAndPassword(state.email,state.password)){
                validationEventChannel.send(ValidationEvent.Success)
            }else{
                state = state.copy(
                    emailError = "Incorrect Sign In",
                    passwordError = "Incorrect Sign In",
                )
            }
        }

        }
    }

    sealed class ValidationEvent {
        object Success: ValidationEvent()
    }
}