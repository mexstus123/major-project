package uk.ac.aber.major.mik46.go.ui.screens.signIn

import android.widget.Toast
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Login
import androidx.compose.material.icons.filled.PersonAdd
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.forms.signIn.EmailSignInEvent
import uk.ac.aber.major.mik46.go.model.forms.signIn.SignInViewModel
import uk.ac.aber.major.mik46.go.ui.components.GoogleSignInButton
import uk.ac.aber.major.mik46.go.ui.components.SignInScaffold
import uk.ac.aber.major.mik46.go.ui.navigation.Screen

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SignInScreen(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController) {

        val signInViewModel = viewModel<SignInViewModel>()
        val state = signInViewModel.state
        val context = LocalContext.current
        val focusManager = LocalFocusManager.current



        LaunchedEffect(key1 = context) {
                signInViewModel.validationEvents.collect { event ->
                        when (event) {
                                is SignInViewModel.ValidationEvent.Success -> {
                                        mainViewModel.setupCurrentUser()
                                        mainViewModel.userSignedIn()
                                        Toast.makeText(
                                                context,
                                                "Email Sign In successful",
                                                Toast.LENGTH_LONG
                                        ).show()
                                }
                        }
                }
        }
        SignInScaffold() { innerPadding ->
                Surface(
                        modifier = Modifier
                                .padding(innerPadding)
                                .fillMaxSize()
                                .verticalScroll(rememberScrollState())
                                .pointerInput(Unit) { detectTapGestures(onTap = { focusManager.clearFocus() })},
                        color = MaterialTheme.colorScheme.background
                ) {
                        Column(
                                modifier = Modifier
                                        .fillMaxSize()
                                        .padding(horizontal = 32.dp),
                                verticalArrangement = Arrangement.Top,
                                horizontalAlignment = Alignment.CenterHorizontally
                        ) {

                                Spacer(modifier = Modifier.height(20.dp))

                                Text(
                                        modifier = Modifier
                                                .fillMaxWidth()
                                                .padding(10.dp, 20.dp, 0.dp, 5.dp),
                                        text = stringResource(R.string.email),
                                        textAlign = TextAlign.Start,
                                        fontWeight = FontWeight.SemiBold,
                                        fontSize = 18.sp,
                                        color = MaterialTheme.colorScheme.outline
                                )

                                TextField(
                                        value = state.email,
                                        onValueChange = {
                                                signInViewModel.onEvent(
                                                        EmailSignInEvent.EmailChanged(
                                                                it
                                                        )
                                                )
                                        },
                                        isError = state.emailError != null,
                                        modifier = Modifier.fillMaxWidth(),
                                        placeholder = {
                                                Text(text = stringResource(R.string.email))
                                        },
                                        keyboardOptions = KeyboardOptions(
                                                keyboardType = KeyboardType.Email,
                                                imeAction = ImeAction.Next
                                        ),
                                        singleLine = true,
                                        keyboardActions = KeyboardActions(
                                                onNext = {
                                                        focusManager.moveFocus(FocusDirection.Down)
                                                }
                                        ),
                                        trailingIcon = {
                                                IconButton(onClick = {
                                                        signInViewModel.isCleared.value = true
                                                        signInViewModel.onEvent(
                                                                EmailSignInEvent.EmailChanged(""))
                                                }) {
                                                        Icon(
                                                                imageVector = Icons.Filled.Clear,
                                                                contentDescription = "Clear",
                                                                tint = if (state.emailError != null) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.onSurface
                                                        )
                                                }
                                        }
                                )
                                if (state.emailError != null) {
                                        Text(
                                                text = state.emailError,
                                                color = MaterialTheme.colorScheme.error,
                                                modifier = Modifier.align(Alignment.End)
                                        )
                                }
                                Spacer(modifier = Modifier.height(5.dp))

                                Text(
                                        modifier = Modifier
                                                .fillMaxWidth()
                                                .padding(10.dp, 20.dp, 0.dp, 5.dp),
                                        text = stringResource(R.string.password),
                                        textAlign = TextAlign.Start,
                                        fontWeight = FontWeight.SemiBold,
                                        fontSize = 18.sp,
                                        color = MaterialTheme.colorScheme.outline
                                )

                                TextField(
                                        value = state.password,
                                        onValueChange = {
                                                signInViewModel.onEvent(
                                                        EmailSignInEvent.PasswordChanged(
                                                                it
                                                        )
                                                )
                                        },
                                        isError = state.passwordError != null,
                                        modifier = Modifier.fillMaxWidth(),
                                        placeholder = {
                                                Text(text = stringResource(R.string.password))
                                        },
                                        keyboardOptions = KeyboardOptions(
                                                keyboardType = KeyboardType.Password,
                                        ),
                                        visualTransformation = PasswordVisualTransformation(),
                                        singleLine = true,
                                        trailingIcon = {
                                                IconButton(onClick = {
                                                        signInViewModel.isCleared.value = true
                                                        signInViewModel.onEvent(
                                                                EmailSignInEvent.PasswordChanged(""))
                                                }) {
                                                        Icon(
                                                                imageVector = Icons.Filled.Clear,
                                                                contentDescription = "Clear",
                                                                tint = if (state.passwordError != null) MaterialTheme.colorScheme.error else MaterialTheme.colorScheme.onSurface
                                                        )
                                                        }
                                                }
                                        )

                                Text(
                                        text = state.passwordError ?: "",
                                        color = MaterialTheme.colorScheme.error,
                                        modifier = Modifier.align(Alignment.End)
                                )

                                Spacer(modifier = Modifier.height(32.dp))

                                Row(
                                        modifier = Modifier.fillMaxWidth(),
                                        horizontalArrangement = Arrangement.Center
                                ) {

                                        Button(
                                                onClick = {
                                                        navController.navigate(Screen.Registration.route)
                                                },
                                                modifier = Modifier
                                                        .width(148.dp),
                                                colors = ButtonDefaults.buttonColors(containerColor = MaterialTheme.colorScheme.secondary)

                                        ) {
                                                Text(
                                                        modifier = Modifier
                                                                .weight(0.70F, true),
                                                        text = "Sign Up",
                                                        fontWeight = FontWeight.SemiBold,
                                                        fontSize = 18.sp
                                                )
                                                Icon(
                                                        modifier = Modifier
                                                                .weight(0.3F)
                                                                .fillMaxWidth(),
                                                        imageVector = Icons.Filled.PersonAdd,
                                                        contentDescription = "Register button Icon"
                                                )
                                        }

                                        Spacer(modifier = Modifier.width(40.dp))

                                        Button(
                                                onClick = {
                                                        signInViewModel.onEvent(EmailSignInEvent.Submit)
                                                },
                                                modifier = Modifier
                                                        .width(148.dp),
                                        ) {
                                                Text(
                                                        modifier = Modifier
                                                                .weight(0.60F, false),
                                                        text = "Log In",
                                                        fontWeight = FontWeight.SemiBold,
                                                        fontSize = 18.sp
                                                )
                                                Icon(
                                                        modifier = Modifier
                                                                .weight(0.4F, false)
                                                                .fillMaxWidth(),
                                                        imageVector = Icons.Filled.Login,
                                                        contentDescription = "Login button Icon"
                                                )
                                        }


                                }
                                Spacer(modifier = Modifier.height(50.dp))




                                GoogleSignInButton()
                        }
                }
        }


}