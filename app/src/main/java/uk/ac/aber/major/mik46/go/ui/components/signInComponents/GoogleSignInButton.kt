package uk.ac.aber.major.mik46.go.ui.components

import android.app.Activity
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.theme.GoLocalUKTheme


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GoogleSignInButton(
) {
    val ctx = LocalContext.current as Activity

    Card(
        modifier = Modifier
            .padding(horizontal = 10.dp)
            .height(55.dp)
            .fillMaxWidth()
            .clickable {
                val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(ctx.getString(uk.ac.aber.major.mik46.go.R.string.default_web_client_id))
                    .requestEmail()
                    .build()

                var googleSignInClient = GoogleSignIn.getClient(ctx, gso)
                val signInIntent = googleSignInClient.signInIntent
                ctx.startActivityForResult(signInIntent, 100)
            },
        shape = RoundedCornerShape(10.dp),
        border = BorderStroke(width = 1.5.dp, color = MaterialTheme.colorScheme.onBackground)
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.White),
            verticalAlignment = Alignment.CenterVertically
        ) {

            Image(
                modifier = Modifier
                    .padding(start = 25.dp)
                    .size(36.dp)
                    .padding(0.dp)
                    .align(Alignment.CenterVertically),
                painter = painterResource(id = uk.ac.aber.major.mik46.go.R.drawable.ic_google_logo),
                contentDescription = "google_logo"
            )
            Text(
                modifier = Modifier
                    .padding(start = 25.dp)
                    .align(Alignment.CenterVertically),
                text = "Sign In With Google",
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp,
                color = Color.Black
            )
        }
    }

}

@Composable
@Preview
private fun GoogleButtonPreview() {
    GoLocalUKTheme(dynamicColor = false) {
        GoogleSignInButton()
    }
}