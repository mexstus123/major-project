package uk.ac.aber.major.mik46.go.ui.components

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer

class CustomClusterRenderer(
    context: Context,
    map: GoogleMap,
    clusterManager: ClusterManager<CustomClusterItem>
) : DefaultClusterRenderer<CustomClusterItem>(context, map, clusterManager) {



    override fun shouldRenderAsCluster(cluster: Cluster<CustomClusterItem>): Boolean =
        cluster.size >= 2 // Define when the manager should regroup nearby markers into a cluster


}
