package uk.ac.aber.major.mik46.go.ui.components

import android.Manifest
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccessAlarm
import androidx.compose.material.icons.filled.BackHand
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import uk.ac.aber.major.mik46.go.R

@Composable
fun PermissionDialog(
    permissionTextProvider: PermissionTextProvider,
    isPermanentlyDeclined: Boolean,
    onDismiss: () -> Unit,
    onConfirmClick: () -> Unit,
    onGoToAppSettingsClick: () -> Unit,
    modifier: Modifier = Modifier
) {

    AlertDialog(

        title = { Text(text = "Permission required") },
        text = {
            Text(
                text = permissionTextProvider.getDescription(isPermanentlyDeclined),
                textAlign = TextAlign.Center
            )
        },
        onDismissRequest = onDismiss,
        icon = {
            Icon(
                imageVector = Icons.Filled.BackHand,
                contentDescription = stringResource(R.string.access_alarm)
            )
        },
        dismissButton = {
            Button(
                onClick = onDismiss) {
                Text(stringResource(R.string.dismiss))
            }
        },
        confirmButton = {
            Button(
                onClick = {
                    when(isPermanentlyDeclined) {
                        true -> {
                            onDismiss
                            onGoToAppSettingsClick()
                        }
                        false -> onConfirmClick()
                    }
                }
            ) {
                Text(
                    text = when(isPermanentlyDeclined) {
                        true -> "Grant permission"
                        false -> "Ok"
                        }
                    )

            }
        },
        modifier = modifier
    )
}

interface PermissionTextProvider {
    fun getDescription(isPermanentlyDeclined: Boolean): String
}

class CameraPermissionTextProvider: PermissionTextProvider {
    override fun getDescription(isPermanentlyDeclined: Boolean): String {
        return if(isPermanentlyDeclined) {
            "It seems you permanently declined camera permission. " +
                    "You can go to the app settings to grant it."
        } else {
            "This app needs access to your camera so that you can take photos " +
                    "and then upload them."
        }
    }
}

class NotificationPermissionTextProvider: PermissionTextProvider {
    override fun getDescription(isPermanentlyDeclined: Boolean): String {
        return if(isPermanentlyDeclined) {
            "It seems you permanently declined the Notification permission. " +
                    "You can go to the app settings to grant it."
        } else {
            "This app needs access to Notifications so that you get notified " +
                    "when you receive new messages."
        }
    }
}

class GalleryPermissionTextProvider: PermissionTextProvider {
    override fun getDescription(isPermanentlyDeclined: Boolean): String {
        return if(isPermanentlyDeclined) {
            "It seems you permanently declined the Gallery permission. " +
                    "You can go to the app settings to grant it."
        } else {
            "This app needs the Gallery permission so you can send images " +
                    "from your Gallery."
        }
    }
}

class LocationPermissionTextProvider: PermissionTextProvider {
    override fun getDescription(isPermanentlyDeclined: Boolean): String {
        return if(isPermanentlyDeclined) {
            "It seems you permanently declined the Location permission. " +
                    "You can go to the app settings to grant it."
        } else {
            "This app needs the Location permission so you can search for " +
                    "businesses near you."
        }
    }
}