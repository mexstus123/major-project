package uk.ac.aber.major.mik46.go.ui.components

import android.Manifest
import android.content.ContentValues
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.provider.MediaStore
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.*

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.viewmodels.PermissionsViewModel
import java.io.ByteArrayOutputStream
import java.util.Date

@OptIn(ExperimentalComposeUiApi::class, ExperimentalMaterial3Api::class)
@Composable
fun MessagingBottomBar(
    status: ConnectivityObserver.Status,
    typingState: MutableState<Boolean>,
    user: User,
    receiverUID: String,
    mainViewModel: MainViewModel,
    modifier: Modifier,
    permissionsViewModel: PermissionsViewModel = viewModel(),
    showTimeState: List<MutableState<Boolean>>
){
    val keyboardController = LocalSoftwareKeyboardController.current
    val ctx = LocalContext.current
    val counter = rememberSaveable { mutableStateOf(1) }


    val firstTextValue = rememberSaveable { mutableStateOf("") }

    val cameraPermissionResultLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission(),
        onResult = { isGranted ->
            permissionsViewModel.onPermissionResult(
                permission = Manifest.permission.CAMERA,
                isGranted = isGranted
            )
        }
    )

    val galleryPermissionResultLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission(),
        onResult = { isGranted ->
            permissionsViewModel.onPermissionResult(
                permission = Manifest.permission.ACCESS_MEDIA_LOCATION,
                isGranted = isGranted
            )
        }
    )

    var photoUri by remember{
        mutableStateOf<Uri>(Uri.Builder().build())
    }

    val singlePhotoPickerLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.PickVisualMedia(),
        onResult = {uri ->
            if(uri != null){
                CoroutineScope(Dispatchers.Unconfined).launch{
                    val date = Date()
                    //changing the uri to a bitmap
                    val bitmap = getBitmapFromImage(ctx, uri)
                    //rotation
                    val rotatedBitmap = imageProcessing(uri, bitmap,ctx)

                    val outputStream = ByteArrayOutputStream()
                    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream)
                    val data = outputStream.toByteArray()

                    //uploading the image to Fire store as a message
                    photoUri = mainViewModel.addImageToFirebaseStorage(data,date)
                    mainViewModel.sendUserMessage(
                    receiverUID,
                    UserChatMessage(content = photoUri.toString(), sender = user.emailAddress, image = true, time = date))
                }

            }
        }
    )



    val cameraLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.TakePicture(),
        onResult = {photoWasTaken ->
            if(photoWasTaken) {
                if (photoUri.toString() != "") {

                    CoroutineScope(Dispatchers.Unconfined).launch{
                        //TODO add to storage here and send uri as message content
                        val date = Date()

                        val bitmap = getBitmapFromImage(ctx, photoUri)

                        val rotatedBitmap = imageProcessing(photoUri, bitmap,ctx)

                        //deleting the temp image from gallery
                        val contentResolver = ctx.contentResolver
                        contentResolver.delete(photoUri, null, null)


                        val outputStream = ByteArrayOutputStream()
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream)
                        val data = outputStream.toByteArray()
                        photoUri = mainViewModel.addImageToFirebaseStorage(data,date)
                        mainViewModel.sendUserMessage(
                            receiverUID,
                            UserChatMessage(content = photoUri.toString(), sender = user.emailAddress, image = true, time = date))
                    }
                }
            }
        }
    )

    Surface(
        modifier = modifier.wrapContentHeight(),
        color = MaterialTheme.colorScheme.surfaceVariant
    ) {
        Row(modifier = modifier.wrapContentHeight()) {

            Spacer(modifier = Modifier.weight(0.05F))

            Row(
                modifier = Modifier
                    .weight(0.2F)
                    .height(65.dp)
                    .align(Alignment.Bottom),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically) {


                Surface(
                    modifier = Modifier
                        .size(50.dp)
                        .padding(0.dp, 5.dp, 5.dp, 10.dp)
                        .weight(0.45F),

                    shape = RoundedCornerShape(10.dp),
                    color = MaterialTheme.colorScheme.secondary,
                    onClick = {
                        //get camera permission from user
                        cameraPermissionResultLauncher.launch(Manifest.permission.CAMERA)

                        //get gallery permissions from user - NOTE needed as the file is temporarily saved to written to gallery, from there its processed and deleted
                        galleryPermissionResultLauncher.launch(Manifest.permission.ACCESS_MEDIA_LOCATION)

                        //check if user has given permission to use the camera
                        if (ContextCompat.checkSelfPermission(
                                ctx,
                                Manifest.permission.CAMERA
                            ) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(
                                ctx,
                                Manifest.permission.ACCESS_MEDIA_LOCATION
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            CoroutineScope(Dispatchers.IO).launch {
                                //create location for image
                                val contentValues = ContentValues().apply {
                                    put(MediaStore.Images.Media.DISPLAY_NAME, "tempImage.jpg")
                                    put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
                                }
                                //make it into a uri
                                val contentResolver = ctx.contentResolver
                                photoUri = contentResolver.insert(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    contentValues
                                )!!

                                //open camera
                                cameraLauncher.launch(
                                    photoUri
                                )
                            }
                        }
                    }
                ) {

                    Icon(
                        imageVector = Icons.Filled.PhotoCamera,
                        contentDescription = stringResource(R.string.back_arrow),
                        // specify the tint color for the icon
                        tint = MaterialTheme.colorScheme.secondaryContainer,
                    )
                }
                Spacer(modifier = Modifier.weight(0.1F))
                Surface(
                    modifier = Modifier
                        .size(50.dp)
                        .padding(5.dp, 5.dp, 0.dp, 10.dp)
                        .weight(0.45F),
                    shape = RoundedCornerShape(10.dp),
                    color = MaterialTheme.colorScheme.secondary,
                    onClick = {
                        //get gallery permissions from user
                        galleryPermissionResultLauncher.launch(Manifest.permission.ACCESS_MEDIA_LOCATION)

                        //check if user has given permission to use the gallery
                        if (ContextCompat.checkSelfPermission(
                                ctx,
                                Manifest.permission.ACCESS_MEDIA_LOCATION
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {

                            //open the gallery
                            singlePhotoPickerLauncher.launch(
                                PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly)
                            )
                        }
                    }
                ) {
                    Icon(
                        imageVector = Icons.Filled.PhotoAlbum,
                        contentDescription = stringResource(R.string.back_arrow),
                        // specify the tint color for the icon
                        tint = MaterialTheme.colorScheme.secondaryContainer,
                    )
                }
            }


            MessagingTypingBar(
                typingWord = firstTextValue,
                status = status,
                sendMessageOnClick = {
                    showTimeState.forEach{
                        it.value =  false
                    }
                    if (firstTextValue.value != "") {
                        mainViewModel.sendUserMessage(
                            receiverUID,
                            UserChatMessage(
                                time = Date(),
                                content = firstTextValue.value,
                                sender = user.emailAddress,
                                image = false
                            )
                        )
                        firstTextValue.value = ""
                    }
                },
                modifier = Modifier
                    .weight(0.75F)
                    .wrapContentHeight()
            )
        }
    }
}

private suspend fun getBitmapFromImage(context: Context, uri: Uri): Bitmap {

    val inputStream = context.contentResolver.openInputStream(uri)
    val bitmap = BitmapFactory.decodeStream(inputStream)
    val drawable = BitmapDrawable(context.resources, bitmap)


    // in below line we are creating our bitmap and initializing it.
    val bit = Bitmap.createBitmap(
        drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888
    )

    // on below line we are
    // creating a variable for canvas.
    val canvas = Canvas(bit)

    // on below line we are setting bounds for our bitmap.
    drawable.setBounds(0, 0, canvas.width, canvas.height)

    // on below line we are simply
    // calling draw to draw our canvas.
    drawable.draw(canvas)

    // on below line we are
    // returning our bitmap.
    return bit
}


