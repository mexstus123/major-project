package uk.ac.aber.major.mik46.go.ui.components

import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.navigation.Screen


/**
 * MainScaffold is a composable that displays a scaffold with a top app bar, bottom app bar, and content area.
 * It also displays a navigation drawer and a snackbar when needed.
 * @param activity: an instance of [MainActivity]
 * @param navController: an instance of [NavHostController] that is used to navigate between destinations in the app
 * @param userWordsViewModel: an instance of [UserWordsViewModel] used to access the app's data
 * @param snackbarContent: a lambda representing the content that should be displayed in the snackbar, default value is an empty lambda
 * @param coroutineScope: a [CoroutineScope] that is used to launch coroutines
 * @param snackbarHostState: the state of the snackbar host, represented by an instance of [SnackbarHostState], default value is null
 * @param pageContent: a lambda representing the content that should be displayed in the content area of the scaffold, default value is an empty lambda
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScaffold(
    navController: NavHostController,
    snackbarContent: @Composable (SnackbarData) -> Unit = {},
    user: User,
    coroutineScope: CoroutineScope,
    snackbarHostState: SnackbarHostState? = null,
    pageContent:
    @Composable (innerPadding: PaddingValues) -> Unit = {}
    ) {

    // drawerState is a [DrawerState] that is saved across composition rebuilds
    // it is initialized to the closed state
    //val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)

        Scaffold(
//            topBar = {
//                if(navController.currentDestination?.route == Screen.Chats.route){
//                ChatsPageTopAppBar(onClick = {
//                    navController.navigate(Screen.Account.route) {
//                        // Pop up to the start destination and save the state
//                        popUpTo(navController.graph.findStartDestination().id) {
//                            saveState = true
//                        }
//                        // Set the launch mode to singleTop
//                        launchSingleTop = true
//                        // Restore the state when navigating
//                        restoreState = true
//                    }
//                                             },
//                    user.profilePictureURI
//                )
//
//                }
//                    if(navController.currentDestination?.route == Screen.Products.route){
//                        ChatsPageTopAppBar(onClick = {
//                            navController.navigate(Screen.Account.route) {
//                                // Pop up to the start destination and save the state
//                                popUpTo(navController.graph.findStartDestination().id) {
//                                    saveState = true
//                                }
//                                // Set the launch mode to singleTop
//                                launchSingleTop = true
//                                // Restore the state when navigating
//                                restoreState = true
//                            }
//                },
//                    user.profilePictureURI
//                )
//            }
                   //  },
            bottomBar = { MainPageNavigationBar(navController) },
            content = { innerPadding -> pageContent(innerPadding) },
            snackbarHost = {
                snackbarHostState?.let {
                    SnackbarHost(hostState = snackbarHostState){ data ->
                        snackbarContent(data)
                    }
                }
            }

        )
    }
