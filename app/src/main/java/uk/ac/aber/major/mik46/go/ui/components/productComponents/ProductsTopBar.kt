package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductsTopBar(
    onClick: () -> Unit = {},
    searchWord: MutableState<String>,
    status: ConnectivityObserver.Status

){
    // the CenterAlignedTopAppBar composable displays the top app bar
    CenterAlignedTopAppBar(
        // specify the colors for the top app bar
        colors = TopAppBarDefaults.mediumTopAppBarColors(containerColor = MaterialTheme.colorScheme.surfaceVariant),
        // the title of the top app bar
        navigationIcon = {},
        actions = {},
        title = {
            Row(
                Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()) {
                Spacer(modifier = Modifier.weight(0.12F))
                ProductsSearchBar(searchWord = searchWord , onClick = onClick, status = status, modifier = Modifier
                    .height(50.dp)
                    .weight(0.76F)
                )
                Spacer(modifier = Modifier.weight(0.12F))
            }

        },

    )
}