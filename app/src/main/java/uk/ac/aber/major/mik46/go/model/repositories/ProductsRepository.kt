package uk.ac.aber.major.mik46.go.model.repositories

import android.util.Log
import com.google.firebase.firestore.*
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import uk.ac.aber.major.mik46.go.model.*
import java.util.*


/**
 * Repository class responsible for handling data operations.
 * This is the mediator between the [UserWordDao] and the [UserWordsViewModel].
 *
 * @param application The application context
 */
class ProductsRepository() : ProductsRepo{


    var db = FirebaseFirestore.getInstance()

    private lateinit var lastLoadedProduct : DocumentSnapshot
    private lateinit var lastGeoPoint: GeoPoint
    private lateinit var lastQueryType: ProductQueryType

    //TODO add product

    fun addTestProductsList() {
        for (i in 4..6)
            db.collection("new/kdnW0yltNZedVFw2Zrz0/Products")
            .add(
                hashMapOf(
                    "description" to "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    "imageURI" to "https://firebasestorage.googleapis.com/v0/b/golocaluk-29e43.appspot.com/o/IMAGES%2F1Rsgzsq9VfxGXQWZ8nXl%2FSun%20Mar%2005%2013%3A21%3A43%20GMT%202023?alt=media&token=3bcd54fb-c53b-4122-9841-1b0244ce28af",
                    "manufacturerID" to "kdnW0yltNZedVFw2Zrz0",
                    "minOrder" to "20",
                    "maxOrder" to "1000",
                    "name" to "Reeeeeeee",
                    "price" to "£$i-£${i}23",
                    "productID" to "$i",
                    "uploadTime" to Date(),
                    "favouriteCount" to 10,
                    "rating" to 4.7,
                    "location" to GeoPoint(53.9185227427364,(-3.0 + i)),
                    )
            )
    }

    override suspend fun getProductsByQuery(queryCondition: ProductQueryType, limit: Long, usersLocation: GeoPoint) : List<Product>{


        var productsList = listOf<Product>()
        val isNewQueryType = ::lastQueryType.isInitialized && queryCondition != lastQueryType


        when(queryCondition){
            ProductQueryType.DATE ->{
                productsList = getProductsByDate(limit,isNewQueryType)
            }
            ProductQueryType.LOCATION ->{
                productsList = getProductsByLocation(usersLocation,limit,isNewQueryType)

            }
            ProductQueryType.FAVOURITES ->{
                productsList = getProductsByFavourites(limit,isNewQueryType)
            }
            ProductQueryType.RATING ->{
                productsList = getProductsByRating(limit,isNewQueryType)
            }
            ProductQueryType.NOQUERY ->{
                productsList = getProductsByNoQuery(limit, isNewQueryType)
            }
            else -> {

            }
        }
        lastQueryType = queryCondition
        return productsList.toList()

    }


    private suspend fun getProductsByLocation(userLocation: GeoPoint, limit: Long, isNewQueryType: Boolean): List<Product>{


        val productsList = mutableListOf<Product>()

        when(::lastLoadedProduct.isInitialized && !isNewQueryType){
            true ->{db.collectionGroup("Products").orderBy("location")
                .startAt(userLocation)
                .startAfter(lastLoadedProduct)
                .limit(limit).get().await()
                .forEachIndexed {index, it ->
                    productsList.add(it.toObject(Product::class.java))
                    if (index == productsList.lastIndex){
                        lastLoadedProduct = it
                        lastGeoPoint = it.toObject(Product::class.java).location
                    }
                }}
            false -> {db.collectionGroup("Products").orderBy("location",)
                .startAt(userLocation)
                .limit(limit).get().await()
                .forEachIndexed {index, it ->
                    productsList.add(it.toObject(Product::class.java))
                    if (index == productsList.lastIndex){
                        lastLoadedProduct = it
                        lastGeoPoint = it.toObject(Product::class.java).location
                    }
                }}
        }

        return productsList

    }

    private suspend fun getProductsByRating(limit: Long, isNewQueryType: Boolean): List<Product>{

        val productsList = mutableListOf<Product>()

        when(::lastLoadedProduct.isInitialized && !isNewQueryType){
            true ->{db.collectionGroup("Products").orderBy("rating",Query.Direction.DESCENDING)
                .startAfter(lastLoadedProduct)
                .limit(limit).get().await()
                .forEachIndexed {index, it ->
                    productsList.add(it.toObject(Product::class.java))
                    if (index == productsList.lastIndex){
                        lastLoadedProduct = it
                    }
                }}
            false -> {db.collectionGroup("Products").orderBy("rating",Query.Direction.DESCENDING)
                .limit(limit).get().await()
                .forEachIndexed {index, it ->
                    productsList.add(it.toObject(Product::class.java))
                    if (index == productsList.lastIndex){
                        lastLoadedProduct = it
                    }
                }}
        }

        return productsList

    }

    private suspend fun getProductsByNoQuery(limit: Long, isNewQueryType: Boolean): List<Product>{

        val productsList = mutableListOf<Product>()

        when(::lastLoadedProduct.isInitialized && !isNewQueryType){
            true ->{db.collectionGroup("Products")
                .startAfter(lastLoadedProduct)
                .limit(limit).get().await()
                .forEachIndexed {index, it ->
                    productsList.add(it.toObject(Product::class.java))
                    if (index == productsList.lastIndex){
                        lastLoadedProduct = it
                    }
                }}
            false -> {db.collectionGroup("Products")
                .limit(limit).get().await()
                .forEachIndexed {index, it ->
                    productsList.add(it.toObject(Product::class.java))
                    if (index == productsList.lastIndex){
                        lastLoadedProduct = it
                    }
                }}
        }

        return productsList

    }

    private suspend fun getProductsByFavourites( limit: Long, isNewQueryType: Boolean): List<Product>{

        val productsList = mutableListOf<Product>()

        when(::lastLoadedProduct.isInitialized && !isNewQueryType){
            true ->{db.collectionGroup("Products").orderBy("favouriteCount",Query.Direction.DESCENDING)
                .startAfter(lastLoadedProduct)
                .limit(limit).get().await()
                .forEachIndexed {index, it ->
                    productsList.add(it.toObject(Product::class.java))
                    if (index == productsList.lastIndex){
                        lastLoadedProduct = it
                    }
                }}
            false -> {db.collectionGroup("Products").orderBy("favouriteCount",Query.Direction.DESCENDING)
                .limit(limit).get().await()
                .forEachIndexed {index, it ->
                    productsList.add(it.toObject(Product::class.java))
                    if (index == productsList.lastIndex){
                        lastLoadedProduct = it
                    }
                }}
        }

        return productsList

    }

    private suspend fun getProductsByDate( limit: Long, isNewQueryType: Boolean): List<Product>{

        val productsList = mutableListOf<Product>()

        when(::lastLoadedProduct.isInitialized && !isNewQueryType){
            true ->{db.collectionGroup("Products").orderBy("uploadTime",Query.Direction.DESCENDING)
                .startAfter(lastLoadedProduct)
                .limit(limit).get().await()
                .forEachIndexed {index, it ->
                    productsList.add(it.toObject(Product::class.java))
                    if (index == productsList.lastIndex){
                        lastLoadedProduct = it
                    }
                }}
            false -> {db.collectionGroup("Products").orderBy("uploadTime",Query.Direction.DESCENDING)
                .limit(limit).get().await()
                .forEachIndexed {index, it ->
                    productsList.add(it.toObject(Product::class.java))
                    if (index == productsList.lastIndex){
                        lastLoadedProduct = it
                    }
                }}
        }

        return productsList

    }

    private suspend fun getUserDocumentID(UID: String): String{
        var userDocumentID = ""

        try {
            //getting the user id by email
            db.collection("new").whereEqualTo("UID", UID).get().await().forEach {
                userDocumentID = it.id
            }
        }catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
        return userDocumentID
    }

    override suspend fun getUserFromFireStoreByUID(UID: String): User {
        val userDocumentId = getUserDocumentID(UID)
        var user = User()
        try {
            val document = db.document("new/$userDocumentId").get().await()
            if (document.exists()) {
                user = document.toObject(User::class.java)!!
            }
        } catch (e: FirebaseFirestoreException) {
            Log.d("error", "getDataFromFireStore: $e")
        }
        return user
    }


}