package uk.ac.aber.major.mik46.go.ui.components

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

data class CustomClusterItem(
    val markerPosition: LatLng,
    val username: String,
    val description: String,
    val manufacturerUID: String
) : ClusterItem {
    override fun getPosition(): LatLng = markerPosition
    override fun getTitle(): String = username
    override fun getSnippet(): String = description
}
