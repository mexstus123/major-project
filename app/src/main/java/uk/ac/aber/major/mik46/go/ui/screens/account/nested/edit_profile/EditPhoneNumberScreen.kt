package uk.ac.aber.major.mik46.go.ui.screens

import android.app.Activity
import android.widget.Toast
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.viewmodels.VerificationViewModel
import uk.ac.aber.major.mik46.go.model.forms.editProfile.EditProfileViewModel
import uk.ac.aber.major.mik46.go.model.viewmodels.AuthenticationViewModel
import uk.ac.aber.major.mik46.go.ui.components.InnerNavigationScaffold
import uk.ac.aber.major.mik46.go.ui.components.phoneAreaCode.*
import uk.ac.aber.major.mik46.go.ui.screens.signIn.SignInScreen
import java.util.concurrent.TimeUnit


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun EditPhoneNumberScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()
    val isSignedIn by mainViewModel.isSignedIn.collectAsState()
    val focusManager = LocalFocusManager.current

    InnerNavigationScaffold(
        onClickBack = {
            navController.popBackStack()
        },
        title = stringResource(R.string.edit_phone_number)
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
                .pointerInput(Unit) { detectTapGestures(onTap = { focusManager.clearFocus() }) },
            color = MaterialTheme.colorScheme.background
        ) {
            if (!isSignedIn) {
                SignInScreen(coroutineScope = coroutineScope, user = user, status = status , navController = navController, mainViewModel = mainViewModel)
            }else{
                EditPhoneNumberScreenContents(coroutineScope, user, mainViewModel, status,navController)
            }


        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditPhoneNumberScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController, verificationViewModel: VerificationViewModel = viewModel()) {

    val editProfileViewModel: EditProfileViewModel = viewModel()
    val authenticationViewModel = viewModel<AuthenticationViewModel>()

    val alreadyExists = remember { mutableStateOf(false) }

    val ctx = LocalContext.current

    val phoneNumber = rememberSaveable { mutableStateOf("") }
    val fullPhoneNumber = rememberSaveable { mutableStateOf("") }
    val onlyPhoneNumber = rememberSaveable { mutableStateOf("") }
    val checkNumberState = remember { mutableStateOf(false) }

    lateinit var options: PhoneAuthOptions

    if (!alreadyExists.value ){
        PhoneAuthProvider.verifyPhoneNumber(options)
    }


    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 32.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(0.dp, 50.dp, 0.dp, 0.dp),
            text = "Update your Phone Number",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp,
            color = MaterialTheme.colorScheme.onBackground
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 10.dp, 0.dp),
            text = "You can change your phone number entering a new one below, applying the changes and then verifying your new phone number",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Normal,
            fontSize = 14.sp,
            color = MaterialTheme.colorScheme.outline
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 0.dp, 5.dp),
            text = "Phone Number",
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )



        AreaCodePickerTextField(
            text = phoneNumber,
            onValueChange = { phoneNumber.value = it },
            unfocusedBorderColor = MaterialTheme.colorScheme.primary,
            bottomStyle = false,
            shape = RoundedCornerShape(24.dp),
            checkNumberState = checkNumberState
        )
        Text(
            text = when(alreadyExists.value) {
                true -> "Phone Number Already In Use"
                false -> ""
            },
            color = MaterialTheme.colorScheme.error,
            modifier = Modifier.align(Alignment.End)
        )


        Spacer(modifier = Modifier.height(32.dp))


        Button(
            onClick = {
                if (alreadyExists.value){
                    alreadyExists.value = false
                }
                if (!isPhoneNumber(checkNumberState)) {
                    coroutineScope.launch {

                        if (!authenticationViewModel.phoneNumberAlreadyExists(fullPhoneNumber.value)){
                            fullPhoneNumber.value = getFullPhoneNumber()
                            onlyPhoneNumber.value = getOnlyPhoneNumber()

                            options = PhoneAuthOptions.newBuilder(mainViewModel.currentUser.value!!.authentication)
                                .setPhoneNumber(fullPhoneNumber.value)       // Phone number to verify
                                .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                                .setActivity(ctx as Activity)                 // Activity (for callback binding)
                                .setCallbacks(verificationViewModel.phoneVerificationCallbacks)          // OnVerificationStateChangedCallbacks
                                .build()

                            PhoneAuthProvider.verifyPhoneNumber(options)
                        }else{
                            alreadyExists.value = true
                        }


                    }



                }

            },
            modifier = Modifier
                .width(250.dp),
        ) {
            Text(
                modifier = Modifier,
                text = "Change Number",
                fontWeight = FontWeight.SemiBold,
                textAlign = TextAlign.Center,
                fontSize = 18.sp,
            )
        }

    }
}









