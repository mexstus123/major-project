package uk.ac.aber.major.mik46.go.model.viewmodels

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import uk.ac.aber.major.mik46.go.model.Product
import uk.ac.aber.major.mik46.go.model.ProductQueryType
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.repositories.*

class AuthenticationViewModel constructor(
    private val authRepository: FirebaseAuthRepository = FirebaseAuthRepository(),
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {



    private val coroutineScope: CoroutineScope = CoroutineScope(ioDispatcher)

    private val _isLoading = MutableStateFlow(true)
    val isLoading = _isLoading.asStateFlow()


    private val _isEmailVerified  = MutableStateFlow(false)
    val isEmailVerified  = _isEmailVerified .asStateFlow()

    private val _hasPhoneNumber = MutableStateFlow(false)
    val hasPhoneNumber = _hasPhoneNumber.asStateFlow()

    private val _usernameAlreadyExists = MutableStateFlow(false)
    val usernameAlreadyExists = _usernameAlreadyExists.asStateFlow()

    private val _emailAlreadyExists = MutableStateFlow(false)
    val emailAlreadyExists = _emailAlreadyExists.asStateFlow()

    private val _phoneNumberAlreadyExists = MutableStateFlow(true)
    val phoneNumberAlreadyExists = _phoneNumberAlreadyExists.asStateFlow()

    private val _hasTempSignIn = MutableStateFlow(false)
    val hasTempSignIn = _hasTempSignIn.asStateFlow()




    fun firebaseSignUpWithEmailAndPassword(email: String, password: String, username: String){
        coroutineScope.launch {
            _isLoading.value = true
            authRepository.firebaseSignUpWithEmailAndPassword(email, password,username)
            _hasTempSignIn.value = true
            _isLoading.value = false
        }
    }

    fun sendEmailVerification(){
        coroutineScope.launch {
            _isLoading.value = true
            authRepository.sendEmailVerification()
            _isLoading.value = false
        }
    }

    fun firebaseSignInWithEmailAndPassword(email: String, password: String){
        coroutineScope.launch {
            _isLoading.value = true
            authRepository.firebaseSignInWithEmailAndPassword(email,password)
            _isLoading.value = false
        }
    }

    fun reloadFirebaseUser(){
        coroutineScope.launch {
            _isLoading.value = true
            authRepository.reloadFirebaseUser()
            _isLoading.value = false
        }
    }

    fun sendPasswordResetEmail(email: String){
        coroutineScope.launch {
            _isLoading.value = true
            authRepository.sendPasswordResetEmail(email)
            _isLoading.value = false
        }
    }

    fun signOut(){
        authRepository.signOut()
        _hasTempSignIn.value = false
    }

    fun deleteCurrentUser(){
        coroutineScope.launch {
            _isLoading.value = true
            authRepository.deleteCurrentUser()
            _hasTempSignIn.value = false
            _isLoading.value = false
        }
    }

    fun isEmailVerified(){
        _isLoading.value = true
        var tempBool = false
        coroutineScope.launch {

            tempBool = authRepository.isEmailVerified()
            withContext(Dispatchers.Main){
               _isEmailVerified.value = tempBool
                _isLoading.value = false
            }
        }
    }

    fun hasPhoneNumber(){
        var tempBool = false
        coroutineScope.launch {
            _isLoading.value = true
            tempBool = authRepository.hasPhoneNumber()
            withContext(Dispatchers.Main){
                _hasPhoneNumber.value = tempBool
                _isLoading.value = false
            }
        }
    }

    fun usernameAlreadyExists(username: String){
        var tempBool = false
        coroutineScope.launch {
            _isLoading.value = true
            tempBool = authRepository.usernameAlreadyExists(username)
            withContext(Dispatchers.Main){
                _usernameAlreadyExists.value = tempBool
                _isLoading.value = false
            }
        }
    }

    fun emailAlreadyExists(email: String){
        var tempBool = false
        coroutineScope.launch {
            _isLoading.value = true
            tempBool = authRepository.emailAlreadyExists(email)
            withContext(Dispatchers.Main){
                _emailAlreadyExists.value = tempBool
                _isLoading.value = false
            }
        }
    }

    suspend fun phoneNumberAlreadyExists(phoneNumber: String): Boolean{
        _isLoading.value = true
        val tempBool =  authRepository.phoneNumberAlreadyExists(phoneNumber)
        _isLoading.value = false
        return tempBool

    }

}
