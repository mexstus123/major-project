package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Circle
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import uk.ac.aber.major.mik46.go.R


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChatsRow(
    onClick: () -> Unit = {},
    profilePic: String = "", //TODO maybe pass in user>???
    name: String,
    lastMessage: String,
    lastMessageTime: String,
    lastMessageWasRead: Boolean
){

    Row(modifier = Modifier
        .fillMaxWidth()
        .height(90.dp)
    ) {
        Surface(
            modifier = Modifier
                .fillMaxWidth(),
            onClick = onClick,
            color = MaterialTheme.colorScheme.background
        ) {
            Row(modifier = Modifier.fillMaxSize()){
                Spacer(modifier = Modifier.width(10.dp))
                ProfilePictureCircle(size = 65, profilePic = profilePic)

                Column(modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = 20.dp)
                ) {
                    Spacer(modifier = Modifier.height(3.dp))
                    Text(
                        text = name,
                        fontWeight = FontWeight.Bold,
                        fontSize = 22.sp
                    )
                    Row(modifier = Modifier.wrapContentSize()) {
                        if (!lastMessageWasRead){
                            Icon(
                            imageVector = Icons.Filled.Circle,
                            tint = MaterialTheme.colorScheme.secondary,
                            contentDescription = stringResource(R.string.was_read_icon),
                            modifier = Modifier
                                .fillMaxWidth()
                                .weight(0.1F)
                                .padding(8.dp)
                            ,
                    )
                        }

                        Text(
                            modifier = when(lastMessageWasRead){
                                true -> Modifier.weight(0.7F,false)
                                false -> Modifier.weight(0.8F,false)
                            },
                            overflow = TextOverflow.Ellipsis,
                            maxLines = 1,
                            text = "$lastMessage",
                            fontSize = 16.sp,
                            color = when(lastMessageWasRead){
                                true -> MaterialTheme.colorScheme.outline
                                false -> MaterialTheme.colorScheme.onBackground
                            }

                        )
                            Text(
                                maxLines = 1,
                                text = "   - $lastMessageTime",
                                fontSize = 16.sp

                            )


                    }
                }
            }

        }

    }

}