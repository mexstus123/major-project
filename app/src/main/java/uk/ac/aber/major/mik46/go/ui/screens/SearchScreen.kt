package uk.ac.aber.major.mik46.go.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.components.GoogleSignInButton
import uk.ac.aber.major.mik46.go.ui.components.SignOutButton
import uk.ac.aber.major.mik46.go.ui.components.MainScaffold


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SearchScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()

    MainScaffold(
        navController = navController,
        coroutineScope = coroutineScope,
        user = user,
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            SearchScreenContents(coroutineScope, user, mainViewModel, status)
        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status) {


    val firstTextValue = rememberSaveable { mutableStateOf("") }





    if (user.authentication.currentUser == null) {
        GoogleSignInButton()

    } else {
//        LaunchedEffect(Unit) {
//            fireStoreViewModel.getUserByEmail(user.emailAddress)
//        }
//        val getUser = fireStoreViewModel.state.value  //TODO fix to user rather than load from db
        Column(
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Card(
                modifier = Modifier
                    .size(120.dp)
                    .fillMaxHeight(0.4f),
                shape = RoundedCornerShape(125.dp)
            ) {
                AsyncImage(
                    modifier = Modifier.fillMaxSize(),
                    model = user.profilePictureURI,
                    contentDescription = "profile_photo",
                    contentScale = ContentScale.FillBounds
                )
            }
            Text(text = user.userName)
            Text(text = user.emailAddress)
            Text(text = user.manufacturerAccount.toString())

            SignOutButton(modifier = Modifier.fillMaxWidth(),user)
        }
    }
}









