package uk.ac.aber.major.mik46.go.model.forms.validation

import uk.ac.aber.major.mik46.go.model.repositories.MainRepo
import uk.ac.aber.major.mik46.go.model.repositories.MainRepository
import uk.ac.aber.major.mik46.go.ui.components.BadWordsList

class ValidateUserName(
    private val mainRepository: MainRepo = MainRepository()
) {

    suspend fun executeRegistration(userName: String): ValidationResult {
        if(userName.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "User names cannot be blank"
            )
        }
        if(userName.length < 8) {
            return ValidationResult(
                successful = false,
                errorMessage = "User names consist of at least 8 characters"
            )
        }
        if(userName.contains(" ")) {
            return ValidationResult(
                successful = false,
                errorMessage = "User names cannot contain spaces"
            )
        }
        val regex = Regex("^[a-zA-Z0-9]+$")
        if(!regex.matches(userName)) {
            return ValidationResult(
                successful = false,
                errorMessage = "User names cannot contain special characters"
            )
        }
        BadWordsList.BAD_WORDS.forEach {
            if(userName.contains(it, true)) {
            return ValidationResult(
                successful = false,
                errorMessage = "Remove Explicit word: $it"
            )
        }
    }
        if(mainRepository.checkForUserByUserName(userName)) {
            return ValidationResult(
                successful = false,
                errorMessage = "User name already exists"
            )
        }
        return ValidationResult(
            successful = true
        )
    }
}