package uk.ac.aber.major.mik46.go.model.notifications

data class NotificationData(
    val title: String,
    val message: String
)