package uk.ac.aber.major.mik46.go.model.forms.editProfile

data class EditProfileState(
    val userName: String,
    val userNameError: String? = null,
    var email: String,
    val emailError: String? = null,
    val phoneNumber: String,
    val phoneNumberError: String? = null,
    val password: String,
    val passwordError: String? = null,
    val repeatedPassword: String,
    val repeatedPasswordError: String? = null,
    val acceptedTerms: Boolean = false,
    val termsError: String? = null,
    val description: String,
    val descriptionError: String? = null,
)
