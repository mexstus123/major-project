package uk.ac.aber.major.mik46.go.ui.screens

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.viewmodels.ProductsViewModel
import uk.ac.aber.major.mik46.go.ui.components.*
import uk.ac.aber.major.mik46.go.ui.navigation.Screen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun QueryListScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, productsViewModel: ProductsViewModel = viewModel()){

    val coroutineScope = rememberCoroutineScope()

    BackHandler(enabled = true, onBack = {
        coroutineScope.launch {
            productsViewModel.emptyProductsList()
        }
        navController.popBackStack()
    })

    InnerNavigationScaffold(
        onClickBack = {
            coroutineScope.launch {
                productsViewModel.emptyProductsList()
            }
            navController.popBackStack()
        },
        title = mainViewModel.queryType.value!!.categoryTitle
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            QueryListScreenContents(coroutineScope,navController, user, mainViewModel, status, productsViewModel)
        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun QueryListScreenContents(coroutineScope: CoroutineScope, navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, productsViewModel: ProductsViewModel) {



    LaunchedEffect(key1 = Unit) {
        productsViewModel.getPaginatedProductsByQuery(mainViewModel.queryType.value!!)
    }

    val productList by productsViewModel.products.observeAsState(listOf())

    val lazyGridState = rememberLazyGridState()





    val isLoading by productsViewModel.isLoading.collectAsState()
    val isEndReached by productsViewModel.isEndReached.collectAsState()
    val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = isLoading)


//    SwipeRefresh(
//        swipeEnabled = false,
//
//        state = swipeRefreshState,
//        onRefresh = {
//            coroutineScope.launch {
//                //productsViewModel.getPaginatedProductsByQuery(fireStoreViewModel.queryType.value!!)
//            }
//        }
//    ) {
    Column(modifier = Modifier.fillMaxSize()){
        LazyVerticalGrid(
//            modifier = Modifier.weight(when(isLoading){
//                true -> {0.9F}
//                false -> {1F}}
//            ),
            state = lazyGridState,
            columns = GridCells.Adaptive(200.dp),
            content = {

//                if (lazyGridState.layoutInfo.visibleItemsInfo.lastOrNull() != null) {
//                CoroutineScope(Dispatchers.Unconfined).launch {
//                    if (lazyGridState.layoutInfo.visibleItemsInfo.lastOrNull()?.index!! >= productList.lastIndex && !isLoading) {
//                        productsViewModel.getPaginatedProductsByQuery(fireStoreViewModel.queryType.value!!)
//                    }
//                }
//            }
                items(productList.size) {index ->
                    val it = productList[index]
                    if (index >= productList.size - 1 && !isLoading && !isEndReached){
                        productsViewModel.getPaginatedProductsByQuery(mainViewModel.queryType.value!!)
                    }
                    ProductRowItem(
                        onClick = {
                            mainViewModel.currentViewedProduct.value = it
                            navController.navigate(Screen.ProductPage.route)
                        },
                        price = it.price,
                        title = it.name,
                        image = it.imageURI,
                        modifier = Modifier
                            .height(250.dp)
                            .width(190.dp)
                            .padding(10.dp)

                    )
                }
                item(span = { GridItemSpan(this.maxLineSpan) }, content = {
                    if (isLoading){
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            CircularProgressIndicator()
                        }
                    }
                })


            }


        )






    }

    //}
}









