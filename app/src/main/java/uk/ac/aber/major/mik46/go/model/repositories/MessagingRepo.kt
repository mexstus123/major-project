package uk.ac.aber.major.mik46.go.model.repositories

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.UserChat
import uk.ac.aber.major.mik46.go.model.UserChatMessage
import java.util.*

interface MessagingRepo {

    fun removeCurrentListener()

    suspend fun sendMessage(user: User, receiverUID: String, message: UserChatMessage)

    suspend fun addImageToFirebaseStorage(imageByteArray: ByteArray,UID: String, imageTime: Date): Uri

    suspend fun updateUsersMessages(user: User, receiverUID: String): MutableList<UserChatMessage>

    suspend fun initializeUsersMessagesListener(user: User, receiverUID: String, messageList: MutableLiveData<List<UserChatMessage>>)

}