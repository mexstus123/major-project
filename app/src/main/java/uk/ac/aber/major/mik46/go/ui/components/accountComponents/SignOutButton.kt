package uk.ac.aber.major.mik46.go.ui.components

import android.app.Activity
import android.widget.Toast
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SignOutButton(
    modifier: Modifier,
    user: User,
    mainViewModel: MainViewModel = viewModel()
) {
    val thisActivity = LocalContext.current as Activity
    Button(
        modifier = modifier
            .padding(10.dp,20.dp),
        onClick = {
            CoroutineScope(Dispatchers.IO).launch {
                mainViewModel.removeRegistrationToken()
                signOut(thisActivity,user, mainViewModel)
            }



        }
    ) {
        Text(
            text = "Logout",
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp
        )
    }
}

private fun signOut(activity: Activity, user: User, mainViewModel: MainViewModel) {

    // get the google account
    val googleSignInClient: GoogleSignInClient

    // configure Google SignIn
    val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken(R.string.default_web_client_id.toString())
        .requestEmail()
        .build()

    googleSignInClient = GoogleSignIn.getClient(activity, gso)

    // Sign Out of all accounts
    user.authentication.signOut()
    googleSignInClient.signOut()
        .addOnSuccessListener {
            mainViewModel.userSignedOut()
            Toast.makeText(activity, "Sign Out Successful", Toast.LENGTH_SHORT).show()

        }
        .addOnFailureListener {
            Toast.makeText(activity, "Sign Out Failed", Toast.LENGTH_SHORT).show()
        }

}