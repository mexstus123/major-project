package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Nature
import androidx.compose.material3.IconButtonColors
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector

/**
 * Represents a group of buttons with an associated icon and onClick action.
 *
 * @param icon The icon to display with the button.
 * @param buttonText The text to display on the button.
 * @param onClick The action to perform when the button is clicked.
 */
data class IconButtonGroup(
    val icon: ImageVector = Icons.Default.Nature,
    val buttonText: String = "",
    val onClick: () -> Unit,
    val additionalText: String = "",
    val hasNavigation: Boolean = true,
    val iconColour: Boolean = false,
)
