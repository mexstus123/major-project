package uk.ac.aber.major.mik46.go.ui.components


import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.outlined.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.ui.navigation.Screen
import uk.ac.aber.major.mik46.go.ui.navigation.mainScreens

/**
 * Composable function that creates a bottom navigation bar with filled/outlined icons and different colours for each screen.
 *
 * @param navController the [NavController] to navigate between screens
 */
@Composable
fun MainPageNavigationBar(
    navController: NavController
){
    // Map each screen to an IconGroup object containing the icons and label for the screen
    val icons = mapOf (
        Screen.Map to IconGroup(
            filledIcon = Icons.Filled.LocationOn,
            outlineIcon = Icons.Outlined.LocationOn,
            label = stringResource(id = R.string.map_icon)
        ),
        Screen.Products to IconGroup(
            filledIcon = Icons.Filled.Store,
            outlineIcon = Icons.Outlined.Store,
            label = stringResource(id = R.string.products_icon)
        ),
        Screen.Chats to IconGroup(
            filledIcon = Icons.Filled.Chat,
            outlineIcon = Icons.Outlined.Chat,
            label = stringResource(id = R.string.chats_icon)
        ),
        Screen.Account to IconGroup(
            filledIcon = Icons.Filled.Person,
            outlineIcon = Icons.Outlined.Person,
            label = stringResource(id = R.string.accounts_icons)
        )

    )

    NavigationBar(
    ) {
        // Get the current back stack entry for the nav controller
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        // Get the destination for the current back stack entry
        val currentDestination = navBackStackEntry?.destination

        // For each screen, create a NavigationBarItem with the appropriate icon and label
        mainScreens.forEach { screen ->
            // Determine if the screen is selected based on its route being in the current destination's hierarchy
            val isSelected = currentDestination
                ?.hierarchy?.any { it.route == screen.route } == true
            // Get the label text for the screen
            val labelText = icons[screen]!!.label
            NavigationBarItem(
                // Set the icon for the screen
                icon = {
                    Icon(
                        imageVector = (
                                if (isSelected)
                                    icons[screen]!!.filledIcon
                                else
                                    icons[screen]!!.outlineIcon),
                        contentDescription = labelText
                    )
                },
                // Set the label for the screen
                label = { Text(labelText) },
                // Set the selected state for the screen
                selected = isSelected,
                // Set the onClick listener for the screen
                onClick = {
                    navController.navigate(screen.route) {
                        // Pop up to the start destination and save the state
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        // Set the launch mode to singleTop
                        launchSingleTop = true
                        // Restore the state when navigating
                        restoreState = true
                    }
                }
            )
        }
    }
}