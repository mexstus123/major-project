package uk.ac.aber.major.mik46.go.model.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import uk.ac.aber.major.mik46.go.model.Product
import uk.ac.aber.major.mik46.go.model.repositories.*

class MapsViewModel constructor(
    private val MapsRepository: MapsRepo = MapsRepository(),
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val coroutineScope: CoroutineScope = CoroutineScope(ioDispatcher)

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    private val _products: MutableLiveData<MutableList<Product>> = MutableLiveData<MutableList<Product>>()
    val products: MutableLiveData<MutableList<Product>> get() = _products

    private val _productsByDate: MutableLiveData<MutableList<Product>> = MutableLiveData<MutableList<Product>>()
    val productsByDate: MutableLiveData<MutableList<Product>> get() = _productsByDate


    fun getManufacturersLocations() {

    }


}
