package uk.ac.aber.major.mik46.go.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.components.InnerNavigationScaffold
import uk.ac.aber.major.mik46.go.ui.navigation.Screen
import uk.ac.aber.major.mik46.go.ui.screens.signIn.SignInScreen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AboutScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()
    val isSignedIn by mainViewModel.isSignedIn.collectAsState()








    InnerNavigationScaffold(
        onClickBack = {
            navController.popBackStack()
        },
        title = stringResource(R.string.about)
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            if (!isSignedIn) {
                SignInScreen(coroutineScope = coroutineScope, user = user, status = status , navController = navController, mainViewModel = mainViewModel)
            }else{
                AboutScreenContents(coroutineScope, user, mainViewModel, status,navController)
            }


        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AboutScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController) {


    Column(
            verticalArrangement = Arrangement.Top,
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

        Spacer(modifier = Modifier.height(10.dp))

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 0.dp, 15.dp),
            text = stringResource(R.string.purpose),
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )


        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp),
            text = "description skdjlaksjd;ashdndf;knd ;adksjf dfk a dfij ajndofn ;oajdn ;fjn;n a;djnf;jan;d f;ajnd ;fjn a;djsnf ;jadn f;jand ;fjnasd fjnad ;fjn a;djfn ;ajds nf;ja ds;jf n;akjdn f;kjan d;fkjn a;kdjfn ;kjadsnfkjdshjdfkjbskdjfskdfnkdjs",
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
        )

        Divider(modifier = Modifier.padding(top = 15.dp, bottom = 5.dp))

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 0.dp, 15.dp),
            text = stringResource(R.string.how),
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )


        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp),
            text = "description skdjlaksjd;ashdndf;knd ;adksjf dfk a dfij ajndofn ;oajdn ;fjn;n a;djnf;jan;d f;ajnd ;fjn a;djsnf ;jadn f;jand ;fjnasd fjnad ;fjn a;djfn ;ajds nf;ja ds;jf n;akjdn f;kjan d;fkjn a;kdjfn ;kjadsnfkjdshjdfkjbskdjfskdfnkdjs",
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
        )

        Divider(modifier = Modifier.padding(top = 15.dp, bottom = 5.dp))

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 0.dp, 15.dp),
            text = stringResource(R.string.whats_next),
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )


        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp),
            text = "description skdjlaksjd;ashdndf;knd ;adksjf dfk a dfij ajndofn ;oajdn ;fjn;n a;djnf;jan;d f;ajnd ;fjn a;djsnf ;jadn f;jand ;fjnasd fjnad ;fjn a;djfn ;ajds nf;ja ds;jf n;akjdn f;kjan d;fkjn a;kdjfn ;kjadsnfkjdshjdfkjbskdjfskdfnkdjs",
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
        )

        Divider(modifier = Modifier.padding(top = 15.dp, bottom = 0.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(60.dp)
                .clickable(onClick = {navController.navigate(Screen.SupportTheDev.route)}),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                modifier = Modifier
                    .weight(0.15F)
                    .fillMaxSize(0.5F)
                    .padding(horizontal = 10.dp),
                imageVector = Icons.Filled.Paid,
                contentDescription = stringResource(R.string.support_the_team),
                tint = MaterialTheme.colorScheme.primary
            )

            Text(
                modifier = Modifier
                    .weight(0.7F)
                    .fillMaxWidth(),
                text = stringResource(R.string.support_the_team),
                textAlign = TextAlign.Start,
                fontWeight = FontWeight.SemiBold,
                fontSize = 16.sp
            )
            Icon(
                modifier = Modifier
                    .weight(0.15F)
                    .fillMaxSize(0.5F)
                    .padding(horizontal = 10.dp),
                imageVector = Icons.Default.KeyboardArrowRight,
                contentDescription = "arrow ${stringResource(R.string.support_the_team)}"
            )
        }
    }


}











