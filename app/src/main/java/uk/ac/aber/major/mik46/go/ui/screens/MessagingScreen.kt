package uk.ac.aber.major.mik46.go.ui.screens

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.ui.components.*



@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun MessagingScreen(
    navController: NavHostController,
    user: User,
    mainViewModel: MainViewModel,
    status: ConnectivityObserver.Status
) {


    val typingState = rememberSaveable { mutableStateOf(false) }
    val receiverUID = mainViewModel.manufacturer.value!!.UID
    val numberOfMessages: MutableState<Int> = remember { mutableStateOf(10) }

    val oldMessagesList = mainViewModel.usersOldMessages.observeAsState(listOf())



    LaunchedEffect(key1 = Unit) {
        mainViewModel.initializeUsersMessagesListener(
            receiverUID,
        )

    }

    val showTimeStateList = remember {
        listOf<MutableState<Boolean>>(
            mutableStateOf(false),
            mutableStateOf(false),
            mutableStateOf(false),
            mutableStateOf(false),
            mutableStateOf(false),
            mutableStateOf(false)
        )
    }


    val messageList by mainViewModel.usersMessagesListener.observeAsState(listOf())

    BackHandler(enabled = true, onBack = {
        mainViewModel.removeCurrentListener()
        mainViewModel.emptyOldMessageList()
        navController.popBackStack()
    })


    MessagingScaffold(
        status = status,
        mainViewModel = mainViewModel,
        navController = navController,
        user = user,
        typingState = typingState,
        showTimeState = showTimeStateList
    ) { innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            val isLoading by mainViewModel.isLoading.collectAsState()
            val isEndReached by mainViewModel.isEndReached.collectAsState()
            val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = isLoading)
            val lazyListState = rememberLazyListState()



            LazyColumn(
                state = lazyListState,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = 10.dp),
                reverseLayout = true
            ) {
                items(messageList.size) { index ->
                    val message = messageList[index]
                    if (index >= messageList.size - 1 && !isLoading && !isEndReached && oldMessagesList.value.isEmpty()) {
                        if (oldMessagesList.value != null) {
                            numberOfMessages.value = messageList.size + oldMessagesList.value.size
                        } else {
                            numberOfMessages.value = messageList.size
                        }

                        mainViewModel.updateUsersMessages(
                            receiverUID
                        )
                    }
                    Spacer(modifier = Modifier.height(50.dp))
                    MessageBubble(
                        user = user,
                        messageContent = message.content,
                        messageSender = message.sender,
                        messageTime = message.getTimeFormat(),
                        isMessageImage = message.image,
                        showTimeState = when (index) {
                            0 -> showTimeStateList[0]
                            1 -> showTimeStateList[1]
                            2 -> showTimeStateList[2]
                            3 -> showTimeStateList[3]
                            4 -> showTimeStateList[4]
                            5 -> showTimeStateList[5]
                            else -> remember {
                                mutableStateOf(false)
                            }
                        }
                    )

                }
                items(oldMessagesList.value.size) { index ->
                    val message = oldMessagesList.value[index]

                    if (index >= oldMessagesList.value.size - 1 && !isLoading && !isEndReached) {

                        if (oldMessagesList.value != null) {
                            numberOfMessages.value = messageList.size + oldMessagesList.value.size
                        } else {
                            numberOfMessages.value = messageList.size
                        }

                        mainViewModel.updateUsersMessages(
                            receiverUID
                        )
                    }

                    Spacer(modifier = Modifier.height(50.dp))
                    MessageBubble(
                        user = user,
                        messageContent = message.content,
                        messageSender = message.sender,
                        messageTime = message.getTimeFormat(),
                        isMessageImage = message.image
                    )

                }

                item {
                    if (isLoading) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            CircularProgressIndicator()
                        }
                    }
                }
            }
        }
    }
}

