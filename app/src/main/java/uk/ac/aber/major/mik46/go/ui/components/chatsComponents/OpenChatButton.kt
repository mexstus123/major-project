package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.ui.theme.GoLocalUKTheme


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OpenChatButton(
    onClick: () -> Unit = {},
    height: Int = 65,
    width: Int = 155,
    modifier: Modifier = Modifier
){

    Surface(
        modifier = modifier
            .height(height.dp)
            .width(width.dp)
            .fillMaxHeight(0.4f)
            .padding(10.dp),
        shape = RoundedCornerShape(15.dp),
        onClick = onClick,
        color = MaterialTheme.colorScheme.primary
    ) {
        Row(
            modifier = Modifier.fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
            ) {
            Spacer(
                modifier = Modifier
                    .weight(0.05F),
            )
            Text(
                text = stringResource(R.string.contact),
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.65F)
                    .padding(10.dp, 10.dp),
                fontSize = 17.sp,
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.SemiBold
            )


            Icon(
                painter = painterResource(R.drawable.message_notification_icon),
                contentDescription = stringResource(R.string.message_button),
                // specify the tint color for the icon
                tint = MaterialTheme.colorScheme.onPrimary,
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.25F)
                    .padding(0.dp,3.dp,6.dp,3.dp,)
            )
            Spacer(
                modifier = Modifier
                    .weight(0.05F),
            )
        }


    }
}

@Composable
@Preview
private fun DropDownPreview() {
    GoLocalUKTheme(dynamicColor = false) {
        OpenChatButton(onClick = {})
    }
}