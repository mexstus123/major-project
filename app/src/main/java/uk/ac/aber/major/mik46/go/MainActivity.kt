package uk.ac.aber.major.mik46.go

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.messaging.FirebaseMessaging
import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.connectivity.NetworkConnectivityObserver
import uk.ac.aber.major.mik46.go.model.forms.registration.RegistrationViewModel
import uk.ac.aber.major.mik46.go.model.notifications.FirebaseNotificationService
import uk.ac.aber.major.mik46.go.model.viewmodels.AuthenticationViewModel
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.viewmodels.PermissionsViewModel
import uk.ac.aber.major.mik46.go.ui.components.*
import uk.ac.aber.major.mik46.go.ui.navigation.BuildNavigationGraph
import uk.ac.aber.major.mik46.go.ui.navigation.openAppSettings
import uk.ac.aber.major.mik46.go.ui.theme.GoLocalUKTheme


class MainActivity(user: User = User()) : ComponentActivity() {

    private var currentUser = user

    val mainViewModel: MainViewModel by viewModels()
    val authenticationViewModel: AuthenticationViewModel by viewModels()





    private val startUpPermissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        arrayOf(
            Manifest.permission.POST_NOTIFICATIONS,
            Manifest.permission.ACCESS_FINE_LOCATION,
        )
    } else {
        arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
        )
    }



//To save a value to it



    override fun onStop() {
        super.onStop()
        //TODO sync user to db
        setAllUsersSettings(this, mainViewModel)
        if(authenticationViewModel.hasTempSignIn.value){
            authenticationViewModel.deleteCurrentUser()
        }

    }

    override fun onStart() {
        super.onStart()
        getAllUsersSettings(this, mainViewModel)
    }
    override fun onDestroy() {
        super.onDestroy()
        if(authenticationViewModel.hasTempSignIn.value){
            authenticationViewModel.deleteCurrentUser()
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseNotificationService.sharedPref = this.getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            if (FirebaseNotificationService.token != it){
                FirebaseNotificationService.oldToken = FirebaseNotificationService.token
            }
            FirebaseNotificationService.token = it
        }


        getAllUsersSettings(this, mainViewModel)



        checkUsersLocationPermission(this@MainActivity,mainViewModel)
        if (mainViewModel.locationIsGranted.value) {
            getUsersLastKnownLocation(this, mainViewModel)
        }


        setContent {
            //setup internet connectivity observer
            val status by NetworkConnectivityObserver(application).observe().collectAsState(initial = ConnectivityObserver.Status.Unavailable)

            val permissionViewModel = viewModel<PermissionsViewModel>()
            val dialogQueue = permissionViewModel.visiblePermissionDialogQueue

            val multiplePermissionResultLauncher = rememberLauncherForActivityResult(
                contract = ActivityResultContracts.RequestMultiplePermissions(),
                onResult = { perms ->
                    startUpPermissions.forEach { permission ->
                        permissionViewModel.onPermissionResult(
                            permission = permission,
                            isGranted = perms[permission] == true
                        )
                    }
                }
            )




            LaunchedEffect(key1 = Unit){

                currentUser.setup()
                mainViewModel.setupCurrentUser()

                multiplePermissionResultLauncher.launch(startUpPermissions)

                checkUsersLocationPermission(this@MainActivity,mainViewModel)
                if (mainViewModel.locationIsGranted.value) {
                    getUsersLastKnownLocation(this@MainActivity, mainViewModel)
                }
            }

            GoLocalUKTheme(
                darkTheme = mainViewModel.isDarkTheme.observeAsState().value?: isSystemInDarkTheme()
            ) {

                dialogQueue
                    .reversed()
                    .forEach { permission ->
                        PermissionDialog(
                            permissionTextProvider = when (permission) {
                                Manifest.permission.ACCESS_FINE_LOCATION -> {
                                    LocationPermissionTextProvider()
                                }
                                Manifest.permission.POST_NOTIFICATIONS -> {
                                    NotificationPermissionTextProvider()
                                }
                                else -> return@forEach
                            },
                            isPermanentlyDeclined = !shouldShowRequestPermissionRationale(
                                permission
                            ),
                            onDismiss = {
                                permissionViewModel.dismissDialog()
                                checkUsersLocationPermission(this,mainViewModel)
                                if (mainViewModel.locationIsGranted.value) {
                                    getUsersLastKnownLocation(this@MainActivity, mainViewModel)
                                }
                            },
                            onConfirmClick = {
                                permissionViewModel.dismissDialog()
                                multiplePermissionResultLauncher.launch(
                                    arrayOf(permission)
                                )
                            },
                            onGoToAppSettingsClick = {
                                this.openAppSettings()
                            }
                        )
                    }
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ViewCompat.setOnApplyWindowInsetsListener(findViewById(android.R.id.content))
                    {view, insets ->

                        val bottom = insets.getInsets(WindowInsetsCompat.Type.ime()).bottom
                        view.updatePadding(bottom = bottom)
                        insets
                    }

                    BuildNavigationGraph(currentUser, mainViewModel, status,authenticationViewModel)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // result returned from launching the intent from GoogleSignInApi.getSignInIntent()
        if (requestCode == 100) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            val exception = task.exception
            if (task.isSuccessful) {
                try {
                    // Google SignIn was successful, authenticate with Firebase
                    val account = task.getResult(ApiException::class.java)!!
                    firebaseAuthWithGoogle(account.idToken!!, currentUser)
                } catch (e: Exception) {
                    // Google SignIn Failed
                    Log.d("SignIn", "Google SignIn Failed")
                }
            } else {
                Log.d("SignIn", exception.toString())
                Toast.makeText(this, "SignIn Failed", Toast.LENGTH_SHORT).show()
            }
        }

    }
    private fun firebaseAuthWithGoogle(idToken: String, user: User) {
        val myViewModel: MainViewModel by viewModels()
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        myViewModel.currentUser.value!!.authentication.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // SignIn Successful
                    Toast.makeText(this, "SignIn Successful", Toast.LENGTH_SHORT).show()
                    myViewModel.setupCurrentUser()
                    myViewModel.userSignedIn()
                    //this.recreate()



                } else {
                    // SignIn Failed
                    Toast.makeText(this, "SignIn Failed", Toast.LENGTH_SHORT).show()
                }
            }
    }
}

