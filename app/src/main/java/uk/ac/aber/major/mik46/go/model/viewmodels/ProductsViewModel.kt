package uk.ac.aber.major.mik46.go.model.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import uk.ac.aber.major.mik46.go.model.Product
import uk.ac.aber.major.mik46.go.model.ProductQueryType
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.repositories.*

class ProductsViewModel constructor(
    private val productsRepository: ProductsRepo = ProductsRepository(),
    private val mainRepository: MainRepository = MainRepository(),
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val _manufacturer : MutableLiveData<User> = MutableLiveData<User>()
    val manufacturer : MutableLiveData<User> get() = _manufacturer

    private val coroutineScope: CoroutineScope = CoroutineScope(ioDispatcher)

    private val _isLoading = MutableStateFlow(true)
    val isLoading = _isLoading.asStateFlow()

    private val _isEndReached = MutableStateFlow(false)
    val isEndReached = _isEndReached.asStateFlow()

    private val _products: MutableLiveData<MutableList<Product>> = MutableLiveData<MutableList<Product>>()
    val products: MutableLiveData<MutableList<Product>> get() = _products

    private val _productsPageLists: MutableLiveData<MutableMap<ProductQueryType,List<Product>>> = MutableLiveData<MutableMap<ProductQueryType,List<Product>>>()
    val productsPageLists: MutableLiveData<MutableMap<ProductQueryType,List<Product>>> get() = _productsPageLists



    fun getPaginatedProductsByQuery(queryCondition: ProductQueryType){
        //using a list variable here in order to keep the creation of the list on the IO thread
        var productsList = mutableListOf<Product>()
        _isLoading.value = true

        queryTypeListPagination(queryCondition)

    }


    fun emptyProductsList(){
        _products.value = mutableListOf()
    }

    fun updateProductsScreen(){
        //using a list variable here in order to keep the creation of the list on the IO thread
        _isLoading.value = true
        coroutineScope.launch {
            val productsLists = mutableMapOf<ProductQueryType, List<Product>>()
            ProductQueryType.values().forEach {
                productsLists[it] = productsRepository.getProductsByQuery(it, 8)
            }
            withContext(Dispatchers.Main) {
                _productsPageLists.value = productsLists
            }
            _isLoading.value = false
        }
    }

    private fun queryTypeListPagination(queryCondition: ProductQueryType){
        coroutineScope.launch {
            _isEndReached.value = false
            var productsList = mutableListOf<Product>()
            if (!_products.value.isNullOrEmpty()) {
                productsList = productsRepository.getProductsByQuery(queryCondition, 12).toMutableList()
                withContext(Dispatchers.Main) {
                    _products.value!!.addAll(productsList)
                }
            } else {
                productsList = productsRepository.getProductsByQuery(queryCondition, 12).toMutableList()
                withContext(Dispatchers.Main) {
                    _products.value = productsList
                }
            }
            if (productsList.size != 12){
                _isEndReached.value = true
            }
            _isLoading.value = false
        }
    }


    fun getManufacturer(UID: String) {
        _isLoading.value = true
        coroutineScope.launch {
            var user = User()

                user = productsRepository.getUserFromFireStoreByUID(UID)
                withContext(Dispatchers.Main) {
                    _manufacturer.value = user
                    _isLoading.value = false
                }
            }

    }

}
