package uk.ac.aber.major.mik46.go.model.forms.signIn

data class EmailSignInState(
    val email: String = "",
    val emailError: String? = null,
    val password: String = "",
    val passwordError: String? = null,
)
