package uk.ac.aber.major.mik46.go.model.viewmodels

import android.net.Uri
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.repositories.*
import uk.ac.aber.major.mik46.go.ui.components.CustomClusterItem
import java.util.*

class MainViewModel constructor(
    private val mainRepository: MainRepo = MainRepository(),
    private val messagingRepository: MessagingRepo = MessagingRepository(),//passing these through constructor to allow for fake repo testing
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    val productRepo = ProductsRepository()

    var authentication: FirebaseAuth = FirebaseAuth.getInstance()


    private val _currentUser : MutableLiveData<User> = MutableLiveData<User>(User())
    val currentUser : MutableLiveData<User> get() = _currentUser

    private val _manufacturer : MutableLiveData<User> = MutableLiveData<User>()
    val manufacturer : MutableLiveData<User> get() = _manufacturer

    private val _isSignedIn = MutableStateFlow(false)
    val isSignedIn = _isSignedIn.asStateFlow()

    val userUnknownUserMessagesSetting : MutableState<Boolean> = mutableStateOf(false)

    val userShowLocationSetting : MutableState<Boolean> = mutableStateOf(true)

    val userLanguageSetting : MutableLiveData<String>  = MutableLiveData<String>()

    val userTextSizeSetting : MutableLiveData<String>  = MutableLiveData<String>()

    val userNotificationSetting : MutableState<Boolean> = mutableStateOf(true)

    val userMessageCensorshipSetting : MutableState<Boolean> = mutableStateOf(false)

    val isDarkTheme : MutableLiveData<Boolean?> = MutableLiveData<Boolean?>()

    private val _locationIsGranted = MutableStateFlow(false)
    val locationIsGranted = _locationIsGranted.asStateFlow()

    private val _queryType : MutableLiveData<ProductQueryType> = MutableLiveData<ProductQueryType>()
    val queryType : MutableLiveData<ProductQueryType> get() = _queryType

    var currentViewedProduct = MutableLiveData<Product>()

    private val coroutineScope: CoroutineScope = CoroutineScope(ioDispatcher)

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    private val _isEndReached = MutableStateFlow(false)
    val isEndReached = _isEndReached.asStateFlow()

    private val _usersChats: MutableLiveData<List<UserChat>> = MutableLiveData<List<UserChat>>()
    val usersChats: MutableLiveData<List<UserChat>> get() = _usersChats

    private val _usersMessagesListener: MutableLiveData<List<UserChatMessage>> = MutableLiveData<List<UserChatMessage>>()
    val usersMessagesListener: MutableLiveData<List<UserChatMessage>> get() = _usersMessagesListener


    private val _usersOldMessages: MutableLiveData<MutableList<UserChatMessage>> = MutableLiveData<MutableList<UserChatMessage>>()
    val usersOldMessages: MutableLiveData<MutableList<UserChatMessage>> get() = _usersOldMessages

    private val _manufacturersList: MutableLiveData<List<User>> = MutableLiveData<List<User>>()
    val manufacturersList: MutableLiveData<List<User>> get() = _manufacturersList

    private val _manufacturersProducts: MutableLiveData<List<Product>> = MutableLiveData<List<Product>>()
    val manufacturersProducts: MutableLiveData<List<Product>> get() = _manufacturersProducts

    private val _manufacturerMarkers: MutableLiveData<MutableList<CustomClusterItem>> = MutableLiveData<MutableList<CustomClusterItem>>()
    val manufacturerMarkers: MutableLiveData<MutableList<CustomClusterItem>> get() = _manufacturerMarkers


    init {
        _isSignedIn.value = authentication.currentUser != null
    }

    fun setLocationPermission(isGranted: Boolean){
        _locationIsGranted.value = isGranted
    }

    fun putCurrentUserData(){
        coroutineScope.launch {
            mainRepository.putCurrentUserData(_currentUser.value!!)
        }
    }

    fun setupCurrentUser(){

        if(authentication.currentUser != null){

            coroutineScope.launch {
                if (authentication.currentUser?.displayName!!.isEmpty()){
                authentication.currentUser!!.delete()
                authentication.signOut()
                    _isSignedIn.value = false

            }else{
                    val user = mainRepository.getUserFromFireStoreByUID(authentication.currentUser!!.uid)
                    user.userName = authentication.currentUser?.displayName?: ""
                    user.profilePictureURI = authentication.currentUser?.photoUrl?.toString()?: ""
                    user.emailAddress = authentication.currentUser?.email?: ""
                    user.phoneNumber = authentication.currentUser?.phoneNumber?: ""
                    user.UID = authentication.currentUser?.uid?: ""
                    user.location = _currentUser.value!!.location

                    mainRepository.userSetup(user)
                    withContext(Dispatchers.Main) {
                        _currentUser.value = user
                    }
                }




            }
        }
    }

    fun userSignedIn(){
        coroutineScope.launch {
            _isSignedIn.value = true
        }

    }

    fun userSignedOut(){
        _currentUser.value = User()
        _isSignedIn.value = false

    }

    fun setCurrentUserLocation(geoPoint: GeoPoint){
        _currentUser.value!!.location = geoPoint
    }

    fun removeCurrentListener(){
        _usersMessagesListener.value = emptyList()
        coroutineScope.launch {
            messagingRepository.removeCurrentListener()
        }
    }

    fun emptyOldMessageList(){
            _usersOldMessages.value = mutableListOf<UserChatMessage>()
    }

    fun removeRegistrationToken(){
        coroutineScope.launch {
            mainRepository.removeRegistrationToken(_currentUser.value!! )
        }
    }

    fun getAllManufacturers(){
        if (_manufacturerMarkers.value.isNullOrEmpty()){
            coroutineScope.launch {
                _isLoading.value = true
                val markerList = mutableListOf<CustomClusterItem>()
                val manufacturerList = mainRepository.getAllManufacturers()
                manufacturerList.forEach {
                    markerList.add(
                        CustomClusterItem(
                            markerPosition = LatLng(it.businessLocation.latitude,it.businessLocation.longitude) ,
                            username = it.userName,
                            description = it.businessDescription,
                            manufacturerUID = it.UID

                    ))
                }
                withContext(Dispatchers.Main){
                    _manufacturerMarkers.value = markerList
                    _manufacturersList.value = manufacturerList
                }
                _isLoading.value = false
            }
        }
    }

    fun getProductsByUID(UID: String){
        coroutineScope.launch {
            _isLoading.value = true
            val manufacturerProducts = mainRepository.getProductsByUID(UID)
            withContext(Dispatchers.Main){
                _manufacturersProducts.value = manufacturerProducts
            }
            _isLoading.value = false
        }


    }

    fun mapSetManufacturerByID(UID: String){
        _manufacturersList.value?.forEach {
            if (it.UID == UID){
                manufacturer.value = it
            }
        }
    }

    suspend fun addImageToFirebaseStorage(imageByteArray: ByteArray, imageTime: Date): Uri{
        val newImageUri = coroutineScope.async {
            return@async messagingRepository.addImageToFirebaseStorage(imageByteArray,_currentUser.value!!.UID,imageTime)
        }
        return newImageUri.await()
    }

     fun updateUsersChats(){
         coroutineScope.launch {
            _isLoading.value = true
              val newList = mainRepository.updateUsersChats(_currentUser.value!!)
              withContext(Dispatchers.Main){
                  _usersChats.value = newList
              }
            _isLoading.value = false

        }
    }

    fun sendUserMessage( receiverUID: String, message: UserChatMessage){
        coroutineScope.launch {
            messagingRepository.sendMessage(_currentUser.value!!, receiverUID,message)
        }
    }

    fun updateUsersMessages( receiverUID: String){
        //using a list variable here in order to keep the creation of the list on the IO thread
        var messageList = mutableListOf<UserChatMessage>()
        coroutineScope.launch {
            _isLoading.value = true
            if (!_usersOldMessages.value.isNullOrEmpty()){
                messageList = messagingRepository.updateUsersMessages(_currentUser.value!!, receiverUID)
                withContext(Dispatchers.Main) {
            _usersOldMessages.value!!.addAll(messageList)
                    }
                }
            else{
                messageList = messagingRepository.updateUsersMessages(_currentUser.value!!, receiverUID)
                withContext(Dispatchers.Main) {
                    _usersOldMessages.value = messageList
                }
            }
            if (messageList.size != 10){
                _isEndReached.value = true
            }
            _isLoading.value = false
        }
    }


     fun initializeUsersMessagesListener( receiverUID: String){
        coroutineScope.launch {
            _isLoading.value = true
            _isEndReached.value = false
            messagingRepository.initializeUsersMessagesListener(_currentUser.value!!, receiverUID, _usersMessagesListener)

            _isLoading.value = false
        }
    }

}