package uk.ac.aber.major.mik46.go.model.forms.editProfile

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.forms.validation.*

class EditProfileViewModel(
    private val validateUserName: ValidateUserName = ValidateUserName(),
    private val validateEmail: ValidateEmail = ValidateEmail(),
    private val validatePassword: ValidatePassword = ValidatePassword(),
    private val validateRepeatedPassword: ValidateRepeatedPassword = ValidateRepeatedPassword(),
    private val validateTerms: ValidateTerms = ValidateTerms(),
    private val validateDescription: ValidateDescription = ValidateDescription(),
): ViewModel() {

    private var currentUser = User()

    var state by mutableStateOf(
        EditProfileState(
            userName = currentUser.userName,
            email = currentUser.emailAddress,
            phoneNumber = currentUser.phoneNumber,
            password = "",
            repeatedPassword = "",
            description = ""
        )
    )

    private val _isButtonEnabled = MutableStateFlow(false)
    val isButtonEnabled = _isButtonEnabled.asStateFlow()

    val isCleared = MutableStateFlow(false)

    private val _isPasswordValid = MutableStateFlow(false)
    val isPasswordValid = _isPasswordValid.asStateFlow()

    private val _isUserNameValid = MutableStateFlow(false)
    val isUserNameValid = _isUserNameValid.asStateFlow()

    private val _isEmailValid = MutableStateFlow(false)
    val isEmailValid = _isEmailValid.asStateFlow()

    private val _isPhoneValid = MutableStateFlow(false)
    val isPhoneValid = _isPhoneValid.asStateFlow()

    val isTermsValid = mutableStateOf(false)

    val isDescriptionValid = mutableStateOf(false)

    fun setCurrentUser(user: User) {
        currentUser = user
        state = state.copy(
            userName = currentUser.userName,
            email = currentUser.emailAddress,
            phoneNumber = currentUser.phoneNumber,
            password = "",
            repeatedPassword = ""
        )
    }

    fun onEvent(event: EditProfileEvent) {
        when (event) {
            is EditProfileEvent.UserNameChanged -> {
                if (isCleared.value) {
                    state = state.copy(userName = "")
                } else {
                    state = state.copy(userName = event.userName)
                }

                _isButtonEnabled.value = currentUser.userName != event.userName
            }
            is EditProfileEvent.DescriptionChanged -> {
                state = state.copy(description = event.description)
            }
            is EditProfileEvent.PhoneNumberChanged -> {
                if (isCleared.value) {
                    state = state.copy(phoneNumber = "")
                    isCleared.value = false
                } else {
                    state = state.copy(phoneNumber = event.phoneNumber)
                }

                _isButtonEnabled.value = currentUser.userName != event.phoneNumber
            }
            is EditProfileEvent.EmailChanged -> {
                if (isCleared.value) {
                    state = state.copy(email = "")
                    isCleared.value = false
                } else {
                    state = state.copy(email = event.email)
                }

                _isButtonEnabled.value = currentUser.emailAddress != event.email
            }
            is EditProfileEvent.PasswordChanged -> {
                if (isCleared.value) {
                    state = state.copy(password = "")
                    isCleared.value = false
                } else {
                    state = state.copy(password = event.password)
                }

                _isButtonEnabled.value = true
            }
            is EditProfileEvent.RepeatedPasswordChanged -> {
                if (isCleared.value) {
                    state = state.copy(repeatedPassword = "")
                    isCleared.value = false
                } else {
                    state = state.copy(repeatedPassword = event.repeatedPassword)
                }

                _isButtonEnabled.value = true
            }
            is EditProfileEvent.AcceptTerms -> {
                state = state.copy(acceptedTerms = event.isAccepted)

                _isButtonEnabled.value = event.isAccepted
            }
            is EditProfileEvent.Submit -> {
                submitData()
            }
        }
    }

    private fun submitData() {
        viewModelScope.launch {
            val userNameResult = validateUserName.executeRegistration(state.userName)
            val emailResult = validateEmail.executeRegistration(state.email)
            val passwordResult = validatePassword.executeRegistration(state.password)
            val repeatedPasswordResult = validateRepeatedPassword.execute(
                state.password, state.repeatedPassword
            )
            val termsResult = validateTerms.execute(state.acceptedTerms)
            val descriptionResult = validateDescription.execute(state.description)



            if (passwordResult.successful && repeatedPasswordResult.successful) {
                _isPasswordValid.value = true
            }

            if (userNameResult.successful) {
                _isUserNameValid.value = true
            }

            if (emailResult.successful) {
                _isEmailValid.value = true
            }

            if (termsResult.successful) {
                isTermsValid.value = true
            }

            if (termsResult.successful && descriptionResult.successful) {
                isDescriptionValid.value = true
            }

            val hasError = listOf(
                userNameResult,
                emailResult,
                passwordResult,
                repeatedPasswordResult,
                termsResult
            ).any { !it.successful }


            if (hasError) {
                state = state.copy(
                    userNameError = userNameResult.errorMessage,
                    emailError = emailResult.errorMessage,
                    passwordError = passwordResult.errorMessage,
                    repeatedPasswordError = repeatedPasswordResult.errorMessage,
                    termsError = termsResult.errorMessage,
                    descriptionError = descriptionResult.errorMessage
                )

            }
        }

    }
}