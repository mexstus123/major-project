package uk.ac.aber.major.mik46.go.ui.screens.signIn

import android.widget.ProgressBar
import android.widget.Toast
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.forms.editProfile.EditProfileViewModel
import uk.ac.aber.major.mik46.go.model.viewmodels.AuthenticationViewModel
import uk.ac.aber.major.mik46.go.ui.components.InnerNavigationScaffold
import uk.ac.aber.major.mik46.go.ui.navigation.Screen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ValidateEmailScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status,authenticationViewModel: AuthenticationViewModel){

    val coroutineScope = rememberCoroutineScope()
    val focusManager = LocalFocusManager.current


    InnerNavigationScaffold(
        onClickBack = {
            authenticationViewModel.deleteCurrentUser()
            navController.popBackStack()
        },
        title = stringResource(R.string.validate_email)
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
                .pointerInput(Unit) { detectTapGestures(onTap = { focusManager.clearFocus() }) },
            color = MaterialTheme.colorScheme.background
        ) {
            ValidateEmailScreenContents(coroutineScope, user, mainViewModel, status,navController,authenticationViewModel)
        }
    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ValidateEmailScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController,authenticationViewModel: AuthenticationViewModel) {

    val editProfileViewModel: EditProfileViewModel = viewModel()


    val sentState = remember { mutableStateOf(false) }
    val context = LocalContext.current


    val isLoading = authenticationViewModel.isLoading.collectAsState()
    val hasSignIn = authenticationViewModel.hasTempSignIn.collectAsState()
    val isEmailVerified = authenticationViewModel.isEmailVerified.collectAsState()



    LaunchedEffect(key1 = hasSignIn.value) {
        authenticationViewModel.sendEmailVerification()
        CoroutineScope(Dispatchers.IO).launch{
            while (!isEmailVerified.value){
                delay(5000)
                authenticationViewModel.isEmailVerified()
            }
        }
    }



    LaunchedEffect(key1 = isEmailVerified.value) {
        if (isEmailVerified.value) {
            navController.popBackStack(Screen.Account.route, false)
        }

    }




    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 32.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(0.dp, 50.dp, 0.dp, 0.dp),
            text = "Email Verification",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp,
            color = MaterialTheme.colorScheme.onBackground
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 10.dp, 0.dp),
            text = "An email has been sent to you with a link to verify your account",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Normal,
            fontSize = 14.sp,
            color = MaterialTheme.colorScheme.outline
        )



        Spacer(modifier = Modifier.height(32.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            CircularProgressIndicator()
        }

        Spacer(modifier = Modifier.height(32.dp))

        Button(
            onClick = {
                if (hasSignIn.value){
                    authenticationViewModel.sendEmailVerification()
                }


            },
            modifier = Modifier
                .width(250.dp),
        ) {
            Text(
                modifier = Modifier,
                text = "Re-Send Email",
                fontWeight = FontWeight.SemiBold,
                textAlign = TextAlign.Center,
                fontSize = 18.sp,
            )
        }

    }
}









