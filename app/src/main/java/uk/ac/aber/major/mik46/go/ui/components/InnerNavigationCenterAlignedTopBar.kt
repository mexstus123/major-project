package uk.ac.aber.major.mik46.go.ui.components


import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import uk.ac.aber.major.mik46.go.ui.theme.GoLocalUKTheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun InnerNavigationCenterAlignedTopBar(
    onClickBack: () -> Unit = {},
    title: String,

){
    // the CenterAlignedTopAppBar composable displays the top app bar
    CenterAlignedTopAppBar(
        // specify the colors for the top app bar
        colors = TopAppBarDefaults.mediumTopAppBarColors(containerColor = MaterialTheme.colorScheme.surfaceVariant),
        // the title of the top app bar
        title = {
            // display the app title text
            Text(
                text = title,
                fontWeight = FontWeight.Bold,
            )
        },
        // the navigation icon for the top app bar
        navigationIcon = {
            // the IconButton displays the navigation icon and calls the onClick lambda when clicked

            BackNavigationButton(onClickBack, modifier = Modifier)
        }
    )
}

@Composable
@Preview
private fun QueryListTopBarPreview() {
    GoLocalUKTheme(dynamicColor = false) {
        InnerNavigationCenterAlignedTopBar(
        title = "String",

        )
    }
}
