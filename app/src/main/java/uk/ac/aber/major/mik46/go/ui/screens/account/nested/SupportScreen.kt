package uk.ac.aber.major.mik46.go.ui.screens

import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.forms.editProfile.EditProfileEvent
import uk.ac.aber.major.mik46.go.model.forms.editProfile.EditProfileViewModel
import uk.ac.aber.major.mik46.go.ui.components.InnerNavigationScaffold
import uk.ac.aber.major.mik46.go.ui.screens.signIn.SignInScreen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SupportScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()
    val isSignedIn by mainViewModel.isSignedIn.collectAsState()
    val focusManager = LocalFocusManager.current








    InnerNavigationScaffold(
        onClickBack = {
            navController.popBackStack()
        },
        title = stringResource(R.string.support)
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
                .pointerInput(Unit) { detectTapGestures(onTap = { focusManager.clearFocus() }) },
            color = MaterialTheme.colorScheme.background
        ) {
            if (!isSignedIn) {
                SignInScreen(coroutineScope = coroutineScope, user = user, status = status , navController = navController, mainViewModel = mainViewModel)
            }else{
                SupportScreenContents(coroutineScope, user, mainViewModel, status,navController)
            }


        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SupportScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status, navController: NavHostController) {


    val editProfileViewModel: EditProfileViewModel = viewModel()

    val reportDetails = rememberSaveable { mutableStateOf("") }



    val state = editProfileViewModel.state
    val context = LocalContext.current


    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 25.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(0.dp, 30.dp, 0.dp, 0.dp),
            text = "Send A Support Ticket",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp,
            color = MaterialTheme.colorScheme.onBackground
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 10.dp, 0.dp),
            text = "Fill out the form below and a member of our team will get back to you as soon as possible",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Normal,
            fontSize = 14.sp,
            color = MaterialTheme.colorScheme.outline
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 0.dp, 5.dp),
            text = stringResource(R.string.contact_email),
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )

        TextField(
            value = state.email,
            onValueChange = {
                editProfileViewModel.onEvent(EditProfileEvent.EmailChanged(it))
            },
            isError = state.emailError != null,
            modifier = Modifier.fillMaxWidth().padding(horizontal = 10.dp),
            placeholder = {
                Text(text = "Preferred Email")
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Email,
            ),
            singleLine = true,
        )

        Text(
            text = state.emailError ?: "",
            color = MaterialTheme.colorScheme.error,
            modifier = Modifier.align(Alignment.End)
        )


        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 5.dp, 0.dp, 5.dp),
            text = stringResource(R.string.problem_report),
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )

        TextField(
            value = reportDetails.value,
            onValueChange = {
                reportDetails.value = it
            },
            modifier = Modifier
                .fillMaxWidth()
                .height(300.dp),
            placeholder = {
                Text(text = "Enter Details Here...")
            },
            keyboardOptions = KeyboardOptions(
                capitalization = KeyboardCapitalization.Sentences,
            )

        )

        Spacer(modifier = Modifier.height(32.dp))

        Button(
            onClick = {
                editProfileViewModel.onEvent(EditProfileEvent.Submit)
            },
            modifier = Modifier
                .width(250.dp),
            enabled = editProfileViewModel.isButtonEnabled.collectAsState().value
        ) {
            Text(
                modifier = Modifier,
                text = "Send Report",
                fontWeight = FontWeight.SemiBold,
                textAlign = TextAlign.Center,
                fontSize = 18.sp,
            )
        }

    }
}










