package uk.ac.aber.major.mik46.go.model.forms.validation

import android.util.Patterns
import uk.ac.aber.major.mik46.go.model.repositories.FirebaseAuthRepository
import uk.ac.aber.major.mik46.go.model.repositories.MainRepo
import uk.ac.aber.major.mik46.go.model.repositories.MainRepository

class ValidateEmail(
    private val mainRepository: MainRepo = MainRepository(),
    private val authRepository: FirebaseAuthRepository = FirebaseAuthRepository()
) {

    suspend fun executeRegistration(email: String): ValidationResult {
        if(email.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "The email cannot be blank"
            )
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Invalid email"
            )
        }
        if(authRepository.emailAlreadyExists(email)) { //TODO add check for user email
            return ValidationResult(
                successful = false,
                errorMessage = "Email already in use"
            )
        }
        return ValidationResult(
            successful = true
        )
    }

    suspend fun executeSignIn(email: String): ValidationResult {
        if(email.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Invalid Email"
            )
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Invalid email"
            )
        }
        return ValidationResult(
            successful = true
        )
    }
}