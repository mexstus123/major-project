package uk.ac.aber.major.mik46.go.model.repositories

import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

interface FirebaseAuthRepo {

    val currentUser: FirebaseUser?

    suspend fun firebaseSignUpWithEmailAndPassword(email: String, password: String, username: String)

    suspend fun sendEmailVerification()

    suspend fun firebaseSignInWithEmailAndPassword(email: String, password: String): Boolean

    suspend fun reloadFirebaseUser()

    suspend fun sendPasswordResetEmail(email: String)

    fun signOut()

    suspend fun deleteCurrentUser()

    suspend fun isEmailVerified(): Boolean

    suspend fun hasPhoneNumber(): Boolean

    suspend fun usernameAlreadyExists(username: String): Boolean

    suspend fun emailAlreadyExists(email: String): Boolean

    suspend fun phoneNumberAlreadyExists(phoneNumber: String): Boolean


}