package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.ui.graphics.vector.ImageVector

/**
 * Represents a group of icons and a label.
 *
 * @param filledIcon The filled version of the icon.
 * @param outlineIcon The outline version of the icon.
 * @param label The label associated with the icon group.
 */
data class IconGroup(
    val filledIcon: ImageVector,
    val outlineIcon: ImageVector,
    val label: String
)
