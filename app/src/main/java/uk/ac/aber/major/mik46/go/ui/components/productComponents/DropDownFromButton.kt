package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.ui.theme.GoLocalUKTheme


/**
 * Composable function that creates a dropdown menu with a button as the trigger.
 *
 * @param items the list of items to display in the dropdown menu
 * @param modifier the [Modifier] for the button
 * @param fontSize the font size for the button text. Default is `16sp`.
 * @param itemClick lambda to be called when an item in the dropdown menu is clicked, with the clicked item as the parameter
 */
@Composable
fun DropDownFromButton(
    items: List<String>,
    modifier: Modifier = Modifier,
    fontSize: TextUnit = 16.sp,
    itemClick: (String) -> Unit = {}
) {
    var itemText by rememberSaveable { mutableStateOf(if (items.isNotEmpty()) items[0] else "") }
    var expanded by rememberSaveable { mutableStateOf(false) }

    Button(
        modifier = modifier,
        onClick = { expanded = !expanded }
    ) {
        Text(
            text = itemText,
            fontSize = fontSize,
            modifier = Modifier.padding(end = 8.dp)
        )

        Icon(
            imageVector = Icons.Filled.ArrowDropDown,
            contentDescription = stringResource(R.string.drop_down)
        )

        // Create a dropdown menu with a fixed width of 150dp, and toggle its expansion state based on the 'expanded' state
        DropdownMenu(
            modifier = Modifier.width(150.dp),
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            // For each item in the list, create a dropdown menu item that displays the item text and collapses the menu on click
            items.forEach {
                DropdownMenuItem(
                    text = { Text(text = it, textAlign = TextAlign.Center) },
                    onClick = {
                        // Collapse the dropdown menu
                        expanded = false

                        // Remember the name of the item selected
                        itemText = it

                        // Hoist the state to the caller
                        itemClick(it)
                    }
                )
            }
        }
    }
}

/**
 * Preview for the [DropDownFromButton] composable.
 */
@Composable
@Preview
private fun DropDownPreview() {
    GoLocalUKTheme(dynamicColor = false) {
        val items = listOf("Numbers", "One", "Two")
        DropDownFromButton(items = items)
    }
}