package uk.ac.aber.major.mik46.go.ui.screens.account.nested

import android.annotation.SuppressLint
import android.view.MotionEvent
import android.widget.Toast
import androidx.compose.foundation.gestures.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.GeoPoint
import com.google.maps.android.compose.*
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.forms.editProfile.EditProfileEvent
import uk.ac.aber.major.mik46.go.model.forms.editProfile.EditProfileViewModel
import uk.ac.aber.major.mik46.go.model.forms.registration.RegistrationFormEvent
import uk.ac.aber.major.mik46.go.model.forms.registration.RegistrationViewModel
import uk.ac.aber.major.mik46.go.ui.components.InnerNavigationScaffold
import uk.ac.aber.major.mik46.go.ui.navigation.Screen
import uk.ac.aber.major.mik46.go.ui.screens.signIn.SignInScreen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun UpgradeToBusinessScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel, status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()
    val isSignedIn by mainViewModel.isSignedIn.collectAsState()


    val scrollStateEnabled = remember{ mutableStateOf(true) }
    val scrollState = rememberScrollState()
    val cameraPositionState = rememberCameraPositionState()






    InnerNavigationScaffold(
        onClickBack = {
            navController.popBackStack()
        },
        title = stringResource(R.string.upgrade)
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
                .verticalScroll(
                    state = scrollState,
                    enabled = scrollStateEnabled.value
                )
                .pointerInteropFilter(onTouchEvent = {
                    when (it.action) {
                        MotionEvent.ACTION_DOWN -> {
                            scrollStateEnabled.value = true
                            false
                        }
                        else -> {
                            true
                        }
                    }
                }
                ),
            color = MaterialTheme.colorScheme.background
        ) {
            if (!isSignedIn) {
                SignInScreen(coroutineScope = coroutineScope, user = user, status = status , navController = navController, mainViewModel = mainViewModel)
            }else{
                UpgradeToBusinessScreenContents(coroutineScope, user, mainViewModel, status,navController,scrollStateEnabled,cameraPositionState)
            }


        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@SuppressLint("MissingPermission")
@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun UpgradeToBusinessScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel, status: ConnectivityObserver.Status, navController: NavHostController, scrollStateEnabled: MutableState<Boolean>, cameraPositionState: CameraPositionState ) {


    val editProfileViewModel: EditProfileViewModel = viewModel()

    val context = LocalContext.current

    val markerState = rememberMarkerState()

    val isValidState = editProfileViewModel.isTermsValid


    LaunchedEffect(isValidState.value) {
        if (isValidState.value) {
            isValidState.value = false
            mainViewModel.currentUser.value!!.businessLocation = GeoPoint(markerState.position.latitude, markerState.position.longitude)
            navController.navigate(Screen.RegisterBusiness.route)
        }
    }





    LaunchedEffect(key1 = Unit) {
        LocationServices.getFusedLocationProviderClient(context).lastLocation.addOnSuccessListener() {
            cameraPositionState.position = CameraPosition.fromLatLngZoom(
                LatLng(it.latitude, it.longitude),
                10.5F
            )
            markerState.position = LatLng(it.latitude, it.longitude)
        }
    }




    val state = editProfileViewModel.state


    Column(
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(0.dp, 20.dp, 0.dp, 0.dp),
            text = "Upgrade To Business",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp,
            color = MaterialTheme.colorScheme.onBackground
        )

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 10.dp, 10.dp, 10.dp),
            text = "Below you can move the pin to where your business is located. Your business information will then be shown to other users from this location along with any products you upload.",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Normal,
            fontSize = 14.sp,
            color = MaterialTheme.colorScheme.outline
        )


        val mapProperties = MapProperties(
            // Only enable if user has accepted location permissions.
            isMyLocationEnabled = mainViewModel.locationIsGranted.collectAsState().value,
        )






        Surface(
            modifier = Modifier.size(385.dp),
            color = MaterialTheme.colorScheme.surfaceVariant,
            shape = RoundedCornerShape(45.dp)
        ) {
            GoogleMap(
                cameraPositionState = cameraPositionState,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(10.dp)
                    .clip(RoundedCornerShape(40.dp))
                    .pointerInteropFilter(onTouchEvent = {
                        when (it.action) {
                            MotionEvent.ACTION_DOWN -> {
                                scrollStateEnabled.value = false
                                false
                            }
                            else -> {
                                true
                            }
                        }
                    }
                    ),
                properties = mapProperties,
            ) {
                Marker(
                    state = markerState,
                    draggable = true,
                    title = "Hold to move",
                    snippet = "Your Business Location Pin",
                    icon = BitmapDescriptorFactory.defaultMarker(),
                    zIndex = 10F
                )
            }
        }

        Spacer(modifier = Modifier.height(10.dp))

        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Checkbox(
                checked = state.acceptedTerms,
                onCheckedChange = {
                    editProfileViewModel.onEvent(EditProfileEvent.AcceptTerms(it))
                }
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Accept Location Terms & Conditions")
        }
        if (state.termsError != null) {
            Text(
                text = state.termsError,
                color = MaterialTheme.colorScheme.error,
            )
        }

        Spacer(modifier = Modifier.height(10.dp))

        Button(
            onClick = {
                editProfileViewModel.onEvent(EditProfileEvent.Submit)
            },
            modifier = Modifier
                .width(250.dp),
        ) {
            Text(
                modifier = Modifier,
                text = "Confirm Location",
                fontWeight = FontWeight.SemiBold,
                textAlign = TextAlign.Center,
                fontSize = 18.sp,
            )
        }

    }
}








