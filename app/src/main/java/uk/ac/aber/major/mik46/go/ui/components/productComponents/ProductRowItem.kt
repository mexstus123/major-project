package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.material3.CardDefaults.cardColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.DefaultShadowColor
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductRowItem(
    onClick: () -> Unit = {},
    price: String = "",
    title: String = "",
    image: String = "",
    modifier: Modifier
) {




            Card(
                modifier = modifier,
                shape = RoundedCornerShape(20.dp),
                onClick = onClick ,
                colors = cardColors(
                    containerColor = MaterialTheme.colorScheme.outline.copy(alpha = 0.25F),
                ),

            ) {
                    Card(
                        modifier = Modifier
                            .padding(5.dp).weight(0.75F),
                        shape = RoundedCornerShape(15.dp),
                    ) {
                        AsyncImage(
                            modifier = Modifier.fillMaxSize(),
                            model = image,
                            contentDescription = "product image $image",
                            contentScale = ContentScale.FillBounds)

                        }

                Text(
                    modifier = Modifier.fillMaxWidth().padding(10.dp,0.dp).weight(0.10F),
                    text = price,
                    fontSize = 14.sp,
                    textAlign = TextAlign.Center,



                )
                Text(
                    modifier = Modifier.fillMaxWidth().padding(10.dp,0.dp).weight(0.10F),
                    text = title,
                    fontSize = 15.sp,
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.SemiBold
                )
                Spacer(modifier = Modifier.weight(0.05F))
            }
}

