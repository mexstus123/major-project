package uk.ac.aber.major.mik46.go.ui.screens

import android.Manifest
import android.app.Activity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import kotlinx.coroutines.CoroutineScope
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.viewmodels.PermissionsViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.components.*
import uk.ac.aber.major.mik46.go.ui.navigation.Screen
import uk.ac.aber.major.mik46.go.ui.navigation.openAppSettings
import uk.ac.aber.major.mik46.go.ui.screens.signIn.SignInScreen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AccountScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel, status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()
    val isSignedIn by mainViewModel.isSignedIn.collectAsState()








    MainScaffold(
        navController = navController,
        coroutineScope = coroutineScope,
        user = user,
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            if (!isSignedIn) {
                SignInScreen(coroutineScope = coroutineScope, user = user, status = status , navController = navController, mainViewModel = mainViewModel)
            }else{
                AccountScreenContents(coroutineScope, user, mainViewModel, status,navController)
            }


        }


    }
}

/**
 * A composable function that creates the contents for the `AddWordScreen`.
 *
 * @param activity the activity that this screen belongs to
 * @param user the user object
 * @param userWordsViewModel the view model for the user's words
 * @param coroutineScope the coroutine scope for this composable
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AccountScreenContents(coroutineScope: CoroutineScope, user: User, mainViewModel: MainViewModel, status: ConnectivityObserver.Status, navController: NavHostController, permissionsViewModel: PermissionsViewModel = viewModel()) {

    val ctx = LocalContext.current

    val dialogQueue = permissionsViewModel.visiblePermissionDialogQueue

    val currentUser by mainViewModel.currentUser.observeAsState()


    val locationPermissionResultLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission(),
        onResult = { isGranted ->
            permissionsViewModel.onPermissionResult(
                permission = Manifest.permission.ACCESS_FINE_LOCATION,
                isGranted = isGranted
            )
        }
    )

    dialogQueue
        .reversed()
        .forEach { permission ->
            PermissionDialog(
                permissionTextProvider = when (permission) {
                    Manifest.permission.ACCESS_FINE_LOCATION -> {
                        LocationPermissionTextProvider()
                    }
                    else -> return@forEach
                },
                isPermanentlyDeclined = !shouldShowRequestPermissionRationale(
                    ctx as Activity,
                    permission
                ),
                onDismiss = {
                    permissionsViewModel.dismissDialog()
                    checkUsersLocationPermission(ctx,mainViewModel)
                    if (mainViewModel.locationIsGranted.value) {
                        getUsersLastKnownLocation(ctx , mainViewModel)
                    }
                },
                onConfirmClick = {
                    permissionsViewModel.dismissDialog()
                    locationPermissionResultLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
                },
                onGoToAppSettingsClick = {
                    ctx.openAppSettings()
                }
            )
        }



    val profileButtonList = remember {
        profileButtonsList(
        upgradeOnClick = {
        when (currentUser!!.manufacturerAccount) {
            true -> {
                navController.navigate(route = Screen.MyBusiness.route)
            }
            false -> {

                if (mainViewModel.locationIsGranted.value){
                    navController.navigate(route = Screen.UpgradeToBusiness.route)
                }else{
                    locationPermissionResultLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
                }

            }
        }
        },
        mainViewModel,
        navController) }
    val settingsButtonList = remember { settingsButtonsList(mainViewModel,navController) }
    val supportButtonList = remember { supportButtonsList(mainViewModel,navController) }
    val scrollState = rememberScrollState()






    LaunchedEffect(key1 = Unit) {
        mainViewModel.setupCurrentUser()
    }


    Column(
            verticalArrangement = Arrangement.Top,
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(scrollState),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Spacer(modifier = Modifier.height(10.dp))

            Card(
                modifier = Modifier
                    .size(120.dp)
                    .padding(10.dp),
                shape = RoundedCornerShape(125.dp)
            ) {
                AsyncImage(
                    modifier = Modifier.fillMaxSize(),
                    model = currentUser!!.profilePictureURI,
                    contentDescription = "profile_photo",
                    contentScale = ContentScale.FillBounds
                )
            }
            Text(
                text = currentUser!!.userName,
                fontWeight = FontWeight.SemiBold,
                fontSize = 24.sp
            )
            Text(
                text = currentUser!!.emailAddress,
                fontWeight = FontWeight.Normal,
                fontSize = 20.sp
            )

            Divider(modifier = Modifier.padding(top = 20.dp, bottom = 5.dp))
            //Spacer(modifier = Modifier.height(10.dp))

            profileButtonList.forEach {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .height(60.dp)
                    .clickable(onClick = it.onClick),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(modifier = Modifier
                        .weight(0.15F)
                        .fillMaxSize(0.5F)
                        .padding(horizontal = 10.dp),
                        imageVector = it.icon,
                        contentDescription = it.buttonText,
                        tint = if (it.iconColour){
                            MaterialTheme.colorScheme.primary
                        }else{
                            MaterialTheme.colorScheme.onBackground
                        }
                    )

                    Text(modifier = Modifier
                        .weight(0.7F)
                        .fillMaxWidth(),
                        text = it.buttonText,
                        textAlign = TextAlign.Start,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 16.sp
                    )
                    Icon(modifier = Modifier
                        .weight(0.15F)
                        .fillMaxSize(0.5F)
                        .padding(horizontal = 10.dp),
                        imageVector = Icons.Default.KeyboardArrowRight,
                        contentDescription = "arrow ${it.buttonText}"
                    )
                }
            }

        Divider(modifier = Modifier.padding(top = 15.dp, bottom = 5.dp))

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 0.dp, 5.dp),
            text = stringResource(R.string.app_settings),
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )

        settingsButtonList.forEach {
            Row(modifier = Modifier
                .fillMaxWidth()
                .height(60.dp)
                .clickable(onClick = it.onClick),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(modifier = Modifier
                    .weight(0.15F)
                    .fillMaxSize(0.5F)
                    .padding(horizontal = 10.dp),
                    imageVector = it.icon,
                    contentDescription = it.buttonText
                )

                Text(modifier = Modifier
                    .weight(0.7F)
                    .fillMaxWidth(),
                    text = it.buttonText,
                    textAlign = TextAlign.Start,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 16.sp
                )
                Icon(modifier = Modifier
                    .weight(0.15F)
                    .fillMaxSize(0.5F)
                    .padding(horizontal = 10.dp),
                    imageVector = Icons.Default.KeyboardArrowRight,
                    contentDescription = "arrow ${it.buttonText}"
                )
            }
        }

        Divider(modifier = Modifier.padding(top = 15.dp, bottom = 5.dp))

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, 20.dp, 0.dp, 5.dp),
            text = "Support",
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.SemiBold,
            fontSize = 18.sp,
            color = MaterialTheme.colorScheme.outline
        )

        supportButtonList.forEach {
            Row(modifier = Modifier
                .fillMaxWidth()
                .height(60.dp)
                .clickable(onClick = it.onClick),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(modifier = Modifier
                    .weight(0.15F)
                    .fillMaxSize(0.5F)
                    .padding(horizontal = 10.dp),
                    imageVector = it.icon,
                    contentDescription = it.buttonText
                )

                Text(modifier = Modifier
                    .weight(0.7F)
                    .fillMaxWidth(),
                    text = it.buttonText,
                    textAlign = TextAlign.Start,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 16.sp
                )
                Icon(modifier = Modifier
                    .weight(0.15F)
                    .fillMaxSize(0.5F)
                    .padding(horizontal = 10.dp),
                    imageVector = Icons.Default.KeyboardArrowRight,
                    contentDescription = "arrow ${it.buttonText}"
                )
            }
        }

            SignOutButton(modifier = Modifier.fillMaxWidth(),user,mainViewModel)

    }
}

private fun profileButtonsList(upgradeOnClick: () -> Unit, mainViewModel: MainViewModel, navController: NavHostController): List<IconButtonGroup> {

    return listOf(
        IconButtonGroup(
            Icons.Default.Edit,
            "Profile",
            onClick = {
                navController.navigate(route = Screen.EditProfile.route)
            }
        ),
        IconButtonGroup(
            Icons.Default.GppGood,
            buttonText = when (mainViewModel.currentUser.value!!.manufacturerAccount) {
                true -> {
                    "Business Page"
                }
                false -> {
                    "Upgrade To Business"
                }
            },
            onClick = upgradeOnClick,
            iconColour = true

        ), IconButtonGroup(
            Icons.Default.Favorite,
            "Favourites",
            onClick = {
                navController.navigate(route = Screen.Favourites.route)
            }
        ), IconButtonGroup(
            Icons.Default.Star,
            "Following",
            onClick = {
                navController.navigate(route = Screen.Following.route)
            }
        ),

    )
}

private fun settingsButtonsList(mainViewModel: MainViewModel, navController: NavHostController): List<IconButtonGroup> {

    return listOf(

        IconButtonGroup(
            Icons.Default.Shield,
            "Privacy & Safety",
            onClick = {
                navController.navigate(route = Screen.PrivacyAndSafety.route)
            }
        ),
        IconButtonGroup(
            Icons.Default.Settings,
            "Settings",
            onClick = {
                navController.navigate(route = Screen.AccountSettings.route)
            }
        )

    )
}

private fun supportButtonsList(mainViewModel: MainViewModel, navController: NavHostController): List<IconButtonGroup> {

    return listOf(

        IconButtonGroup(
            Icons.Default.ContactSupport,
            "Support",
            onClick = {
                navController.navigate(route = Screen.Support.route)
            }
        ),
        IconButtonGroup(
            Icons.Default.Info,
            "What's new",
            onClick = {
                navController.navigate(route = Screen.WhatsNew.route)
            }
        ),
        IconButtonGroup(
            Icons.Default.Info,
            "About",
            onClick = {
                navController.navigate(route = Screen.About.route)
            }
        )

    )
}









