package uk.ac.aber.major.mik46.go.model

import android.icu.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


data class UserChatMessage(
    var time: Date = Date(),
    var content: String = "",
    var sender: String = "",
    var receiver: String = "",
    var image: Boolean = false, // firebase does not allow isImage as it sees it as something else and ignores it when trying to map it to an object
    var wasRead: Boolean = true
){

    fun getTimeFormat(): String{
        val currentDate = Date()

        val currentTime = Date().time
        val messageTime = time.time

        val diffInMs = currentTime - messageTime
        val diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs)
        val diffInMin = TimeUnit.MILLISECONDS.toMinutes(diffInMs)
        val diffInHours = TimeUnit.MILLISECONDS.toHours(diffInMs)
        val diffInDays = TimeUnit.MILLISECONDS.toDays(diffInMs)


        return when {
            diffInMs <= 6000 -> "just now"
            diffInSec <= 60 -> "${diffInSec}s ago"
            diffInMin <= 60 -> "${diffInMin}m ago"
            diffInHours <= 24 -> "${diffInHours}h ago"
            diffInDays <= 7 -> "${diffInDays}d ago"
            else -> SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()).format(time)
            }
        }

}



