package uk.ac.aber.major.mik46.go.model.repositories

import android.content.ContentValues
import android.net.Uri

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.*
import com.google.firebase.messaging.Constants
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.UserChat
import uk.ac.aber.major.mik46.go.model.UserChatMessage
import uk.ac.aber.major.mik46.go.model.notifications.NotificationData
import uk.ac.aber.major.mik46.go.model.notifications.PushNotification
import uk.ac.aber.major.mik46.go.model.notifications.RetrofitInstance
import java.util.*


/**
 * Repository class responsible for handling data operations.
 * This is the mediator between the [UserWordDao] and the [UserWordsViewModel].
 *
 * @param application The application context
 */
class MapsRepository(): MapsRepo {

    private val storage = FirebaseStorage.getInstance()


    private val db = FirebaseFirestore.getInstance()

    private lateinit var currentLastVisibleDocument: DocumentSnapshot
    lateinit var listenerRegistration: ListenerRegistration

    private suspend fun getUIDByEmail(email: String):String{
        try {
            val userFromDb = db.collection("new").whereEqualTo("emailAddress", email).get().await()
            if (!userFromDb.isEmpty) {
                userFromDb
                    .forEach { documentSnapshot ->
                        return documentSnapshot.id
                    }
            }
        }catch (e: FirebaseFirestoreException) {
            Log.d("error", "getting user ID by email: $e")
        }
        return ""
    }

    override suspend fun getManufacturersLocations(): List<UserChat> {
        TODO("Not yet implemented")
    }


}