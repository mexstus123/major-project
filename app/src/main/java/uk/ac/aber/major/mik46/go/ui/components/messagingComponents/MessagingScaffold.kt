package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel

import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.navigation.Screen

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MessagingScaffold(
    status: ConnectivityObserver.Status,
    mainViewModel: MainViewModel,
    typingState: MutableState<Boolean>,
    navController: NavHostController,
    user: User,
    showTimeState: List<MutableState<Boolean>>,
    pageContent: @Composable (innerPadding: PaddingValues) -> Unit = {},


    ) {
    val receiver = mainViewModel.manufacturer.observeAsState()
    val chatterName = receiver.value!!.userName
    val chatterPic = receiver.value!!.profilePictureURI
    val chatterUID = receiver.value!!.UID


    Scaffold(
        topBar = {
            MessagingTopAppBar(
                profilePic = chatterPic,
                name = chatterName,
                onClickBack = {
                    mainViewModel.removeCurrentListener()
                    mainViewModel.emptyOldMessageList()
                    navController.popBackStack()
                },
                onClickProfile = {
                    navController.navigate(Screen.ManufacturerPage.route) {
                        if (navController.backQueue.size >= 4) {
                            popUpTo(Screen.Messaging.route) {
                                saveState = true
                            }
                        }
                        launchSingleTop =true
                    }
                }
            )
        },

        bottomBar = {
            Box(
                modifier = Modifier
                    .wrapContentHeight(unbounded = true)
                    .fillMaxWidth()
            ) {
                MessagingBottomBar(
                    typingState = typingState,
                    user = user,
                    receiverUID = chatterUID,
                    modifier = Modifier.align(Alignment.BottomCenter),
                    showTimeState = showTimeState,
                    status = status,
                mainViewModel = mainViewModel)
            }

              },
        content = { innerPadding -> pageContent(innerPadding) },
        modifier = Modifier.wrapContentHeight()
    )
}
