package uk.ac.aber.major.mik46.go.model

import android.content.Context
import android.graphics.Bitmap
import android.media.ExifInterface
import android.net.Uri
import android.provider.MediaStore
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage
import java.io.File


fun imageProcessing(imageUri: Uri, bitmap: Bitmap, context: Context):Bitmap {

    var rotatedBitmap = bitmap
    val imagePath = getRealPathFromUri(context = context, uri = imageUri)

    if (!imagePath.isNullOrEmpty()) {
        val file = File(imagePath)

        //getting the orientation of the image
        val ei = ExifInterface(file)
        val orientation = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )
        //creating the rotated bitmap
        rotatedBitmap =
        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90)
            ExifInterface.ORIENTATION_ROTATE_180 ->  rotateImage(bitmap, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270)
            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> bitmap
        }
    }
    return rotatedBitmap
}

fun getRealPathFromUri(context: Context, uri: Uri): String? {
    //getting a cursor inorder to navigate through the directories
    val cursor = context.contentResolver.query(uri, null, null, null, null)
    cursor?.let {
        it.moveToFirst()
        val columnIndex = it.getColumnIndex(MediaStore.Images.Media.DATA)
        val path = it.getString(columnIndex)
        it.close()
        return path
    }
    return null
}