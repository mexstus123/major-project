package uk.ac.aber.major.mik46.go.model.forms.signIn

sealed class EmailSignInEvent {
    data class EmailChanged(val email: String) : EmailSignInEvent()
    data class PasswordChanged(val password: String) : EmailSignInEvent()

    object Submit: EmailSignInEvent()
}
