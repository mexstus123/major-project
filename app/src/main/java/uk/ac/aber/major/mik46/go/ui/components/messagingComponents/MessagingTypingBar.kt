package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.*

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import uk.ac.aber.major.mik46.go.R
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver

@Composable
fun MessagingTypingBar(
    status: ConnectivityObserver.Status,
    typingWord: MutableState<String>,
    sendMessageOnClick: () -> Unit,
    modifier: Modifier
){

        Row(modifier = modifier
            .fillMaxHeight()
            .wrapContentHeight(unbounded = true)) {

            MessagingTextField(typingWord ,modifier =  Modifier.weight(0.87F), status)

            Row(
                modifier = Modifier
                    .weight(0.13F)
                    .fillMaxWidth()
                    .align(Alignment.Bottom)
                    .height(65.dp),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(onClick = sendMessageOnClick) {
                    Icon(
                        imageVector = Icons.Filled.Send,
                        //tint = MaterialTheme.colorScheme.onBackground,
                        contentDescription = stringResource(R.string.send_icon),
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(2.dp, 0.dp, 8.dp, 0.dp)

                        ,
                        tint = MaterialTheme.colorScheme.inversePrimary
                    )
                }
            }


        }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MessagingTextField(
    textFieldWord: MutableState<String>,
    modifier: Modifier,
    status: ConnectivityObserver.Status

) {
    TextField(
        textStyle = LocalTextStyle.current.copy(color = MaterialTheme.colorScheme.onBackground),
        enabled = status == ConnectivityObserver.Status.Available,
        value = when(status != ConnectivityObserver.Status.Available) {
            true -> "Network ${status.name}"
            false -> textFieldWord.value
        },
            onValueChange = {textFieldWord.value = it},
                //onClick = {typingState.value = true},
            modifier = modifier
                .padding(20.dp, 6.dp, 10.dp, 6.dp),
            shape = RoundedCornerShape(22.dp),
            placeholder = { Text(text = stringResource(id =R.string.empty_message_field ), fontSize = 14.sp, color = Color(0xFFC1C9BF)) },
            colors = TextFieldDefaults.textFieldColors(
                containerColor = when(status != ConnectivityObserver.Status.Available) {
                    true -> MaterialTheme.colorScheme.outline
                    false -> MaterialTheme.colorScheme.outline.copy(alpha = 0.10F)
                }, // onSurface colour- to not change on theme change
                focusedIndicatorColor =  Color.Transparent, //hide the indicator
                unfocusedIndicatorColor = Color.Transparent,
                textColor = Color(0xFF191C19),
                disabledTextColor = MaterialTheme.colorScheme.error,
                disabledIndicatorColor = Color.Transparent,
                disabledPlaceholderColor = Color.Transparent
            ),
            keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Sentences),
            maxLines = 5,
//        trailingIcon = { //emoji button
//
//            IconButton(onClick = {}) {
//                Icon(
//                    imageVector = Icons.Outlined.EmojiEmotions,
//                    //tint = MaterialTheme.colorScheme.onBackground,
//                    contentDescription = stringResource(R.string.emoji_face_button),
//                    modifier = Modifier
//                        .fillMaxSize()
//                        .padding(2.dp,0.dp,8.dp,0.dp)
//
//                    ,
//                    tint = MaterialTheme.colorScheme.inversePrimary
//                )
//            }
//        }
                )

}