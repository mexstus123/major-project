package uk.ac.aber.major.mik46.go.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import uk.ac.aber.major.mik46.go.R


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BackNavigationButton(
    onClick: () -> Unit = {},
    size: Int = 63,
    modifier: Modifier
){

    Surface(
        modifier = modifier
            .size(size.dp)
            .fillMaxHeight(0.4f)
            .padding(10.dp),
        shape = RoundedCornerShape(15.dp),
        onClick = onClick,
        color = MaterialTheme.colorScheme.secondary
    ) {
        Icon(
            imageVector = Icons.Filled.KeyboardArrowLeft,
            contentDescription = stringResource(R.string.back_arrow),
            // specify the tint color for the icon
            tint = MaterialTheme.colorScheme.secondaryContainer,
            modifier = Modifier.fillMaxSize()
        )
    }
}