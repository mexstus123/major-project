package uk.ac.aber.major.mik46.go.model.notifications

data class PushNotification(
    val data: NotificationData,
    val to: String
)