package uk.ac.aber.major.mik46.go.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver

import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.ui.components.*
import uk.ac.aber.major.mik46.go.ui.navigation.Screen
import uk.ac.aber.major.mik46.go.ui.screens.signIn.SignInScreen


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ChatsScreen(navController: NavHostController, user: User, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status){

    val coroutineScope = rememberCoroutineScope()

    val isSignedIn by mainViewModel.isSignedIn.collectAsState()


    MainScaffold(
        navController = navController,
        coroutineScope = coroutineScope,
        user = user,
    ){  innerPadding ->
        Surface(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {

            if (!isSignedIn){
                SignInScreen(coroutineScope = coroutineScope, user = user, status = status , navController = navController, mainViewModel = mainViewModel)
            }else {
                ChatsScreenContents(coroutineScope, user, navController,mainViewModel, status)
            }
        }


    }
}


@Composable
fun ChatsScreenContents(coroutineScope: CoroutineScope, user: User, navController: NavHostController, mainViewModel: MainViewModel = viewModel(), status: ConnectivityObserver.Status) {


    val usersChats by mainViewModel.usersChats.observeAsState(listOf())
    if(status == ConnectivityObserver.Status.Available) {
        LaunchedEffect(key1 = mainViewModel.currentUser.value) {
            mainViewModel.updateUsersChats()
        }
    }


    Column(
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            ChatsPageTopAppBar(onClick = {
                navController.navigate(Screen.Account.route) {
                    // Pop up to the start destination and save the state
                    popUpTo(navController.graph.findStartDestination().id) {
                        saveState = true
                    }
                    // Set the launch mode to singleTop
                    launchSingleTop = true
                    // Restore the state when navigating
                    restoreState = true
                }
            },
                mainViewModel.currentUser.observeAsState(User()).value.profilePictureURI,
                status = status
            )
            val isLoading by mainViewModel.isLoading.collectAsState()
            val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = isLoading)

            SwipeRefresh(
                swipeEnabled = status == ConnectivityObserver.Status.Available,

                state = swipeRefreshState,
                onRefresh = {
                    coroutineScope.launch{
                        mainViewModel.updateUsersChats()
                    }
                }
            ){

                LazyColumn(
                    modifier = Modifier.fillMaxSize(),
                    userScrollEnabled = true
                ){

                    usersChats.forEachIndexed{index, it ->

                        item {
                            Spacer(modifier = Modifier.height(10.dp))
                            ChatsRow(
                                name = it.receiver.userName,
                                lastMessage = it.lastMessage,
                                lastMessageTime = it.getTimeFormat(),
                                profilePic = it.receiver.profilePictureURI,
                                onClick = {
                                    mainViewModel.manufacturer.value = it.receiver

                                    navController.navigate(Screen.Messaging.route)

                                },
                                lastMessageWasRead = it.wasLastMessageRead
                            )

                        }
                    }

            }

            }



        }
    }









