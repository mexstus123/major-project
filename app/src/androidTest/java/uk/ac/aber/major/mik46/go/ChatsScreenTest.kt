package uk.ac.aber.major.mik46.go

import android.net.Uri
import androidx.compose.runtime.Composable
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlinx.coroutines.*
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.ac.aber.major.mik46.go.model.*
import uk.ac.aber.major.mik46.go.model.connectivity.ConnectivityObserver
import uk.ac.aber.major.mik46.go.model.viewmodels.MainViewModel
import uk.ac.aber.major.mik46.go.repositories.FakeMainRepository
import uk.ac.aber.major.mik46.go.repositories.FakeMessagingRepository
import uk.ac.aber.major.mik46.go.ui.navigation.Screen
import uk.ac.aber.major.mik46.go.ui.screens.*
import uk.ac.aber.major.mik46.go.ui.theme.GoLocalUKTheme

@OptIn(ExperimentalCoroutinesApi::class)
class ChatsScreenTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    private lateinit var mainViewModel: MainViewModel
    private lateinit var fakeFireStoreRepository: FakeMainRepository
    private lateinit var fakeMessagingRepository: FakeMessagingRepository
    private lateinit var testUser: User
    private lateinit var testUser2: User
    private lateinit var noAuthenticationTestUser: User


    private lateinit var authentication : FirebaseAuth
    private lateinit var noAuthentication : FirebaseAuth
    @Composable
    private fun BuildChatsTestNavGraph(user: User, mainViewModel: MainViewModel, status: ConnectivityObserver.Status){

        val navController = rememberNavController()


        NavHost(
            navController = navController,
            startDestination = Screen.Chats.route
        ){
            composable(Screen.Products.route){ ProductsScreen(navController,user,mainViewModel,status) }
            composable(Screen.Chats.route){ ChatsScreen(navController,user,mainViewModel,status) }
            composable(Screen.Account.route){ AccountScreen(navController,user,mainViewModel,status) }
            composable(Screen.Map.route){ MapScreen(navController,user,mainViewModel,status) }
            composable(Screen.Messaging.route){ MessagingScreen(navController,user,mainViewModel,status) }
        }
    }



    @Before
    fun setup() {
        authentication = mockk<FirebaseAuth>()
        every { authentication.currentUser } returns mockk<FirebaseUser>()
        every { authentication.currentUser!!.uid } returns "test"
        every { authentication.currentUser!!.email} returns "test"
        every { authentication.currentUser!!.displayName } returns "test"
        every { authentication.currentUser!!.photoUrl } returns mockk<Uri>()

        noAuthentication = mockk<FirebaseAuth>()
        every { noAuthentication.currentUser } returns null

        testUser = User(authentication = authentication, userName = "user1", emailAddress = "test@email.com", UID = "1")
        testUser2 = User(authentication = authentication, userName = "user2")
        noAuthenticationTestUser = User(authentication = noAuthentication)

        fakeFireStoreRepository = FakeMainRepository()
        fakeMessagingRepository = FakeMessagingRepository()

        mainViewModel = MainViewModel(
            mainRepository = fakeFireStoreRepository,
            messagingRepository = fakeMessagingRepository,
            ioDispatcher = Dispatchers.Main
        )
    }

    @After
    fun testEnd(){
        clearAllMocks()
        unmockkAll()

    }


    @Test
    fun `Chats screen is being displayed`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
            BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
                }
        }
        //composeTestRule.onNodeWithText("Library").performClick()
//        composeTestRule.onNodeWithText("Chats").assertIsDisplayed()

        composeTestRule.onNode(hasText("Chats") and hasNoClickAction()).assertIsDisplayed()
    }

    @Test
    fun `Chats screen not displayed when user is not authenticated`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = noAuthenticationTestUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }

        composeTestRule.onNodeWithText("Sign In With Google").assertIsDisplayed()
    }

    @Test
    fun `user is not authenticated navigating away and back`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = noAuthenticationTestUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }

        composeTestRule.onNodeWithText("Sign In With Google").assertIsDisplayed()
        composeTestRule.onNodeWithText("Products").performClick()
        composeTestRule.onNodeWithText("Chats").performClick()
        composeTestRule.onNodeWithText("Sign In With Google").assertIsDisplayed()

    }

    @Test
    fun `user is not authenticated navigating away and authenticating then back`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = noAuthenticationTestUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }

        composeTestRule.onNodeWithText("Sign In With Google").assertIsDisplayed()
        composeTestRule.onNodeWithText("Products").performClick()
        noAuthenticationTestUser.authentication = authentication
        composeTestRule.onNodeWithText("Chats").performClick()
        composeTestRule.onNode(hasText("Chats") and hasNoClickAction()).assertIsDisplayed()

    }

    @Test
    fun `users chat List is updated on refresh`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }

        fakeFireStoreRepository.chatsList.add(UserChat(lastMessage = "test1", receiver = testUser2))
        composeTestRule.onNode(hasText("Chats") and hasNoClickAction()).assertIsDisplayed()

        composeTestRule.onNodeWithText("Products").performClick()
        composeTestRule.onNodeWithText("Chats").performClick()
        composeTestRule.onNodeWithText("test1").assertIsDisplayed()
    }

    @Test
    fun `navigates to messages and displays users name`() = runTest {

        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }

        fakeFireStoreRepository.chatsList.add(UserChat(lastMessage = "test1", receiver = testUser2))
        composeTestRule.onNode(hasText("Chats") and hasNoClickAction()).assertIsDisplayed()

        composeTestRule.onNodeWithText("Products").performClick()
        composeTestRule.onNodeWithText("Chats").performClick()
        composeTestRule.onNodeWithText("test1").performClick()
        composeTestRule.onNode(hasText("user2") and hasNoClickAction()).assertIsDisplayed()
    }

    @Test
    fun `navigates to messages and displays users name2`() = runTest {

        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }

        fakeFireStoreRepository.chatsList.add(UserChat(lastMessage = "test1", receiver = testUser2))
        composeTestRule.onNode(hasText("Chats") and hasNoClickAction()).assertIsDisplayed()

        composeTestRule.onNodeWithText("Products").performClick()
        composeTestRule.onNodeWithText("Chats").performClick()
        composeTestRule.onNodeWithText("test1").performClick()
        composeTestRule.onNode(hasText("user2") and hasNoClickAction()).assertIsDisplayed()
    }

    @Test
    fun `users chat List 10 chats`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }
        for (i in 0..10){
            fakeFireStoreRepository.chatsList.add(UserChat(lastMessage = "test$i", receiver = User(userName = "testName$i")))
        }

        composeTestRule.onNode(hasText("Chats") and hasNoClickAction()).assertIsDisplayed()

        composeTestRule.onNodeWithText("Products").performClick()
        composeTestRule.onNodeWithText("Chats").performClick()

        composeTestRule.onNode(hasScrollAction()).performScrollToIndex(10)
        composeTestRule.onNode(hasText("testName10")).assertIsDisplayed()

    }

    @Test
    fun `users chat List 50 chats`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }
        for (i in 0..50){
            fakeFireStoreRepository.chatsList.add(UserChat(lastMessage = "test$i", receiver = User(userName = "testName$i")))
        }

        composeTestRule.onNode(hasText("Chats") and hasNoClickAction()).assertIsDisplayed()

        composeTestRule.onNodeWithText("Products").performClick()
        composeTestRule.onNodeWithText("Chats").performClick()

        composeTestRule.onNode(hasScrollAction()).performScrollToIndex(50)
        composeTestRule.onNode(hasText("testName50")).assertIsDisplayed()
        composeTestRule.onNode(hasScrollAction()).performScrollToIndex(25)
        composeTestRule.onNode(hasText("testName25")).assertIsDisplayed()

    }

    @Test
    fun `users chat List 100 chats`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }
        for (i in 0..100){
            fakeFireStoreRepository.chatsList.add(UserChat(lastMessage = "test$i", receiver = User(userName = "testName$i")))
        }

        composeTestRule.onNode(hasText("Chats") and hasNoClickAction()).assertIsDisplayed()

        composeTestRule.onNodeWithText("Products").performClick()
        composeTestRule.onNodeWithText("Chats").performClick()

        composeTestRule.onNode(hasScrollAction()).performScrollToIndex(100)
        composeTestRule.onNode(hasText("testName100")).assertIsDisplayed()
        composeTestRule.onNode(hasScrollAction()).performScrollToIndex(75)
        composeTestRule.onNode(hasText("testName75")).assertIsDisplayed()
        composeTestRule.onNode(hasScrollAction()).performScrollToIndex(50)
        composeTestRule.onNode(hasText("testName50")).assertIsDisplayed()

    }

    @Test
    fun `users chat network unavailable`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Unavailable)
            }
        }

        composeTestRule.onNode(hasText("Network Unavailable") and hasNoClickAction()).assertIsDisplayed()
    }

    @Test
    fun `users chat network Available`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }

        composeTestRule.onNode(hasText("Network Unavailable") and hasNoClickAction()).assertDoesNotExist()
        composeTestRule.onNode(hasText("Network Lost") and hasNoClickAction()).assertDoesNotExist()
    }

    @Test
    fun `users chat network Lost`() = runTest {
        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Lost)
            }
        }

        composeTestRule.onNode(hasText("Network Lost") and hasNoClickAction()).assertIsDisplayed()
    }

    @Test
    fun `users chat network Lost adding to chats list`() = runTest {

        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Lost)
            }
        }


        composeTestRule.onNode(hasText("Network Lost") and hasNoClickAction()).assertIsDisplayed()

        for (i in 0..1){
            fakeFireStoreRepository.chatsList.add(UserChat(lastMessage = "test$i", receiver = User(userName = "testName$i")))
        }
        composeTestRule.onNodeWithText("Products").performClick()
        composeTestRule.onNodeWithText("Chats").performClick()
        composeTestRule.onNode(hasText("testName1")).assertDoesNotExist()

    }

    @Test
    fun `updating chat updates the list`() = runTest {

        composeTestRule.setContent {
            GoLocalUKTheme {
                BuildChatsTestNavGraph(user = testUser, mainViewModel = mainViewModel, status = ConnectivityObserver.Status.Available)
            }
        }

        for (i in 0..1){
            fakeFireStoreRepository.chatsList.add(UserChat(lastMessage = "test$i", receiver = User(userName = "testName$i")))
        }
        mainViewModel.updateUsersChats(testUser)
        composeTestRule.onNode(hasText("test1")).assertIsDisplayed()

    }

}