package uk.ac.aber.major.mik46.go.repositories

import uk.ac.aber.major.mik46.go.model.User
import uk.ac.aber.major.mik46.go.model.UserChat
import uk.ac.aber.major.mik46.go.model.repositories.MainRepo


class FakeMainRepository : MainRepo{

    val usersList = mutableListOf<User>()
    val chatsList = mutableListOf<UserChat>()
    val testRegToken = "testToken"
    val userRegTokenList = mutableListOf<String>()

    private var shouldReturnNetworkError = false

    fun setShouldReturnNetworkError(value: Boolean){
        shouldReturnNetworkError = value
    }

    override suspend fun updateUsersChats(user: User): List<UserChat> {
        return chatsList
    }

    override suspend fun removeRegistrationToken(user: User, oldToken: String) {
        if (usersList.contains(user)){
            usersList.remove(user)
            userRegTokenList.removeFirst()
        }
    }

    override suspend fun getCurrentRegistrationToken(): String {
        return testRegToken
    }

    override suspend fun userSetup(user: User){
        if (!usersList.contains(user)){
            userRegTokenList.add(testRegToken)
            usersList.add(user)
        }
    }
}